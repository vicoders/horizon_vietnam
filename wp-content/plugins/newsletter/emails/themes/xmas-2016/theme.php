<?php
if (!defined('ABSPATH'))
    exit;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div class="content" style="max-width: 750px; margin: 0 auto; background: #ebebeb;">
<div class="body-conten">
<div class="section1"><img src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/logo-ban.png" alt="" /></div>
<div class="section2" style="width: 100%; height: 307px; background-image: url('http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/123.png'); background-position: 86% 100%; margin-top: -4px; z-index: 9999999999999999; position: relative;"> </div>
<div class="section3" style="font-family: georgia, Arial, Helvetica, sans-serif; font-style: italic; max-width: 551px; margin: 0 auto; background: white; padding: 15px; margin-top: -51px; z-index: 1;">
<h1 style="font-size: 16px;">Cher monsieur {name},</h1>
<p style="font-size: 14px;"><font size="3">Toute l'équipe de HORIZON VIETNAM TRAVEL se joint à moi pour vous remercier de la confiance que vous nous avez accordée durant ces derniers temps</font></p>
<p style="font-size: 14px;"><font size="3">En cette période des fêtes, nous vous transmettons nos meilleurs voeux de santé, joie, bonheur et prospérité à vous et votre famille pour la nouvelle année 2017.</font></p>
<p style="font-size: 14px;"><font size="3">Au plaisir de vous revoir !</font></p>
<h2 style="font-size: 15px; font-weight: normal; text-align: center;"><font size="3">TA BAU et l'équipe de HORIZON VIETNAM TRAVEL</font></h2>
</div>
<div class="bg" style="background: #fff url('http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/bg.png'); background-repeat: no-repeat; background-position: 0% 111%; max-width: 581px; margin: 0 auto;">
<div class="section4" style="font-family: utm_androgyneregular; max-width: 581px; margin: 0 auto; z-index: 1;"><img style="width: 100%; height: auto;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/khung.png" alt="" /></div>
<div class="section5" style="font-family: utm_androgyneregular; max-width: 581px; margin: 0 auto; z-index: 1; height: 394px; background: url('http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/33.png'); background-position: 100% 100%; background-size: contain; position: relative;">
<div class="text" style="text-align: right; display: table; float: right; margin-top: 100px; margin-right: 15px;">
<p style="color: white; display: table; position: absolute; right: 18px; top: 98px; background: url('http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/22.png'); background-size: cover; width: 197px; height: 32px; vertical-align: middle; text-align: center; line-height: 29px; font-family: utm_avoregular;">Voyage Solidaire</p>
</div>
<p style="display: table; margin-right: 15px; clear: both; right: 0px; float: right;"><img style="position: absolute; right: 10px; top: 170px;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/11.png" alt="" /></p>
</div>
</div>
<div class="footer" style="background: #117700; text-align: center; color: #fff; padding: 10px;">
<div class="country">
<ul>
<li style="list-style: none; display: inline-block; margin-right: 30px;"><img style="vertical-align: middle; margin-right: 3px;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/vn.png" alt="" /><span>Viet Nam</span></li>
<li style="list-style: none; display: inline-block; margin-right: 30px;"><img style="vertical-align: middle; margin-right: 3px;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/cam.png" alt="" /><span>Campodge</span></li>
<li style="list-style: none; display: inline-block; margin-right: 30px;"><img style="vertical-align: middle; margin-right: 3px;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/lao.png" alt="" /><span>Laos</span></li>
</ul>
</div>
<div class="contact">
<p style="font-size: 15px; margin: 5px;">© Droit d'auteur par HORIZON VIETNAM TRAVEL</p>
<p style="font-size: 15px; margin: 5px;">Licence d` état de voyage International - N° 01-387/TCDL- GP LHQT</p>
<p style="font-size: 15px; margin: 5px;">Agrée par la Direction Générale du Tourisme du Vietnam</p>
<p style="font-size: 15px; margin: 5px;">Si vous avez des difficultés pour visualiser ce message <a style="color: yellow;" href="http://horizon-vietnamvoyage.com/{email_url}">cliquez ici</a></p>
<p><em><font face="helvetica">Pour vous désabonner de la newsletter <a style="color: yellow;" href="http://horizon-vietnamvoyage.com/{unsubscription_url}">cliquez ici</a></font></em></p>
</div>
</div>
</div>
</div>
</html>