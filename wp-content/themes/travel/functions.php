<?php

/*
 *
 *    Coo Functions
 *    ------------------------------------------------
 *    Cootheme
 *     http://www.cootheme.com
 *
 *    VARIABLE DEFINITIONS
 *    PLUGIN INCLUDES
 *    THEME UPDATER
 *    THEME SUPPORT
 *    THUMBNAIL SIZES
 *    CONTENT WIDTH
 *    LOAD THEME LANGUAGE
 *    ct_custom_content_functions()
 *    ct_include_framework()
 *    ct_enqueue_styles()
 *    ct_enqueue_scripts()
 *    ct_load_custom_scripts()
 *    ct_admin_scripts()
 *    ct_layerslider_overrides()
 *
 */
define('THEME_PREFIX', 'coo-theme-admin');
define('THEME_NAME', 'coo-theme-admin');
define('TEXT_DOMAIN', 'coo-theme-admin');

/* VARIABLE DEFINITIONS
================================================== */
define('CT_TEMPLATE_PATH', get_template_directory());
define('CT_INCLUDES_PATH', CT_TEMPLATE_PATH . '/includes');
define('CT_FRAMEWORK_PATH', CT_TEMPLATE_PATH . '/coo-theme');
define('CT_WIDGETS_PATH', CT_INCLUDES_PATH . '/widgets');
define('CT_LOCAL_PATH', get_template_directory_uri());

/* PLUGIN INCLUDES
================================================== */
$options        = get_option('ct_coo_options');
$disable_loveit = false;
if (isset($options['disable_loveit']) && $options['disable_loveit'] == 1) {
    $disable_loveit = true;
}
require_once CT_INCLUDES_PATH . '/plugins/aq_resizer.php';
include_once CT_INCLUDES_PATH . '/plugin-includes.php';

/* THEME UPDATER FRAMEWORK
================================================== */
//require_once(CT_INCLUDES_PATH . '/wp-updates-theme.php');
//new WPUpdatesThemeUpdater_516( 'http://wp-updates.com/api/2/theme', basename(get_template_directory()));

/* add categories to attachments
================================================== */
//    function wptp_add_categories_to_attachments() {
//        register_taxonomy_for_object_type( 'category', 'attachment' );
//    }
//    add_action( 'init' , 'wptp_add_categories_to_attachments' );
/* THEME SUPPORT
================================================== */
add_theme_support('structured-post-formats', ['audio', 'gallery', 'image', 'link', 'video']);
add_theme_support('post-formats', ['aside', 'chat', 'quote', 'status']);
add_theme_support('automatic-feed-links');
add_theme_support('post-thumbnails');
add_theme_support('woocommerce');

/* THUMBNAIL SIZES
================================================== */
set_post_thumbnail_size(220, 150, true);
add_image_size('widget-image', 94, 70, true);
add_image_size('thumb-image', 600, 450, true);

add_image_size('blog-image', 1280, 9999);
add_image_size('full-width-image-gallery', 1280, 720, true);

//shortcode (travel)

add_image_size('img-introduce', 545, 340, true);

add_image_size('img-category', 264, 162, true);

add_image_size('img-title', 1170, 294, true);

add_image_size('img-slide', 1170, 478, true);

add_image_size('img-slider', 1170, 400, true);

add_image_size('img-link', 140, 140, true);

add_image_size('img-user', 155, 155, true);

add_image_size('img-des-category', 300, 100, true);

add_image_size('img-embassies', 162, 225, true);

add_image_size('aboutus-category', 300, 100, true);

add_image_size('img-top-footer', 110, 110, true);

//end

/* CONTENT WIDTH
================================================== */
if (!isset($content_width)) {
    $content_width = 1140;
}

/* LOAD THEME LANGUAGE
================================================== */
load_theme_textdomain('cootheme', CT_TEMPLATE_PATH . '/language');

/* CONTENT FUNCTIONS
================================================== */
if (!function_exists('ct_custom_content')) {
    function ct_custom_content_functions()
    {
        include_once CT_INCLUDES_PATH . '/ct-header.php';
        include_once CT_INCLUDES_PATH . '/ct-blog.php';
        include_once CT_INCLUDES_PATH . '/ct-portfolio.php';
        include_once CT_INCLUDES_PATH . '/ct-products.php';
        include_once CT_INCLUDES_PATH . '/ct-post-formats.php';
    }

    add_action('init', 'ct_custom_content_functions', 0);
}

/* Cootheme
================================================== */
if (!function_exists('ct_include_framework')) {
    function ct_include_framework()
    {
        require_once CT_INCLUDES_PATH . '/ct-theme-functions.php';
        require_once CT_INCLUDES_PATH . '/ct-comments.php';
        require_once CT_INCLUDES_PATH . '/ct-formatting.php';
        require_once CT_INCLUDES_PATH . '/ct-media.php';
        require_once CT_INCLUDES_PATH . '/ct-menus.php';
        require_once CT_INCLUDES_PATH . '/ct-pagination.php';
        require_once CT_INCLUDES_PATH . '/ct-sidebars.php';
        require_once CT_INCLUDES_PATH . '/ct-customizer-options.php';
        include_once CT_INCLUDES_PATH . '/ct-custom-styles.php';
        include_once CT_INCLUDES_PATH . '/ct-stylecootheme/ct-stylecootheme.php';
        require_once CT_FRAMEWORK_PATH . '/coo-theme.php';
    }

    add_action('init', 'ct_include_framework', 0);
}

/* THEME OPTIONS FRAMEWORK
================================================== */
require_once CT_INCLUDES_PATH . '/ct-colour-scheme.php';
if (!function_exists('ct_include_theme_options')) {
    function ct_include_theme_options()
    {
        require_once CT_INCLUDES_PATH . '/ct-options.php';
    }

    add_action('after_setup_theme', 'ct_include_theme_options', 0);
}

/* Cootheme Assets
================================================== */
require_once CT_INCLUDES_PATH . '/ct-assets.php';
global $CTAssets;
$CTAssets = new CT_Assets();

/* LOAD STYLESHEETS
================================================== */
if (!function_exists('ct_enqueue_styles')) {
    function ct_enqueue_styles()
    {

        $options = get_option('ct_coo_options');

        wp_register_style('bootstrap', CT_LOCAL_PATH . '/css/bootstrap.min.css', [], null, 'all');
        wp_register_style('fontawesome', CT_LOCAL_PATH . '/css/font-awesome.min.css', [], null, 'all');
        wp_register_style('ssgizmo', CT_LOCAL_PATH . '/css/ss-gizmo.css', [], null, 'all');
        wp_register_style('ct-owlcarousel', CT_LOCAL_PATH . '/css/owl.carousel/owl.carousel.css', [], null, 'all');
        wp_register_style('ct-owltheme', CT_LOCAL_PATH . '/css/owl.carousel/owl.theme.css', [], null, 'all');
        wp_register_style('ct-fancybox', CT_LOCAL_PATH . '/css/fancybox/jquery.fancybox.css', [], null, 'all');
        wp_register_style('ct-magnific-popup', CT_LOCAL_PATH . '/css/magnific-popup.css', [], null, 'all');
        wp_register_style('ct-fancybox-thumbs', CT_LOCAL_PATH . '/css/fancybox/jquery.fancybox-thumbs.css', [], null, 'all');
        wp_register_style('ct-main', get_stylesheet_directory_uri() . '/style.css', [], null, 'all');
        wp_register_style('stylesheet-site', get_stylesheet_directory_uri() . '/css/stylesheet.css', [], null, 'all');
        wp_register_style('ct-responsive', CT_LOCAL_PATH . '/css/responsive.css', [], null, 'all');
        wp_register_style('choijun-responsive', CT_LOCAL_PATH . '/css/choijun-responsive.css', [], null, 'all');

        // register slick scss
        wp_register_style('slick-css', CT_LOCAL_PATH . '/css/slick.css', [], null, 'all');
        wp_register_style('slick-theme-css', CT_LOCAL_PATH . '/css/slick-theme.css', [], null, 'all');

        wp_register_style('unite-gallery-css', CT_LOCAL_PATH . '/css/unite-gallery.css', [], null, 'all');
        wp_register_style('unite-gallery-theme', CT_LOCAL_PATH . '/css/ug-theme-default.css', [], null, 'all');

        wp_enqueue_style('bootstrap');
        wp_enqueue_style('ssgizmo');
        wp_enqueue_style('fontawesome');
        wp_enqueue_style('ct-owlcarousel');
        wp_enqueue_style('ct-owltheme');
        wp_enqueue_style('ct-fancybox');
        wp_enqueue_style('ct-magnific-popup');
        wp_enqueue_style('ct-fancybox-thumbs');
        wp_enqueue_style('ct-main');
        wp_enqueue_style('stylesheet-site');
        wp_enqueue_style('slick-css');
        wp_enqueue_style('slick-theme-css');
        wp_enqueue_style('unite-gallery-css');
        wp_enqueue_style('unite-gallery-theme');

        global $CTAssets;

        $CTAssets->add_style(CT_LOCAL_PATH . '/css/bootstrap.css');
        $CTAssets->add_style(CT_LOCAL_PATH . '/css/ss-gizmo.css');
        $CTAssets->add_style(CT_LOCAL_PATH . '/css/owl.carousel/owl.carousel.css');
        $CTAssets->add_style(CT_LOCAL_PATH . '/css/owl.carousel/owl.theme.css');
        $CTAssets->add_style(CT_LOCAL_PATH . '/css/font-awesome.min.css');
        $CTAssets->add_style(get_stylesheet_directory_uri() . '/style.css');

        wp_enqueue_style('ct-responsive');
        $CTAssets->add_style(CT_LOCAL_PATH . '/css/responsive.css');
        wp_enqueue_style('choijun-responsive');
        $CTAssets->add_style(CT_LOCAL_PATH . '/css/choijun-responsive.css');

        $CTAssets->minify_style(CT_TEMPLATE_PATH . '/css/site.style.css');
        wp_register_style('ct-site-style', get_stylesheet_directory_uri() . '/css/site.style.css', [], null, 'all');
        //wp_enqueue_style('ct-site-style');
    }

    add_action('wp_enqueue_scripts', 'ct_enqueue_styles', 99);
}

/* LOAD FRONTEND SCRIPTS
================================================== */
if (!function_exists('ct_enqueue_scripts')) {
    function ct_enqueue_scripts()
    {

        global $is_IE;
        global $CTAssets;

        wp_register_script('ct-bootstrap-js', CT_LOCAL_PATH . '/js/bootstrap.min.js', 'jquery', null, true);
        wp_register_script('ct-flexslider', CT_LOCAL_PATH . '/js/jquery.flexslider-min.js', 'jquery', null, true);
//            wp_register_script('ct-isotope', CT_LOCAL_PATH . '/js/jquery.isotope.min.js', 'jquery', NULL, TRUE);
        wp_register_script('ct-imagesLoaded', CT_LOCAL_PATH . '/js/imagesloaded.js', 'jquery', null, true);
        wp_register_script('ct-easing', CT_LOCAL_PATH . '/js/jquery.easing.js', 'jquery', null, true);
        wp_register_script('ct-owlcarousel', CT_LOCAL_PATH . '/js/owl.carousel.js', 'jquery', null, true);

        wp_register_script('slick-js', CT_LOCAL_PATH . '/js/slick.min.js', 'jquery', null, true);

        wp_register_script('unitegallery-js', CT_LOCAL_PATH . '/js/unitegallery.min.js', 'jquery', null, true);
        wp_register_script('ug-theme-default-js', CT_LOCAL_PATH . '/js/ug-theme-default.js', 'jquery', null, true);

        wp_register_script('ct-sticky', CT_LOCAL_PATH . '/js/jquery.sticky-kit.min.js', 'jquery', null, true);

        wp_register_script('ct-bxslider', CT_LOCAL_PATH . '/js/jquery.bxslider.min.js', 'jquery', null, true);
        wp_register_script('ct-fancybox', CT_LOCAL_PATH . '/js/jquery.fancybox.js', 'jquery', null, true);
        wp_register_script('ct-fancybox-thumbs', CT_LOCAL_PATH . '/js/jquery.fancybox-thumbs.js', 'jquery', null, true);
        wp_register_script('ct-jquery.magnific-popup.min', CT_LOCAL_PATH . '/js/jquery.magnific-popup.min.js', 'jquery', null, true);
        wp_register_script('ct-jquery-ui', CT_LOCAL_PATH . '/js/jquery-ui-1.10.2.custom.min.js', 'jquery', null, true);
        wp_register_script('ct-viewjs', CT_LOCAL_PATH . '/js/view.min.js?auto', 'jquery', null, true);
        wp_register_script('ct-fitvids', CT_LOCAL_PATH . '/js/jquery.fitvids.js', 'jquery', null, true);
        wp_register_script('ct-maps', '//maps.google.com/maps/api/js?sensor=false', 'jquery', null, true);
        wp_register_script('ct-respond', CT_LOCAL_PATH . '/js/respond.min.js', '', null, false);
        wp_register_script('ct-html5shiv', CT_LOCAL_PATH . '/js/html5shiv.js', '', null, false);
        wp_register_script('ct-excanvas', CT_LOCAL_PATH . '/js/excanvas.compiled.js', '', null, false);
        wp_register_script('ct-elevatezoom', CT_LOCAL_PATH . '/js/jquery.elevateZoom.min.js', 'jquery', null, true);
        wp_register_script('ct-infinite-scroll', CT_LOCAL_PATH . '/js/jquery.infinitescroll.min.js', 'jquery', null, true);
        wp_register_script('ct-theme-scripts', CT_LOCAL_PATH . '/js/theme-scripts.js', 'jquery', null, true);
        wp_register_script('ct-functions', CT_LOCAL_PATH . '/js/functions.js', 'jquery', null, true);
        wp_register_script('ct-jquery-noconflict', CT_LOCAL_PATH . '/js/jquery.noconflict.js', 'jquery', null, true);
        wp_register_script('widgets', CT_LOCAL_PATH . '/js/widgets.js', 'jquery', null, true);

        wp_register_script('ctmain', CT_LOCAL_PATH . '/js/main.js', 'jquery', null, true);

        if ($is_IE) {
            wp_enqueue_script('ct-respond');
            wp_enqueue_script('ct-html5shiv');
            wp_enqueue_script('ct-excanvas');
            //$CTAssets->add_script(CT_LOCAL_PATH . '/js/respond.min.js');
            //$CTAssets->add_script(CT_LOCAL_PATH . '/js/html5shiv.js');
            //$CTAssets->add_script(CT_LOCAL_PATH . '/js/excanvas.compiled.js');
        }

        wp_enqueue_script('jquery');
        wp_enqueue_script('ct-jquery-noconlict');
        wp_enqueue_script('ct-bootstrap-js');
        wp_enqueue_script('ct-jquery-ui');
        wp_enqueue_script('ct-flexslider');
        wp_enqueue_script('ct-easing');
        wp_enqueue_script('ct-fitvids');
        wp_enqueue_script('ct-owlcarousel');

        wp_enqueue_script('ct-sticky');

        wp_enqueue_script('ct-bxslider');
        wp_enqueue_script('ct-fancybox');
        wp_enqueue_script('ct-jquery.magnific-popup.min');
        wp_enqueue_script('ct-fancybox-thumbs');
        wp_enqueue_script('ct-theme-scripts');
        wp_enqueue_script('widgets');
        wp_enqueue_script('slick-js');

        wp_enqueue_script('unitegallery-js');
        wp_enqueue_script('ug-theme-default-js');

        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/bootstrap.min.js');
        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/jquery-ui-1.10.2.custom.min.js');
        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/jquery.flexslider-min.js');
        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/jquery.easing.js');
        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/jquery.fitvids.js');
        //            $CTAssets->add_script(CT_LOCAL_PATH . '/js/jquery.bxslider.min.js');
        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/theme-scripts.js');

        if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            if (!is_account_page()) {
                wp_enqueue_script('ct-viewjs');
                //$CTAssets->add_script(CT_LOCAL_PATH . '/js/view.min.js');
            }
        } else {
            wp_enqueue_script('ct-viewjs');
            //$CTAssets->add_script(CT_LOCAL_PATH . '/js/view.min.js');
        }

        wp_enqueue_script('ct-maps');
        wp_enqueue_script('ct-isotope');
        wp_enqueue_script('ct-imagesLoaded');
        wp_enqueue_script('ct-infinite-scroll');

//            $CTAssets->add_script('http://maps.google.com/maps/api/js?sensor=false');
        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/jquery.isotope.min.js');
        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/imagesloaded.js');
        //$CTAssets->add_script(CT_LOCAL_PATH . '/js/jquery.infinitescroll.min.js');

        $options = get_option('ct_coo_options');

        if (isset($options['enable_product_zoom'])) {
            $enable_product_zoom = $options['enable_product_zoom'];
            if ($enable_product_zoom) {
                wp_enqueue_script('ct-elevatezoom');
                //$CTAssets->add_script(CT_LOCAL_PATH . '/js/jquery.elevateZoom.min.js');
            }
        }

        if (!is_admin()) {
            //    wp_enqueue_script('ct-functions');
            $CTAssets->add_script(CT_LOCAL_PATH . '/js/functions.js');
        }

        //minify script
        $CTAssets->minify_script(CT_TEMPLATE_PATH . '/js/site.script.js');
        wp_register_script('ct-site-script', CT_LOCAL_PATH . '/js/site.script.js', 'jquery', null, true);
        wp_enqueue_script('ct-site-script');

        wp_enqueue_script('ctmain');

        wp_localize_script('ctmain', 'ctmain', ['ajaxurl' => admin_url('admin-ajax.php')]);

        if (is_singular() && comments_open()) {
            wp_enqueue_script('comment-reply');
        }
    }

    add_action('wp_enqueue_scripts', 'ct_enqueue_scripts');
}

/* LOAD BACKEND SCRIPTS
================================================== */
function ct_admin_scripts()
{
    wp_register_script('admin-functions', get_template_directory_uri() . '/js/ct-admin.js', 'jquery', '1.0', true);
    wp_enqueue_script('admin-functions');
}

add_action('admin_init', 'ct_admin_scripts');

/* LAYERSLIDER OVERRIDES
================================================== */
function ct_layerslider_overrides()
{
    // Disable auto-updates
    $GLOBALS['lsAutoUpdateBox'] = false;
}

add_action('layerslider_ready', 'ct_layerslider_overrides');

function wt_get_category_count($input = '')
{
    global $wpdb;
    if ($input == '') {
        $category = get_the_category();
        return $category[0]->category_count;
    } elseif (is_numeric($input)) {
        $SQL = "SELECT $wpdb->term_taxonomy.count FROM $wpdb->terms, $wpdb->term_taxonomy WHERE $wpdb->terms.term_id=$wpdb->term_taxonomy.term_id AND $wpdb->term_taxonomy.term_id=$input";
        return $wpdb->get_var($SQL);
    } else {
        $SQL = "SELECT $wpdb->term_taxonomy.count FROM $wpdb->terms, $wpdb->term_taxonomy WHERE $wpdb->terms.term_id=$wpdb->term_taxonomy.term_id AND $wpdb->terms.slug='$input'";
        return $wpdb->get_var($SQL);
    }
}

//ajax
add_action('wp_ajax_Show_post_next', 'Show_post_next');
add_action('wp_ajax_nopriv_Show_post_next', 'Show_post_next');
function Show_post_next()
{

    $checkid  = $_POST["sid"];
    $checktax = $_POST["stax"];

    $querytiep = new WP_Query([
        'post_type'      => 'testimonials',
        'tax_query'      => [
            [
                'taxonomy' => 'testimonials-category',
                'field'    => 'id',
                'terms'    => $checktax,
                'operator' => 'IN',
            ]],
        'posts_per_page' => 5,
        'offset'         => $checkid,
    ]);

    while ($querytiep->have_posts()): $querytiep->the_post();

        $testimonial_text      = get_the_content();
        $testimonial_name      = get_post_meta(get_the_ID(), 'ct_testimonial_name', true);
        $testimonial_number    = get_post_meta(get_the_ID(), 'ct_testimonial_number', true);
        $testimonial_address   = get_post_meta(get_the_ID(), 'ct_testimonial_address', true);
        $testimonial_tour_name = get_post_meta(get_the_ID(), 'ct_testimonial_tour_name', true);
        $testimonial_datetime  = get_post_meta(get_the_ID(), 'ct_testimonial_datetime', true);
        $testimonial_email     = get_post_meta(get_the_ID(), 'ct_testimonial_email', true);
        $testimonial_tour_type = get_post_meta(get_the_ID(), 'ct_testimonial_tour_type', true);
        $testimonial_image     = rwmb_meta('ct_testimonial_image', 'type=image', get_the_ID());

        $post_format = get_post_format($post->ID);
        if ($post_format == "") {
            $post_format = 'standard';
        }?>
	    <li <?php post_class('brand-item  format-' . $post_format);?>>

	        <div class="testimonial-wrap clearfix">
	            <?php if ($testimonial_name) {?>
	                <div class="information-tesimonial pull-left">
	                    <div class="ts-name"><?php echo __('Monsieur: ', TEXT_DOMAIN); ?><?php echo $testimonial_name; ?> (<?php echo __("groupe " . $testimonial_number . " personnes", TEXT_DOMAIN); ?>)</div>
	                    <div class="ts-address"><?php echo __('Adresse: ' . $testimonial_address, TEXT_DOMAIN); ?></div>
	                    <div class="ts-tour-name"><?php echo __('Circuit: ' . $testimonial_tour_name, TEXT_DOMAIN); ?></div>
	                    <div class="ts-tour-type"><?php echo __('Type de circuit: ' . $testimonial_tour_type, TEXT_DOMAIN); ?></div>
	                    <div class="ts-date"><?php echo __('Voyage: ' . $testimonial_datetime, TEXT_DOMAIN); ?></div>
	                </div>

	                <div class="ts-mail pull-right">
	                    <div class="image-ts-email"><a class="send-email-click" href="mailto:<?php echo antispambot($testimonial_email); ?>"><img src="<?php echo CT_LOCAL_PATH . '/images/email.png'; ?>" alt="img-email"/></a></div>
	                    <a href="mailto:<?php echo antispambot($testimonial_email); ?>"><?php echo __("cliquez ce lien pour contacter  <b>" . $testimonial_name . "</b>", TEXT_DOMAIN) ?></a>
	                </div>

	            <?php }?>
	        </div>

	        <div class="mini-testimonial-item-wrap">
	            <figure class="animated-overlay information-img overlay-alt faqs-img">

	                    <a href="<?php echo get_permalink(); ?>">
	                        <?php echo the_post_thumbnail(); ?>
	                    </a>

	            </figure>
	            <div class="testimonial-details-wrap">
	                <h3 itemprop="name headline" class="title text-left hidden">
	                    <a href="<?php echo get_permalink(); ?>"><?php echo the_title(); ?></a>
	                </h3>
	                <div itemprop="description" class="excerpt">
	                    <?php echo the_content(); ?>
	                </div>

	            </div>
	        </div>

	    </li><?php

    endwhile;

    die();
}

/* BETTER SEO PAGE TITLE
================================================== */
if (!function_exists('ct_filter_wp_title')) {
    function ct_filter_wp_title($title)
    {
        global $page, $paged, $post;

        if (is_feed()) {
            return $title;
        }

        if (is_archive()) {
            $term_slug    = get_query_var('term');
            $taxonomyName = get_query_var('taxonomy');
            $current_term = get_term_by('slug', $term_slug, $taxonomyName);
            $term_id      = $current_term->term_id;

            $cat_data = get_option("tag_$term_id");

            $seo_title = $cat_data['seo_met_title'];

            if ($seo_title) {
                $title = $seo_title . ' | ' . get_bloginfo("name");
                return $title;
            } else {
                $current_category = single_cat_title("", false);
                $title            = $current_category . ' | ' . get_bloginfo("name");
                return $title;
            }
        }
        if (is_single()) {
            $custom_title = get_post_meta($post->ID, 'ct_topbar_title', true);
            $topbar_title = $custom_title ? $custom_title : $post->title;

            $title = $topbar_title . ' | ' . get_bloginfo('name');

            return $title;
        }
        return $title;
    }

    add_filter('wp_title', 'ct_filter_wp_title', 1001);
}

include_once trailingslashit(get_template_directory()) . 'includes/custom_include.php';

//remove slug
//function gp_remove_cpt_slug( $post_link, $post, $leavename ) {
//    if ( 'levietnam' != $post->post_type && 'guide' != $post->post_type && 'aboutus' != $post->post_type && 'book' != $post->post_type &&'embassies' != $post->post_type && 'responsibility' != $post->post_type && 'special' != $post->post_type && 'testimonials' != $post->post_type || 'publish' != $post->post_status ) {
//        return $post_link;
//    }
//    var_dump($post_link);die;
//    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
//
//    return $post_link;
//}
//add_filter( 'post_type_link', 'gp_remove_cpt_slug', 10, 3 );
//
//function gp_parse_request_trick( $query ) {
//
//    // Only noop the main query
//    if ( ! $query->is_main_query() )
//        return;
//
//    // Only noop our very specific rewrite rule match
//    if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
//        return;
//    }
//
//    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
//    if ( ! empty( $query->query['name'] ) ) {
//        $query->set( 'post_type', array( 'post', 'levietnam','guide','aboutus','book','embassies','responsibility','testimonials','special','page' ) );
//    }
//}
//add_action( 'pre_get_posts', 'gp_parse_request_trick' );

//-----------------------------------------------

function zAddCss()
{
    wp_register_style('custom-style', get_template_directory_uri() . '/css/z-vicoders.css', [], '20120208', 'all');
    wp_enqueue_style('custom-style');
}

add_action('wp_enqueue_scripts', 'zAddCss', 1000);

function widget_for_security()
{
    register_sidebar([
        'name'          => __('Security footer 1', 'daihochalong'),
        'id'            => 'security-footer-1',
        'description'   => __('Thêm cột 1 trên footer', 'galaxy'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ]);

    register_sidebar([
        'name'          => __('Security footer 2', 'daihochalong'),
        'id'            => 'security-footer-2',
        'description'   => __('Thêm cột 2 trên footer', 'galaxy'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ]);

    register_sidebar([
        'name'          => __('Security footer 3', 'daihochalong'),
        'id'            => 'security-footer-3',
        'description'   => __('Thêm cột 3 trên footer', 'galaxy'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ]);

    register_sidebar([
        'name'          => __('Security footer 4', 'daihochalong'),
        'id'            => 'security-footer-4',
        'description'   => __('Thêm cột 4 trên footer', 'galaxy'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ]);
}

add_action('widgets_init', 'widget_for_security');

// test
// Add custom validation for CF7 form fields

// function is_company_email($email)
// {
//     if (preg_match('/@email\.ru$/i', $email)) {
//         return false; 
//     } else {
//         return true; 
//     }
// }

// function custom_email_validation_filter($result, $tag)
// {
//     $tag = new WPCF7_Shortcode($tag);
//     // if ( 'company-email' == $tag->name ) {
//     $the_value = isset($_POST['email']) ? trim($_POST['email']) : '';
//     if (!is_company_email($the_value)) {
//         $result->invalidate($tag, "Désolé, tout email se terminant par (.ru) n’est pas valide avec notre site. Merci d’essayer avec un autre email.");
//     }
//     // }
//     return $result;
// }

// add_filter('wpcf7_validate_email', 'custom_email_validation_filter', 10, 2);
// add_filter('wpcf7_validate_email*', 'custom_email_validation_filter', 10, 2);

// for textarea

function count_link_in_textarea($content)
{
    // preg_match_all('/(http|\.ru)/', $content, $matched);
	preg_match_all('/(http)/', $content, $matched);
	return count($matched[1]);
}

function custom_textarea_validation_filter($result, $tag)
{
    $tag       = new WPCF7_Shortcode($tag);
    $room_others = isset($_POST['room-others']) ? trim($_POST['room-others']) : '';
    $count_room_others = count_link_in_textarea($room_others);

    $f_requirements = isset($_POST['f-requirements']) ? trim($_POST['f-requirements']) : '';
    $count_f_requirements = count_link_in_textarea($f_requirements);

    $message = 'Désolé, ce champs ci-dessus n’accepte aucun lien du site d’Internet, merci d’enlever ce lien et réessayez!';

    if ( 'room-others' == $tag->name && $count_room_others >= 1) {
        $result->invalidate($tag, $message);
        return $result;
    }
    if ( 'f-requirements' === $tag->name && $count_f_requirements >= 1) {
        $result->invalidate($tag, $message);
        return $result;
    }
    return $result;
}

add_filter('wpcf7_validate_textarea', 'custom_textarea_validation_filter', 10, 2);
add_filter('wpcf7_validate_textarea*', 'custom_textarea_validation_filter', 10, 2);