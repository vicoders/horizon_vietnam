<?php get_header(); ?>

<?php

	$options = get_option('ct_coo_options');
	$sidebar_config = $options['archive_sidebar_config'];
    $config =configLayout(of_get_option('single-layout',$sidebar_config));
	$left_sidebar = $options['archive_sidebar_left'];
	$right_sidebar = $options['archive_sidebar_right'];

	global $ct_has_blog;
	$ct_has_blog = true;

?>

<?php if ( is_front_page() || is_home() ) : ?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>
	<div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-page coo-main <?php echo $config['main']['class'];?> clearfix">
                <div class="page-content clearfix">
                    <div class="page-content-inner">
                    <h1 class="heading-text"><?php single_cat_title(); ?></h1>
                    <?php if(have_posts()) : ?>

                        <div class="blog-wrap">

                            <!-- OPEN .blog-items -->
                            <ul class="blog-items mini-items clearfix" id="blogGrid">

                                <?php while (have_posts()) : the_post(); ?>

                                    <?php
                                        $post_format = get_post_format($post->ID);
                                        if ( $post_format == "" ) {
                                            $post_format = 'standard';
                                        }
                                    ?>
                                    <li <?php post_class('blog-item  format-'.$post_format); ?>>
                                        <?php echo ct_get_post_item($post->ID, 'mini'); ?>
                                    </li>

                                <?php endwhile; ?>

                            <!-- CLOSE .blog-items -->
                            </ul>

                        </div>

                        <?php else: ?>
                        <h3><?php _e("Sorry, there are no posts to display.", "cootheme"); ?></h3>
                        <?php endif; ?>

                    <div class="pagination-wrap">
                        <?php echo pagenavi($wp_query); ?>
                    </div>
                    </div>
                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->
            <?php if($config['left-sidebar']['show']){ ?>
                <div class="left-sidebar <?php echo $config['left-sidebar']['class']; ?>">
                        <div class="sidebar-inner">
                            <?php dynamic_sidebar($left_sidebar); ?>
                        </div>
                </div>
            <?php } ?>
            <!--End.Sidebar-Left -->

            <!--Start.Sidebar-Right-->
            <?php if($config['right-sidebar']['show']){ ?>
                <div class=" right-sidebar <?php echo $config['right-sidebar']['class']; ?>">
                        <div class="sidebar-inner">
                            <?php dynamic_sidebar($right_sidebar); ?>
                        </div>
                </div>
            <?php } ?>
            <!--End.Sidebar-right -->
        </div>
	</div>

<?php endif; ?>

<!--// WordPress Hook //-->
<?php get_footer(); ?>