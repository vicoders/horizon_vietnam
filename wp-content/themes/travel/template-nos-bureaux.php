<?php
/**
 * Template Name: Nos Bureaux
 */
?>

<?php get_header(); ?>

<?php
$remove_breadcrumbs = get_post_meta($post->ID, 'ct_no_breadcrumbs', true);

    $title_image_url="";
    $title_image = rwmb_meta('ct_title_image', 'type=image&size=img-title');
    if (is_array($title_image)) {
        foreach ($title_image as $image) {
            $title_image_url = $image['url'];
            break;
        }
    }
?>
    <?php if($title_image_url){
        ?>
        <div class="row">
            <div class="title-image col-md-12">
                <?php echo $image = '<img itemprop="image" src="'.$title_image_url.'" alt="'.$image_title.'" />';?>
            </div>
        </div>
    <?php
    }?>

    <div class="row">
        <div class="page-heading col-sm-12 clearfix  <?php echo $page_title_bg; ?>">
            <?php
            // BREADCRUMBS
            if (!$remove_breadcrumbs) {
                echo ct_breadcrumbs();
            }
            ?>
        </div>
    </div>
    <div class="inner-page-wrap row clearfix">
        <?php if (have_posts()) : the_post(); ?>
            <!-- Start.Main -->
            <div class="page-content coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">

                <div class="page-content-inner">
                    <h1 class="title-page"><?php the_title();?></h1>
                    <div class="content-text"><?php the_content(); ?></div>
                    <?php 

                    $args = [
                        'post_type' => 'office',
                        'posts_per_page' => -1,
                        'post_status' => 'publish',
                    ];

                    $offices = get_posts($args);

                    if (!empty($offices)) {

                        echo '<div class="list-offices">';

                        echo '<div class="dashline"></div>';

                        foreach ($offices as $key_1 => $office) {
                            $officeId = $office->ID;
                            $officeTitle = $office->post_title;
                            
                            if (has_post_thumbnail($officeId)) {
                                $officeImage = wp_get_attachment_image_src(get_post_thumbnail_id($officeId))[0];
                            } else {
                                $officeImage = get_stylesheet_directory_uri() . '/images/default-thumb.png';
                            }

                            $address = get_field('address', $officeId);
                            $map = get_field('map', $officeId);
                            $gallery = get_field('gallery', $officeId);

                            //var_dump($gallery);

                            ?>
                            <div class="row">
                                <h3><?php echo $officeTitle; ?></h3>
                                <div class="office-info">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="row">
                                                <div class="col-md-4 office-image">
                                                    <img src="<?php echo $officeImage; ?>" atl="offices" />
                                                    <p><?php //_e('Cliquez sur la photo pour visiter notre bureau.', 'textdomain'); ?></p>
                                                </div>
                                                <div class="col-md-8">
                                                    <?php echo $address; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 office-map">
                                            <?php echo $map; ?>
                                        </div>
                                    </div>    
                                </div>

                                <?php
                                if (!empty($gallery)) {

                                    echo '<div class="office-gallery" id="gallery_' . $officeId . '" style="display:none;">';

                                    foreach ($gallery as $key => $image) {
                                        // echo '<pre>';
                                        // var_dump($image);
                                        ?>
                                            <img alt="Preview Image 1"
                                                 src="<?php echo $image['sizes']['thumbnail']; ?>"
                                                 data-image="<?php echo $image['url']; ?>"
                                                 data-description="<?php echo $image['title']; ?>" />
                                        <?php
                                    }

                                    echo '</div>';

                                    if ($key_1 < count($offices) - 1) {
                                        echo '<div class="dashline"></div>';
                                    }
                                }
                                ?>
                                <script type="text/javascript">
                                    jQuery(document).ready(function(){
                                        jQuery('#gallery_<?php echo $officeId; ?>').unitegallery();
                                    });
                                    
                                </script>
                            </div>
                            <?php
                        }

                        echo '</div>';
                    }

                    ?>

                    <div class="category-bottom clearfix">
                        <div class="pull-right LikeButton clearfix">
                            <!-- Facebook Button -->
                            <div class="FacebookButton">
                                <div id="fb-root"></div>
                                <script type="text/javascript">
                                    (function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) {
                                            return;
                                        }
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/en_US/all.js#appId=177111755694317&xfbml=1";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                </script>
                                <div class="fb-like" data-send="false" data-width="200" data-show-faces="true"
                                     data-layout="button_count" data-href="<?php the_permalink(); ?>"></div>
                            </div>
                            <!-- Twitter Button -->
                            <div class="TwitterButton">
                                <a href="<?php the_permalink(); ?>" class="twitter-share-button"
                                   data-count="horizontal" data-via="" data-size="small">
                                </a>
                            </div>
                            <!-- Google +1 Button -->
                            <div class="GooglePlusOneButton">
                                <!-- Place this tag where you want the +1 button to render -->
                                <div class="g-plusone" data-size="medium"
                                     data-href="<?php the_permalink(); ?>"></div>
                                <!-- Place this render call where appropriate -->
                                <script type="text/javascript">
                                    (function () {
                                        var po = document.createElement('script');
                                        po.type = 'text/javascript';
                                        po.async = true;
                                        po.src = 'https://apis.google.com/js/plusone.js';
                                        var s = document.getElementsByTagName('script')[0];
                                        s.parentNode.insertBefore(po, s);
                                    })();
                                </script>

                            </div>
                            <!-- Pinterest Button -->
                            <div class="PinterestButton">
                                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>&description=<?php the_title(); ?>"
                                   data-pin-do="buttonPin" data-pin-config="beside">
                                    <img class="pinterest"
                                         src="//assets.pinterest.com/images/pidgets/pin_it_button.png"/>
                                </a>
                                <script type="text/javascript">
                                    (function (d) {
                                        var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
                                        p.type = 'text/javascript';
                                        p.async = true;
                                        p.src = '//assets.pinterest.com/js/pinit.js';
                                        f.parentNode.insertBefore(p, f);
                                    }(document));
                                </script>
                            </div>
                            <!-- Linkedin Button -->
                            <div class="LinkedinButton">
                                <script type="IN/Share" data-url="<?php the_permalink(); ?>"
                                        data-counter="right"></script>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End.Main -->
        <?php endif; ?>

        <div class="left-sidebar  col-xs-12 col-sm-3 col-sm-pull-9 col-md-3 ">
            <div class="sidebar-inner">
                <?php dynamic_sidebar('sidebar_aboutus'); ?>
            </div>
        </div>

             <!--End.Sidebar-right -->
    </div>



<!--// WordPress Hook //-->
<?php get_footer(); ?>