<?php get_header(); ?>

<?php
$testimonial_text = get_the_content();
$testimonial_name = get_post_meta($post->ID, 'ct_testimonial_name', true);
$testimonial_number = get_post_meta($post->ID, 'ct_testimonial_number', true);
$testimonial_address = get_post_meta($post->ID, 'ct_testimonial_address', true);
$testimonial_tour_name = get_post_meta($post->ID, 'ct_testimonial_tour_name', true);
$testimonial_datetime = get_post_meta($post->ID, 'ct_testimonial_datetime', true);
$testimonial_email = get_post_meta($post->ID, 'ct_testimonial_email', true);
$testimonial_tour_type = get_post_meta($post->ID, 'ct_testimonial_tour_type', true);
$testimonial_image = rwmb_meta('ct_testimonial_image', 'type=image', $post->ID);
if (have_posts()) : the_post();
    //MEDIA TYPE
    $media_type = $media_image = $media_video = $media_gallery = '';

    $use_thumb_content = get_post_meta($post->ID, 'ct_thumbnail_content_main_detail', true);


    $post_format = get_post_format($post->ID);
    if ( $post_format == "" ) {
        $post_format = 'standard';
    }

    if ($use_thumb_content) {
        $media_type = get_post_meta($post->ID, 'ct_thumbnail_type', true);
    } else {
        $media_type = get_post_meta($post->ID, 'ct_detail_type', true);
    }


    $media_width = 770;
    $media_height = NULL;
    $video_height = 433;

    $figure_output = '';

    $figure_output .= '<figure class="media-wrap-brand " >';
    if ($post_format == "standard") {

        if ($media_type == "video") {

            $figure_output .= ct_video_post($post->ID, $media_width,$media_height, $use_thumb_content)."\n";

        } else if ($media_type == "slider") {

            $figure_output .= ct_gallery_post($post->ID, $use_thumb_content)."\n";

        } else if ($media_type == "layer-slider") {

            $figure_output .= '<div class="layerslider">'."\n";

            $figure_output .= do_shortcode('[rev_slider '.$media_slider.']')."\n";

            $figure_output .= '</div>'."\n";

        } else if ($media_type == "custom") {

            $figure_output .= $custom_media."\n";

        } else {

            $figure_output .= ct_image_post($post->ID, $media_width,$media_height, $use_thumb_content)."\n";

        }

    }
    else {

        $figure_output .= ct_get_post_media($post->ID, $media_width, $media_height, $video_height, $use_thumb_content);

    }

    $figure_output .= '</figure>'."\n";

    ?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>
    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-post coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">

                <div class="post-content clearfix">
                    <div class="testimonial-wrap clearfix">
                        <?php if($testimonial_name){?>
                            <div class="information-tesimonial pull-left">
                                <div class="ts-name"><?php echo __('Monsieur: ',TEXT_DOMAIN); ?><?php echo $testimonial_name;?> (<?php echo __("groupe ".$testimonial_number." personnes",TEXT_DOMAIN);?>)</div>
                                <div class="ts-address"><?php echo __('Addresse: '.$testimonial_address,TEXT_DOMAIN);?></div>
                                <div class="ts-tour-name"><?php echo __('Voyage: '.$testimonial_tour_name,TEXT_DOMAIN);?></div>
                                <div class="ts-tour-type"><?php echo __('Type de circuit: '.$testimonial_tour_type,TEXT_DOMAIN);?></div>
                                <div class="ts-date"><?php echo __('Voyage: '.$testimonial_datetime,TEXT_DOMAIN);?></div>
                            </div>
                            <div class="ts-mail pull-right">
                                <div class="image-ts-email"><a href="mailto:<?php echo $testimonial_email;?>"><img src="<?php echo CT_LOCAL_PATH.'/images/email.png';?>" alt="img-email"/></a></div>
                                <a href="mailto:<?php echo $testimonial_email;?>"><?php echo __("cliquez ce lien pour contacter  <b>".$testimonial_name."</b>",TEXT_DOMAIN)?></a>
                            </div>
                        <?php }?>
                    </div>
                    <?php echo $figure_output;?>
                    <!--                    Start.Article -->
                    <section class="article-body-wrap">
                        <h3 class="single-title">
                            <?php the_title(); ?>
                        </h3>
                        <div class="body-text clearfix">
                            <?php the_content(); ?>
                        </div>
                    </section>


                    <?php if ( comments_open() ) { ?>
                        <div id="comment-area">
                            <?php comments_template('', true); ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <!-- End.Main -->
            <!--Start.Sidebar-Left-->

            <div class="left-sidebar col-xs-12 col-sm-3 col-sm-pull-9 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_aboutus'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-Left -->
        </div>
    </div>
<?php endif; ?>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>