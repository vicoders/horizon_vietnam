<?php get_header(); ?>

<?php

$options = get_option('ct_coo_options');

global $ct_has_blog, $wp_query;
$ct_has_blog = true;
$image_title = ct_featured_img_title();

$term_slug = get_query_var( 'term' );
$taxonomyName = get_query_var( 'taxonomy' );
$current_term = get_term_by( 'slug', $term_slug, $taxonomyName );
// echo "<pre>";
// var_dump($term_slug, $taxonomyName, $current_term) ; die;
$eliminate = [
    'nos-voyageurs-parlent'
];

if(in_array($term_slug, $eliminate)) {
    $flag_term = true;
} else {
    $flag_term = false;
}
$term_id = $current_term->term_id;
$cat_data = get_option("tag_$term_id");
$seo_title=$cat_data['seo_met_title'];
?>

    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>

    <div class="inner-page-wrap clearfix">
        <div class="row" data-sticky_parent>
            <!-- Start.Main -->
            <div class="archive-page  coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">

                <div class="page-content clearfix">
                    <div class="page-content-inner">
                        <div class="head-page">
                            <h1 class="heading-text">
                                <?php  if($seo_title){
                                    echo $seo_title;
                                }

                                else{
                                    single_cat_title();
                                } ?>
                            </h1>
                            <div class="desception-category"><?php echo category_description(); ?></div>
                        </div>


<?php 
    $countshowpost = 0;
    $querytiep =  new WP_Query( array(
        'post_type' => 'testimonials',
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'testimonials-category',
                                    'field' => 'id',
                                    'terms' => $term_id,
                                    'operator'=> 'IN'
                             )),
        'posts_per_page' => 5,
        'offset' => 0,
    ) );
?>

                        <?php 
                            if($querytiep->have_posts()) : 
                            // if(have_posts()) : 
                        ?>

                            <div class="blog-wrap blog-items-wrap">

                                <!-- OPEN .blog-items -->
                                <ul class="testimonial-items mini-items clearfix tiep-infinite-scroll" id="blogGrid" data-flagterm="<?php echo $flag_term;?>" data-show="0" data-taxtestimonials="<?php echo $term_id; ?>" ajax_in_progress="false">

                                    <?php 
                                    while ($querytiep->have_posts() ) : $querytiep->the_post();
                                    // while (have_posts() ) : the_post();

                                    $testimonial_text = get_the_content();
                                    $testimonial_name = get_post_meta($post->ID, 'ct_testimonial_name', true);
                                    $testimonial_number = get_post_meta($post->ID, 'ct_testimonial_number', true);
                                    $testimonial_address = get_post_meta($post->ID, 'ct_testimonial_address', true);
                                    $testimonial_tour_name = get_post_meta($post->ID, 'ct_testimonial_tour_name', true);
                                    $testimonial_datetime = get_post_meta($post->ID, 'ct_testimonial_datetime', true);
                                    $testimonial_email = get_post_meta($post->ID, 'ct_testimonial_email', true);
                                    $testimonial_tour_type = get_post_meta($post->ID, 'ct_testimonial_tour_type', true);
                                    $testimonial_image = rwmb_meta('ct_testimonial_image', 'type=image', $post->ID);

                                    foreach ($testimonial_image as $detail_image) {
                                        $testimonial_image_url = $detail_image['url'];
                                        break;
                                    }

                                    if (!$testimonial_image) {
                                        $testimonial_image = get_post_thumbnail_id();
                                        $testimonial_image_url = wp_get_attachment_url( $testimonial_image, 'full' );
                                    }
                                    $testimonial_image = aq_resize( $testimonial_image_url, 160, 160, true, false);

                                    ?>

                                    <?php
                                    $post_format = get_post_format($post->ID);
                                    if ( $post_format == "" ) {
                                        $post_format = 'standard';
                                    }
                                    ?>
                                    <li <?php post_class('brand-item  format-'.$post_format); ?>>

                                        <div class="testimonial-wrap clearfix">
                                            <?php if($testimonial_name){?>
                                                <div class="information-tesimonial pull-left">
                                                    <div class="ts-name"><?php echo __('Monsieur: ',TEXT_DOMAIN); ?><?php echo $testimonial_name;?> (<?php echo __("groupe ".$testimonial_number." personnes",TEXT_DOMAIN);?>)</div>
                                                    <div class="ts-address"><?php echo __('Adresse: '.$testimonial_address,TEXT_DOMAIN);?></div>
                                                    <div class="ts-tour-name"><?php echo __('Circuit: '.$testimonial_tour_name,TEXT_DOMAIN);?></div>
                                                    <div class="ts-tour-type"><?php echo __('Type de circuit: '.$testimonial_tour_type,TEXT_DOMAIN);?></div>
                                                    <div class="ts-date"><?php echo __('Voyage: '.$testimonial_datetime,TEXT_DOMAIN);?></div>
                                                </div>
                                                <?php if($flag_term == true): ?>
                                                <div class="ts-mail pull-right">
                                                    <div class="image-ts-email"><a class="send-email-click" href="mailto:<?php echo antispambot($testimonial_email); ?>"><img src="<?php echo CT_LOCAL_PATH.'/images/email.png';?>" alt="img-email"/></a></div>
                                                    <a href="mailto:<?php echo antispambot($testimonial_email);?>"><?php echo __("cliquez ce lien pour contacter  <b>".$testimonial_name."</b>",TEXT_DOMAIN)?></a>
                                                </div>
                                                <?php endif; ?>
                                            <?php }?>
                                        </div>

                                        <div class="mini-testimonial-item-wrap">
                                            <figure class="animated-overlay information-img overlay-alt faqs-img">
                                                <?php if($flag_term == true): ?>
                                                    <a href="<?php echo get_permalink(); ?>">
                                                        <?php echo the_post_thumbnail();?>
                                                    </a>
                                                <?php else: ?>
                                                    <?php echo the_post_thumbnail();?>
                                                <?php endif; ?>
                                            </figure>
                                            <div class="testimonial-details-wrap">
                                                <h3 itemprop="name headline" class="title text-left hidden">
                                                    <a href="<?php echo get_permalink(); ?>"><?php echo the_title();?></a>
                                                </h3>
                                                <div itemprop="description" class="excerpt">
                                                    <?php echo the_content();?>
                                                </div>

                                            </div>
                                        </div>

                                        <?php endwhile; ?>
                                     </li>
                                </ul>
                                <div class="ajaxLoad"></div>
                                <!-- <a id="show-post-next" href="javascript:void(0)" data-flagterm="<?php echo $flag_term;?>" data-show="0" data-taxtestimonials="<?php echo $term_id; ?>"><span>Show Post Next</span></a> -->

                            </div>

                        <?php else: ?>
                            <h3><?php _e("Sorry, there are no posts to display.", "cootheme"); ?></h3>
                        <?php endif; ?>

                        <div class="pagination-wrap">
                            <?php 
                            // echo pagenavi($wp_query); 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->

            <div class="left-sidebar col-xs-12 col-sm-3 col-sm-pull-9 col-md-3" >
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_aboutus'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-Left -->
        </div>
    </div>

<script type="text/javascript">
    var $ = jQuery;
    var dem = 0;  
    $(window).scroll(function() {
        var filter_height = $('.tiep-infinite-scroll').height();
        var offset_top = $('.tiep-infinite-scroll').offset().top;
        var window_height = $(window).height();
        var window_scroll_top = $(window).scrollTop();

        if (filter_height + offset_top >= window_scroll_top && filter_height + offset_top <= window_scroll_top + window_height) {  
            var ajax_in_progress = $('.tiep-infinite-scroll').attr('ajax_in_progress');
            if (ajax_in_progress == 'false') {
                var tangid = jQuery('.tiep-infinite-scroll').data("show");
                dem +=1;
                tangid = tangid + 5*dem;
                jQuery('.tiep-infinite-scroll').attr("data-show", tangid);

                var taxtestimonials = jQuery('.tiep-infinite-scroll').data("taxtestimonials");

                $('.tiep-infinite-scroll').attr("ajax_in_progress", 'true');
                
                jQuery.ajax({
                    type: "POST",
                    url: ctmain.ajaxurl,
                    data: {
                        action: 'Show_post_next',
                        sid : tangid,
                        stax : taxtestimonials,
                    },
                    beforeSend:function(){
                        $('.ajaxLoad').show(1);
                    },
                    success:function(response){
                        jQuery('.tiep-infinite-scroll').append(response);
                        $('.tiep-infinite-scroll').attr("ajax_in_progress", 'false');
                        $('.ajaxLoad').hide();
                    }
                });
            }
        } 
    });

    jQuery(document).ready(function($){

        // $(window).resize(function(){
            $check_width_brow = window.innerWidth;
            if($check_width_brow > 991) {
                console.log('lon hon 992');

                $('#widget_service-4,#ct_recent_custom_testimonials-5').wrapAll('<div class="sticky_sidebar" data-sticky_column></div>');

                $.fn.isabsolute = function(){
                    var el = this[0];
                    if (window.getComputedStyle(el).getPropertyValue('position').toLowerCase() === 'absolute') return true;
                    return false;
                };
                $( window ).scroll(function() {
                    if ($('.sticky_sidebar').isabsolute()){
                        $('.sticky_sidebar').css('top',  $('.archive-page').height() - $('.sticky_sidebar')[0].scrollHeight - 15);
                    }
                });
            }
        // });

        // $("#widget_service-4").stick_in_parent({
        //     parent: '.inner-page-wrap',
        //    // offset_top: 0
        //  });
        // $("#ct_recent_custom_testimonials-5").stick_in_parent({
        //     parent: '.inner-page-wrap',
        //   //  offset_top: 300
        //  });

    //     $('a[href^="mailto:"]').each(function() {
    //         this.href = this.href.replace('@', '(at)').replace('.', '(dot)');
    //     });
    //     function send_email_click(email){
    //         $('.send-email-click').click(function(){
    //             window.location.href = 'mailto:'+email;
    //         });
    //     }
    });
</script>
<style type="text/css">
    .left-sidebar .sidebar-inner > * {
        border: none !important;
    }
    .left-sidebar .sidebar-inner .widget:first-child {
        border: 2px solid #007700;
    }
    .left-sidebar .sidebar-inner .widget:last-child {
        margin-top: 15px;
        border: 2px solid #007700;
        overflow: hidden;
    }
</style>
    <!--// WordPress Hook //-->
<?php get_footer(); ?>