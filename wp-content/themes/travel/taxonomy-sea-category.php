<?php get_header(); ?>

<?php
$term_slug = get_query_var( 'term' );
$taxonomyName = get_query_var( 'taxonomy' );
$current_term = get_term_by( 'slug', $term_slug, $taxonomyName );
$term_id = $current_term->term_id;

$cat_data = get_option("tag_$term_id");
$seo_title=$cat_data['seo_met_title'];
$image_category  = category_image_src( array('size' =>'img-title') , false );
?>
<?php if($image_category){
    ?>
    <div class="row">
        <div class="title-image col-md-12">
            <img itemprop="image" src="<?php echo $image_category;?>" alt="img-category" />
        </div>
    </div>
<?php
}?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>

    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-page coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">

                <div class="page-content clearfix">
                    <div class="page-content-inner">
                        <h1 class="heading-text">
                            <?php  if($seo_title){
                                echo $seo_title;
                            }

                            else{
                                single_cat_title();
                            } ?>
                        </h1>
                        <div class="desception-category border-line"><?php echo category_description(); ?></div>

                        <?php if(have_posts()) : ?>

                            <div class="blog-wrap blog-items-wrap archive-wrap">

                                <!-- OPEN .blog-items -->
                                <ul class="blog-items mini-items clearfix" id="blogGrid">

                                    <?php while (have_posts()) : the_post(); ?>

                                        <?php
                                        $thumb_image = get_post_thumbnail_id();
                                        $item_title = get_the_title();
                                        $tour_code = get_post_meta($post->ID, 'ct_tour_code', true);
                                        $tour_time = get_post_meta($post->ID, 'ct_tour_time', true);
                                        $tour_time_short = get_post_meta($post->ID, 'ct_tour_time_short', true);
                                        $tour_commencement = get_post_meta($post->ID, 'ct_tour_commencement', true);
                                        $tour_depart = get_post_meta($post->ID, 'ct_tour_depart', true);
                                        $tour_duree = get_post_meta($post->ID, 'ct_tour_duree', true);
                                        $tour_circuit = get_post_meta($post->ID, 'ct_tour_circuit', true);
                                        $tour_depart = get_post_meta($post->ID, 'ct_tour_depart', true);
                                        $tour_programme = get_post_meta($post->ID, 'ct_tour_programme', true);


                                        $thumb_image = get_post_thumbnail_id();
                                        $item_title = get_the_title();
                                        $thumb_img_url = wp_get_attachment_url( $thumb_image, 'full' );
                                        $image = aq_resize( $thumb_img_url, 300, 225, true, false);

                                        $post_format = get_post_format($post->ID);
                                        if ( $post_format == "" ) {
                                            $post_format = 'standard';
                                        }
                                        ?>

                                        <li <?php post_class('blog-item  format-'.$post_format); ?>>
                                            <h3 class="hidden-md hidden-lg archive-title">
                                                <a href="<?php echo get_permalink(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                                <span class="single-tour-time">
                                                    <?php
                                                        if($tour_time_short){ echo $tour_time_short; }
                                                        else {  echo $tour_time; }
                                                    ?>
                                                </span>
                                            </h3>
                                            <figure class="animated-overlay overlay-alt">
                                                <a href="<?php the_permalink(); ?>">
                                                    <img itemprop="image" src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $item_title; ?>" />
                                                </a>
                                                <span class="single-tour-code hidden-md hidden-lg">Code : <?php echo $tour_code;?></span>
                                            </figure>
                                            <div class="">
                                                <div class="group-title">
                                                    <h3 class="archive-title">
                                                        <a href="<?php echo get_permalink(); ?>">
                                                            <?php the_title(); ?>
                                                        </a> -
                                                    </h3>
                                                <span class="single-tour-time">
                                                    <?php if($tour_time_short){
                                                        echo $tour_time_short;
                                                    }
                                                    else {echo $tour_time;}
                                                    ?>   &nbsp;  |  &nbsp;
                                                </span>
                                                    <span class="single-tour-code">Code : <?php echo $tour_code;?></span>
                                                </div>
                                                <div class="tour_programme">
                                                    <?php echo $tour_programme;?>
                                                </div>
                                                <div class="archive-content">
                                                    <?php echo ct_excerpt('200');?>
                                                </div>

                                            </div>
                                            <div class="btn btn-readmore"><a href="<?php echo get_permalink(); ?>"> voir ce voyage </a> </div>
                                        </li>
                                    <?php endwhile; ?>

                                    <!-- CLOSE .blog-items -->
                                </ul>

                            </div>

                        <?php else: ?>
                            <h3><?php _e("Sorry, there are no posts to display.", "cootheme"); ?></h3>
                        <?php endif; ?>

                        <div class="pagination-wrap">
                            <?php echo pagenavi($wp_query); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->

            <div class="left-sidebar  col-xs-12 col-sm-3 col-sm-pull-9 col-md-3 ">
                <div class="sidebar-inner">
                    <?php dynamic_sidebar('sidebar_sea'); ?>
                </div>
            </div>
            <!--End.Sidebar-right -->
        </div>
    </div>

    <!--// WordPress Hook //-->
<?php get_footer(); ?>