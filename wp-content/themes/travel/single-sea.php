<?php get_header(); ?>

<?php

$remove_breadcrumbs = get_post_meta($post->ID, 'ct_no_breadcrumbs', true);

$tour_code = get_post_meta($post->ID, 'ct_tour_code', true);
$tour_time = get_post_meta($post->ID, 'ct_tour_time', true);
$tour_commencement = get_post_meta($post->ID, 'ct_tour_commencement', true);
$tour_depart = get_post_meta($post->ID, 'ct_tour_depart', true);
$tour_duree = get_post_meta($post->ID, 'ct_tour_duree', true);
$tour_circuit = get_post_meta($post->ID, 'ct_tour_circuit', true);
$tour_departs = get_post_meta($post->ID, 'ct_tour_departs', true);
$tour_programme = get_post_meta($post->ID, 'ct_tour_programme', true);

$title_image_url="";
$title_image = rwmb_meta('ct_title_image', 'type=image&size=img-title');
if (is_array($title_image)) {
    foreach ($title_image as $image) {
        $title_image_url = $image['url'];
        break;
    }
}
if (have_posts()) : the_post();
    if($title_image_url){
        ?>
        <div class="row">
            <div class="title-image col-md-12">
                <?php echo $image = '<img itemprop="image" src="'.$title_image_url.'" alt="'.$image_title.'" />';?>
            </div>
        </div>
    <?php
    }?>

    <div class="page-heading col-sm-12">
        <?php
        // BREADCRUMBS
        if (!$remove_breadcrumbs) {
            echo ct_breadcrumbs();
        }
        ?>
    </div>


    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-post coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">
                <div class="post-content-single clearfix">
                    <div class="single-thumb-image">
                        <?php echo the_post_thumbnail('full-width-image-gallery');?>
                    </div>
                    <!--                    Start.Article -->
                    <section class="single-body-wrap article-body-wrap">
                        <div class="group-title">
                            <h1 class="single-title">
                                <?php $title = get_the_title();
                                echo $title;
                                 ?> -
                            </h1>
                            <span class="single-tour-time"> <?php echo $tour_time;?> - </span>
                            <span class="single-tour-code">Code : <?php echo $tour_code;?></span>
                        </div>
                        <script type="text/javascript">
							jQuery(document).ready(function(){
								jQuery('input[name="tour_id"]').val('<?php echo $tour_code;?>');
								jQuery('input[name="tour_name"]').val('<?php echo $title .' - '. $tour_time;?>');
							});
						</script>	
                        <div class="descrition">
                            <div class="single-tour-destination">
                                <div class="single-start-point"><i class="fa fa-home"></i> Commencement : <?php echo $tour_commencement; ?></div>
                                <div class="single-end-point"><i class="fa fa-plane"></i> Depart : <?php echo $tour_depart; ?></div>
                            </div>
                            <div class="single-tour-duree"><i class="fa fa-calendar"></i>
                                Duree :<?php echo $tour_duree ;?>
                            </div>
                            <div class="single-tour-type">
                                <div class="single-circuit"><i class="fa fa-user"></i> Circuit : <?php echo $tour_circuit; ?></div>
                                <div class="single-depart"><i class="fa fa-clock-o"></i> Départ : <?php echo $tour_departs; ?></div>
                            </div>

                        </div>
                        <div class="body-text clearfix">
                            <?php the_content(); ?>
                        </div>
                    </section>
                    <!--                    End.Article-->
                    <!--                    Start.Related-->
                    <div class="related-wrap related-single">
                        <?php
                        $related_posts_query = ct_sea_related_posts($post->ID);

                        if( $related_posts_query->have_posts() ) {
                            _e('<h3 class="title-related"><span>'.__("A voir aussi:", "cootheme").'</span></h3>');
                            echo '<ul class="related-items  owl-carousel carousel-items" data-show="4" data-navigation="true" row clearfix">';
                            while ($related_posts_query->have_posts()) {
                                $related_posts_query->the_post();
                                $item_title = get_the_title();
                                $thumb_image = "";
                                $thumb_image = get_post_meta($post->ID, 'ct_thumbnail_image', true);
                                if (!$thumb_image) {
                                    $thumb_image = get_post_thumbnail_id();
                                }
                                $thumb_img_url = wp_get_attachment_url( $thumb_image, 'full' );
                                $image = aq_resize( $thumb_img_url, 300, 225, true, false);
                                ?>

                                <li class="carousel-item recent-post clearfix">
                                    <div class="item-category-inner clearfix">
                                        <h3 class="no-margin"><a class="title-post" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo $item_title; ?></a></h3>
                                        <?php if ($image) { ?>
                                            <a href="<?php the_permalink(); ?>">
                                                <img  class="img-responsive" itemprop="image" src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $item_title; ?>" />
                                            </a>
                                        <?php } else { ?>
                                            <div class="img-holder"><i class="ss-pen"></i></div>
                                        <?php } ?>
                                    </div>
                                </li>

                            <?php }
                            echo '</ul>';
                        }?>
			

			
		
			<?php
                        wp_reset_query();
                        ?>
                    </div>
                    <!--                     End.Related-->
                    <!--                     Start.Social-->
                    <div class="single-bottom clearfix">
                        <div class="pull-left LikeButton clearfix">
                            <!-- Facebook Button -->
                            <div class="FacebookButton">
                                <div id="fb-root"></div>
                                <script type="text/javascript">
                                    (function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) {
                                            return;
                                        }
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/en_US/all.js#appId=177111755694317&xfbml=1";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                </script>
                                <div class="fb-like" data-send="false" data-width="200" data-show-faces="true"
                                     data-layout="button_count" data-href="<?php the_permalink(); ?>"></div>
                            </div>
                            <!-- Twitter Button -->
                            <div class="TwitterButton">
                                <a href="<?php the_permalink(); ?>" class="twitter-share-button"
                                   data-count="horizontal" data-via="" data-size="small">
                                </a>
                            </div>
                            <!-- Google +1 Button -->
                            <div class="GooglePlusOneButton">
                                <!-- Place this tag where you want the +1 button to render -->
                                <div class="g-plusone" data-size="medium"
                                     data-href="<?php the_permalink(); ?>"></div>
                                <!-- Place this render call where appropriate -->
                                <script type="text/javascript">
                                    (function () {
                                        var po = document.createElement('script');
                                        po.type = 'text/javascript';
                                        po.async = true;
                                        po.src = 'https://apis.google.com/js/plusone.js';
                                        var s = document.getElementsByTagName('script')[0];
                                        s.parentNode.insertBefore(po, s);
                                    })();
                                </script>

                            </div>
                            <!-- Pinterest Button -->
                            <div class="PinterestButton">
                                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>&description=<?php the_title(); ?>"
                                   data-pin-do="buttonPin" data-pin-config="beside">
                                    <img class="pinterest"
                                         src="//assets.pinterest.com/images/pidgets/pin_it_button.png"/>
                                </a>
                                <script type="text/javascript">
                                    (function (d) {
                                        var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
                                        p.type = 'text/javascript';
                                        p.async = true;
                                        p.src = '//assets.pinterest.com/js/pinit.js';
                                        f.parentNode.insertBefore(p, f);
                                    }(document));
                                </script>
                            </div>
                            <!-- Linkedin Button -->
                            <div class="LinkedinButton">
                                <script type="IN/Share" data-url="<?php the_permalink(); ?>"
                                        data-counter="right"></script>
                            </div>
                        </div>

                    </div>

                    <?php if ( comments_open() ) { ?>
                        <div id="comment-area">
                            <?php comments_template('', true); ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <!-- End.Main -->

            <div class="left-sidebar col-xs-12 col-sm-3 col-sm-pull-9 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_sea'); ?>
                    <?php } ?>
                </div>
            </div>
            <!--End.Sidebar-right -->
        </div>
    </div>
<?php endif; ?>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>
