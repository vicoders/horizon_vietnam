<?php get_header(); ?>

<?php
if (have_posts()) : the_post();
    //MEDIA TYPE
    $media_type = $media_image = $media_video = $media_gallery = '';

    $use_thumb_content = get_post_meta($post->ID, 'ct_thumbnail_content_main_detail', true);


    $post_format = get_post_format($post->ID);
    if ( $post_format == "" ) {
        $post_format = 'standard';
    }

    if ($use_thumb_content) {
        $media_type = get_post_meta($post->ID, 'ct_thumbnail_type', true);
    } else {
        $media_type = get_post_meta($post->ID, 'ct_detail_type', true);
    }


    $media_width = 770;
    $media_height = NULL;
    $video_height = 433;

    $figure_output = '';

    $figure_output .= '<figure class="media-wrap-brand " >';
    if ($post_format == "standard") {

        if ($media_type == "video") {

            $figure_output .= ct_video_post($post->ID, $media_width,$media_height, $use_thumb_content)."\n";

        } else if ($media_type == "slider") {

            $figure_output .= ct_gallery_post($post->ID, $use_thumb_content)."\n";

        } else if ($media_type == "layer-slider") {

            $figure_output .= '<div class="layerslider">'."\n";

            $figure_output .= do_shortcode('[rev_slider '.$media_slider.']')."\n";

            $figure_output .= '</div>'."\n";

        } else if ($media_type == "custom") {

            $figure_output .= $custom_media."\n";

        } else {

            $figure_output .= ct_image_post($post->ID, $media_width,$media_height, $use_thumb_content)."\n";

        }

    }
    else {

        $figure_output .= ct_get_post_media($post->ID, $media_width, $media_height, $video_height, $use_thumb_content);

    }

    $figure_output .= '</figure>'."\n";

    ?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>

    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-post coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">
                <div class="post-content clearfix">
                    <?php echo $figure_output;?>
                    <!--                    Start.Article -->
                    <section class="article-body-wrap">
                        <h3 class="single-title">
                            <?php the_title(); ?>
                        </h3>
                        <div class="body-text clearfix">
                            <?php the_content(); ?>
                        </div>
                    </section>
                    <!--                    End.Article-->
                    <!--                    Start.Related-->
                    <div class="related-wrap">
                        <?php
                        $related_posts_query =  ct_book_related_posts( $post->ID );
                        $i=0;
                        if( $related_posts_query->have_posts() ) {
                            _e('<h3 class="title-related"><span>'.__("Related Articles", "cootheme").'</span></h3>');
                            echo '<ul class="related-items row clearfix">';
                            while ($related_posts_query->have_posts()) {
                                $related_posts_query->the_post();
                                $item_title = get_the_title();
                                $thumb_image = "";
                                $thumb_image = get_post_meta($post->ID, 'ct_thumbnail_image', true);
                                if (!$thumb_image) {
                                    $thumb_image = get_post_thumbnail_id();
                                }
                                $thumb_img_url = wp_get_attachment_url( $thumb_image, 'full' );
                                $image = aq_resize( $thumb_img_url, 300, 225, true, false);
                                if($i<3){
                                    ?>

                                    <li class="related-item col-md-4 clearfix">
                                        <figure class="animated-overlay overlay-alt">
                                            <?php if ($image) { ?>
                                                <a href="<?php the_permalink(); ?>">
                                                    <img itemprop="image" src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $item_title; ?>" />
                                                </a>
                                            <?php } else { ?>
                                                <div class="img-holder"><i class="ss-pen"></i></div>
                                            <?php } ?>

                                        </figure>
                                        <h5><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo $item_title; ?></a></h5>
                                    </li>
                                <?php }
                                else{
                                    ?>
                                    <li class="related-item-list col-md-6 clearfix">
                                        <h5><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo $item_title; ?></a></h5>
                                    </li>
                                <?php
                                }
                                ?>
                                <?php $i++; }
                            echo '</ul>';
                        }
                        wp_reset_query();
                        ?>
                    </div>
                    <!--                     End.Related-->
                    <!--                     Start.Social-->
                    <div class="signle-bottom clearfix">
                        <div class="pull-left LikeButton clearfix">
                            <!-- Facebook Button -->
                            <div class="FacebookButton">
                                <div id="fb-root"></div>
                                <script type="text/javascript">
                                    (function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) {
                                            return;
                                        }
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/en_US/all.js#appId=177111755694317&xfbml=1";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                </script>
                                <div class="fb-like" data-send="false" data-width="200" data-show-faces="true"
                                     data-layout="button_count" data-href="<?php the_permalink(); ?>"></div>
                            </div>
                            <!-- Twitter Button -->
                            <div class="TwitterButton">
                                <a href="<?php the_permalink(); ?>" class="twitter-share-button"
                                   data-count="horizontal" data-via="" data-size="small">
                                </a>
                            </div>
                            <!-- Google +1 Button -->
                            <div class="GooglePlusOneButton">
                                <!-- Place this tag where you want the +1 button to render -->
                                <div class="g-plusone" data-size="medium"
                                     data-href="<?php the_permalink(); ?>"></div>
                                <!-- Place this render call where appropriate -->
                                <script type="text/javascript">
                                    (function () {
                                        var po = document.createElement('script');
                                        po.type = 'text/javascript';
                                        po.async = true;
                                        po.src = 'https://apis.google.com/js/plusone.js';
                                        var s = document.getElementsByTagName('script')[0];
                                        s.parentNode.insertBefore(po, s);
                                    })();
                                </script>

                            </div>
                            <!-- Pinterest Button -->
                            <div class="PinterestButton">
                                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>&description=<?php the_title(); ?>"
                                   data-pin-do="buttonPin" data-pin-config="beside">
                                    <img class="pinterest"
                                         src="//assets.pinterest.com/images/pidgets/pin_it_button.png"/>
                                </a>
                                <script type="text/javascript">
                                    (function (d) {
                                        var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
                                        p.type = 'text/javascript';
                                        p.async = true;
                                        p.src = '//assets.pinterest.com/js/pinit.js';
                                        f.parentNode.insertBefore(p, f);
                                    }(document));
                                </script>
                            </div>
                            <!-- Linkedin Button -->
                            <div class="LinkedinButton">
                                <script type="IN/Share" data-url="<?php the_permalink(); ?>"
                                        data-counter="right"></script>
                            </div>
                        </div>

                    </div>

                    <?php if ( comments_open() ) { ?>
                        <div id="comment-area">
                            <?php comments_template('', true); ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->

            <div class="left-sidebar col-xs-12 col-sm-3 col-sm-pull-9 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_aboutus'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-Left -->
        </div>
    </div>
<?php endif; ?>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>
