<?php 

	/*
	*
	*	Theme Customizer Options
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*	ct_customize_register()
	*	ct_customize_preview()
	*
	*/
	
	if (!function_exists('ct_customize_register')) {
		function ct_customize_register($wp_customize) {
		
			$wp_customize->get_setting('blogname')->transport='postMessage';
			$wp_customize->get_setting('blogdescription')->transport='postMessage';
			$wp_customize->get_setting('header_textcolor')->transport='postMessage';

			/* PAGE STYLING
			================================================== */
			
			$wp_customize->add_section( 'page_styling', array(
			    'title'          => __( 'Color - Page', 'coo-theme-admin' ),
			    'priority'       => 203,
			) );
			
			$wp_customize->add_setting( 'page_bg_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
	
			$wp_customize->add_setting( 'inner_page_bg_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'section_divide_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'alt_bg_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_bg_color', array(
                'default'=>'#ffffff',
				'label'   => __( 'Background colour (bordered only)', 'coo-theme-admin' ),
				'section' => 'page_styling',
				'settings'   => 'page_bg_color',
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'inner_page_bg_color', array(
				'label'   => __( 'Inner page background color', 'coo-theme-admin' ),
				'section' => 'page_styling',
				'settings'   => 'inner_page_bg_color',
			) ) );

			/* HEADER STYLING
			================================================== */
			$wp_customize->add_section('demo_style',array(
               'title'      =>__('Demo Style','coo-theme-admin'),
                'priority'  =>204,
            ));
            $wp_customize->add_setting( 'text_color', array(
                'type'           => 'option',
                'transport'      => 'postMessage',
                'capability'     => 'edit_theme_options',
            ) );
            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'text_color', array(
                'label'   => __( 'Demo demo', 'coo-theme-admin' ),
                'section' => 'demo_style',
                'settings'   => 'text_color',
                'priority'       => 1,
            ) ) );
			$wp_customize->add_section( 'header_styling', array(
			    'title'          => __( 'Color - Header', 'coo-theme-admin' ),
			    'priority'       => 205,
			) );
			
			$wp_customize->add_setting( 'header_aux_text_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'topbar_bg_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'topbar_text_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'topbar_link_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'topbar_link_hover_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'topbar_divider_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'header_bg_color1', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'header_bg_color2', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'header_border_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );


			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'topbar_bg_color', array(
				'label'   => __( 'Top Bar Background Color', 'coo-theme-admin' ),
				'section' => 'header_styling',
				'settings'   => 'topbar_bg_color',
				'priority'       => 2,
			) ) );

			/* NAVIGATION STYLING
			================================================== */
			
			$wp_customize->add_section( 'nav_styling', array(
			    'title'          => __( 'Color - Navigation', 'coo-theme-admin' ),
			    'priority'       => 205,
			) );
			$wp_customize->add_setting( 'nav_text_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			$wp_customize->add_setting( 'nav_text_hover_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_selected_text_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_pointer_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_sm_bg_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_sm_text_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_sm_bg_hover_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_sm_text_hover_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_sm_selected_text_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_divider', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'nav_divider_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );	
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'nav_text_color', array(
				'label'   => __( 'Nav Text Color', 'coo-theme-admin' ),
				'section' => 'nav_styling',
				'settings'   => 'nav_text_color',
				'priority'       => 2,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'nav_text_hover_color', array(
				'label'   => __( 'Nav Text Hover Color', 'coo-theme-admin' ),
				'section' => 'nav_styling',
				'settings'   => 'nav_text_hover_color',
				'priority'       => 3,
			) ) );
			/* PAGE HEADING STYLING
			================================================== */
					
			$wp_customize->add_section( 'page_heading_styling', array(
			    'title'          => __( 'Color - Page Heading', 'coo-theme-admin' ),
			    'priority'       => 207,
			) );
	
			$wp_customize->add_setting( 'page_heading_bg_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'page_heading_text_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
	
			$wp_customize->add_setting( 'breadcrumb_text_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'breadcrumb_link_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_heading_bg_color', array(
				'label'   => __( 'Page Heading Background Color', 'coo-theme-admin' ),
				'section' => 'page_heading_styling',
				'settings'   => 'page_heading_bg_color',
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_heading_text_color', array(
				'label'   => __( 'Page Heading Text Color', 'coo-theme-admin' ),
				'section' => 'page_heading_styling',
				'settings'   => 'page_heading_text_color',
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'breadcrumb_text_color', array(
				'label'   => __( 'Breadcrumb Text Color', 'coo-theme-admin' ),
				'section' => 'page_heading_styling',
				'settings'   => 'breadcrumb_text_color',
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'breadcrumb_link_color', array(
				'label'   => __( 'Breadcrumb Link Color', 'coo-theme-admin' ),
				'section' => 'page_heading_styling',
				'settings'   => 'breadcrumb_link_color',
			) ) );
			
			
			/* BODY STYLING
			================================================== */
			
			$wp_customize->add_section( 'body_styling', array(
			    'title'          => __( 'Color - Body', 'coo-theme-admin' ),
			    'priority'       => 208,
			) );
			
			$wp_customize->add_setting( 'body_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'body_alt_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'link_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'h1_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'h2_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'h3_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'h4_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'h5_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'h6_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'impact_text_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_color', array(
				'label'   => __( 'Body Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'body_color',
				'priority'       => 1,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_alt_color', array(
				'label'   => __( 'Body Alt Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'body_alt_color',
				'priority'       => 2,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
				'label'   => __( 'Link Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'link_color',
				'priority'       => 3,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'h1_color', array(
				'label'   => __( 'H1 Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'h1_color',
				'priority'       => 4,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'h2_color', array(
				'label'   => __( 'H2 Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'h2_color',
				'priority'       => 5,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'h3_color', array(
				'label'   => __( 'H3 Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'h3_color',
				'priority'       => 6,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'h4_color', array(
				'label'   => __( 'H4 Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'h4_color',
				'priority'       => 7,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'h5_color', array(
				'label'   => __( 'H5 Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'h5_color',
				'priority'       => 8,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'h6_color', array(
				'label'   => __( 'H6 Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'h6_color',
				'priority'       => 9,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'impact_text_color', array(
				'label'   => __( 'Impact Text Color', 'coo-theme-admin' ),
				'section' => 'body_styling',
				'settings'   => 'impact_text_color',
				'priority'       => 10,
			) ) );
			
			
			/* SHORTCODE STYLING
			================================================== */
			
			$wp_customize->add_section( 'shortcode_styling', array(
			    'title'          => __( 'Color - Shortcodes', 'coo-theme-admin' ),
			    'priority'       => 209,
			) );
			
			$wp_customize->add_setting( 'pt_primary_bg_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'pt_secondary_bg_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'pt_tertiary_bg_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'lpt_primary_row_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'lpt_secondary_row_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'lpt_default_pricing_header', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'lpt_default_package_header', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'lpt_default_footer', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'icon_container_bg_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'ct_icon_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'ct_icon_alt_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'boxed_content_color', array(

				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
					
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'pt_primary_bg_color', array(
				'label'   => __( 'Pricing Table Primary Background Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'pt_primary_bg_color',
				'priority'       => 1,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'pt_secondary_bg_color', array(
				'label'   => __( 'Pricing Table Secondary Background Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'pt_secondary_bg_color',
				'priority'       => 2,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'pt_tertiary_bg_color', array(
				'label'   => __( 'Pricing Table Tertiary Background Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'pt_tertiary_bg_color',
				'priority'       => 3,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'lpt_primary_row_color', array(
				'label'   => __( 'Labelled Pricing Table Primary Row Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'lpt_primary_row_color',
				'priority'       => 4,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'lpt_secondary_row_color', array(
				'label'   => __( 'Labelled Pricing Table Secondary Row Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'lpt_secondary_row_color',
				'priority'       => 5,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'lpt_default_pricing_header', array(
				'label'   => __( 'Labelled Pricing Table Default Pricing Background Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'lpt_default_pricing_header',
				'priority'       => 6,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'lpt_default_package_header', array(
				'label'   => __( 'Labelled Pricing Table Default Package Background Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'lpt_default_package_header',
				'priority'       => 7,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'lpt_default_footer', array(
				'label'   => __( 'Labelled Pricing Table Default Footer Background Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'lpt_default_footer',
				'priority'       => 8,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'icon_container_bg_color', array(
				'label'   => __( 'Icon Container Background Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'icon_container_bg_color',
				'priority'       => 9,
			) ) );	
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ct_icon_color', array(
				'label'   => __( 'Icon Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'ct_icon_color',
				'priority'       => 10,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ct_icon_alt_color', array(
				'label'   => __( 'Icon Hover/Alt Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'ct_icon_alt_color',
				'priority'       => 11,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'boxed_content_color', array(
				'label'   => __( 'Boxed Content Coloured BG Color', 'coo-theme-admin' ),
				'section' => 'shortcode_styling',
				'settings'   => 'boxed_content_color',
				'priority'       => 12,
			) ) );
			
			
			/* FOOTER STYLING
			================================================== */
					
			$wp_customize->add_section( 'footer_styling', array(
			    'title'          => __( 'Color - Footer', 'coo-theme-admin' ),
			    'priority'       => 210,
			) );
			
			$wp_customize->add_setting( 'footer_bg_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
	
			$wp_customize->add_setting( 'footer_text_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'footer_border_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'copyright_bg_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'copyright_text_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_setting( 'copyright_link_color', array(
				'type'           => 'option',
				'transport'      => 'postMessage',
				'capability'     => 'edit_theme_options',
			) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_bg_color', array(
				'label'   => __( 'Footer Background Color', 'coo-theme-admin' ),
				'section' => 'footer_styling',
				'settings'   => 'footer_bg_color',
				'priority'       => 1,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_text_color', array(
				'label'   => __( 'Footer Text Color', 'coo-theme-admin' ),
				'section' => 'footer_styling',
				'settings'   => 'footer_text_color',
				'priority'       => 3,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_border_color', array(
				'label'   => __( 'Footer Border Color', 'coo-theme-admin' ),
				'section' => 'footer_styling',
				'settings'   => 'footer_border_color',
				'priority'       => 4,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'copyright_bg_color', array(
				'label'   => __( 'Copyright Background Color', 'coo-theme-admin' ),
				'section' => 'footer_styling',
				'settings'   => 'copyright_bg_color',
				'priority'       => 5,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'copyright_text_color', array(
				'label'   => __( 'Copyright Text Color', 'coo-theme-admin' ),
				'section' => 'footer_styling',
				'settings'   => 'copyright_text_color',
				'priority'       => 6,
			) ) );
			
			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'copyright_link_color', array(
				'label'   => __( 'Copyright Link Color', 'coo-theme-admin' ),
				'section' => 'footer_styling',
				'settings'   => 'copyright_link_color',
				'priority'       => 7,
			) ) );
			
		}
		add_action( 'customize_register', 'ct_customize_register' );
	}
	
	
	function ct_customizer_live_preview() {
		wp_enqueue_script( 'ct-customizer',	get_template_directory_uri().'/js/ct-customizer.js', array( 'jquery','customize-preview' ), NULL, true);
	}
	add_action( 'customize_preview_init', 'ct_customizer_live_preview' );
	
?>