<?php
// Register Custom Post Type
function register_ct_hotel_in_city() {
    $labels = array(
        'name'                  => _x( 'Hotel in City', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Hotel in City', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Hotel in City', 'text_domain' ),
        'name_admin_bar'        => __( 'Hotel in City', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'All Items', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Add New', 'text_domain' ),
        'new_item'              => __( 'New Item', 'text_domain' ),
        'edit_item'             => __( 'Edit Item', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Hotel in City', 'text_domain' ),
        'description'           => __( 'Post Type Description', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title'),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'hotel_in_city', $args );

}
add_action( 'init', 'register_ct_hotel_in_city', 0 );
add_action( 'admin_init', 'register_metabox_for_hotel_in_city');

function register_metabox_for_hotel_in_city(){
    global $meta_boxes;
    $prefix = 'jun';
    $text_domain = "coo-theme-admin";
    $meta_boxes[] = array(
        'id' => 'post_meta_box',
        'title' => __('Post Meta', $text_domain),
        'pages' => array( 'hotel_in_city' ),
        'context' => 'normal',
        'fields' => array(
            array(
                'name' => __('Standard', $text_domain),
                'id' => $prefix . '_standard',
                'clone' => true,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Supérieur', $text_domain),
                'id' => $prefix . '_superieur',
                'clone' => true,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Première Classe', $text_domain),
                'id' => $prefix . '_premiere',
                'clone' => true,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Deluxe', $text_domain),
                'id' => $prefix . '_deluxe',
                'clone' => true,
                'type'  => 'text',
                'std' => '',
            )
        )
    );
}

add_action('admin_head','jun_add_custom_css_admin');
function jun_add_custom_css_admin(){
    ?>
    <style type="text/css">
        .rwmb-clone {
            margin-bottom: 10px;
        }
    </style>
<?php
}

add_shortcode('hotel_in_city','create_shortcode_hotel_in_city');
function create_shortcode_hotel_in_city($atts){
    $output = $ids = $el_class = '';
    extract(shortcode_atts(array(
        'ids' => '',
        'el_class' => ''
    ),$atts));

    if($ids == ''){
        return $output;
    }
    $ids = explode(',',$ids);
    $ids = array_map('trim',$ids);
    $ids = array_map('absint',$ids);
    ob_start();
    if(!empty($ids)){
?><div class="table-responsive">
        <table class="hotel-in-city-table table table-bordered">
            <thead>
                <tr>
                    <th>Destination</th>
                    <th>Standard</th>
                    <th>Supérieur</th>
                    <th>Première Classe</th>
                    <th>Deluxe</th>
                </tr>
            </thead>
            <tbody>
<?php
        foreach($ids as $id){
            $standard = get_post_meta($id, 'jun_standard', true);
            $superieur = get_post_meta($id, 'jun_superieur', true);
            $premiere = get_post_meta($id, 'jun_premiere', true);
            $deluxe = get_post_meta($id, 'jun_deluxe', true);
?>
            <tr>
                <td data-th="Destination"><?php echo get_the_title($id)?></td>
                <td data-th="Standard"><?php
                    if(!empty($standard)){
                        foreach ( $standard as $hotel ){
                            if(strpos($hotel,'_nd_') === false){
                                echo '<p><span>'.$hotel.'</span><span>'.str_repeat('<i class="fa fa-star"></i>',2).'</span></p>';
                            }else{
                                echo '<p><span style="width: 100%;">'.str_replace('_nd_','',$hotel).'</span></p>';
                            }
                        }
                    }
                    ?></td>
                <td data-th="Supérieur"><?php
                    if(!empty($superieur)){
                        foreach ( $superieur as $hotel ){
                            if(strpos($hotel,'_nd_') === false){
                                echo '<p><span>'.$hotel.'</span><span>'.str_repeat('<i class="fa fa-star"></i>',3).'</span></p>';
                            }else{
                                echo '<p><span style="width: 100%;">'.str_replace('_nd_','',$hotel).'</span></p>';
                            }
                        }
                    }
                    ?></td>
                <td data-th="Première Classe"><?php
                    if(!empty($premiere)){
                        foreach ( $premiere as $hotel ){
                            if(strpos($hotel,'_nd_') === false){
                                echo '<p><span>'.$hotel.'</span><span>'.str_repeat('<i class="fa fa-star"></i>',4).'</span></p>';
                            }else{
                                echo '<p><span style="width: 100%;">'.str_replace('_nd_','',$hotel).'</span></p>';
                            }
                        }
                    }
                    ?></td>
                <td data-th="Deluxe"><?php
                    if(!empty($deluxe)){
                        foreach ( $deluxe as $hotel ){
                            if(strpos($hotel,'_nd_') === false){
                                echo '<p><span>'.$hotel.'</span><span>'.str_repeat('<i class="fa fa-star"></i>',5).'</span></p>';
                            }else{
                                echo '<p><span style="width: 100%;">'.str_replace('_nd_','',$hotel).'</span></p>';
                            }
                        }
                    }
                    ?></td>
            </tr>
<?php
        }
        ?>
            </tbody>
        </table>
</div>
        <?php
    }
    $output = ob_get_clean();
    return $output;
}