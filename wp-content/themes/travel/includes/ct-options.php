<?php
/*
 *
 * Set the text domain for the theme or plugin.
 *
 */
define('Redux_TEXT_DOMAIN', 'coo-theme-admin');

/*
 *
 * Require the framework class before doing anything else, so we can use the defined URLs and directories.
 * If you are running on Windows you may have URL problems which can be fixed by defining the framework url first.
 *
 */
//define('Redux_OPTIONS_URL', site_url('path the options folder'));
if(!class_exists('Redux_Options')){
    require_once(dirname(__FILE__) . '/options/defaults.php');
}

/*
 *
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 *
 * NOTE: the defined constansts for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 *
 */
function add_another_section($sections){
    //$sections = array();
    $sections[] = array(
        'title' => __('A Section added by hook', Redux_TEXT_DOMAIN),
        'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', Redux_TEXT_DOMAIN),
        // Redux ships with the glyphicons free icon pack, included in the options folder.
        // Feel free to use them, add your own icons, or leave this blank for the default.
        'icon' => trailingslashit(get_template_directory_uri()) . 'options/img/icons/glyphicons_062_attach.png',
        // Leave this as a blank section, no options just some intro text set above.
        'fields' => array()
    );

    return $sections;
}
//add_filter('redux-opts-sections-twenty_eleven', 'add_another_section');


/*
 * 
 * Custom function for filtering the args array given by a theme, good for child themes to override or add to the args array.
 *
 */
function change_framework_args($args){
    //$args['dev_mode'] = false;
    
    return $args;
}
//add_filter('redux-opts-args-twenty_eleven', 'change_framework_args');


/*
 *
 * Most of your editing will be done in this section.
 *
 * Here you can override default values, uncomment args and change their values.
 * No $args are required, but they can be over ridden if needed.
 *
 */
function setup_framework_options(){
    $args = array();

    // Setting dev mode to true allows you to view the class settings/info in the panel.
    // Default: false
    $args['dev_mode'] = false;

    // If you want to use Google Webfonts, you MUST define the api key.
    $args['google_api_key'] = 'AIzaSyCWA2ZOS0NolFoVBu1iMwij_oWy4L2AJYY';

    // Define the starting tab for the option panel.
    // Default: '0';
    //$args['last_tab'] = '0';

    // Define the option panel stylesheet. Options are 'standard', 'custom', and 'none'
    // If only minor tweaks are needed, set to 'custom' and override the necessary styles through the included custom.css stylesheet.
    // If replacing the stylesheet, set to 'none' and don't forget to enqueue another stylesheet!
    // Default: 'standard'
    $args['admin_stylesheet'] = 'custom';

    // Add HTML before the form.
    //$args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Add content after the form.
    //$args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Set footer/credit line.
    //$args['footer_credit'] = __('<p>This text is displayed in the options panel footer across from the WordPress version (where it normally says \'Thank you for creating with WordPress\'). This field accepts all HTML.</p>', Redux_TEXT_DOMAIN);

    // Setup custom links in the footer for share icons
//    $args['share_icons']['twitter'] = array(
//        'link' => 'http://twitter.com/cootheme',
//        'title' => 'Follow us on Twitter', 
//        'img' => Redux_OPTIONS_URL . 'img/social/Twitter.png'
//    );

    // Enable the import/export feature.
    // Default: true
    //$args['show_import_export'] = false;

    // Set a custom option name. Don't forget to replace spaces with underscores!
    $args['opt_name'] = 'ct_coo_options';

    // Set a custom menu icon.
    // Redux ships with the glyphicons free icon pack, included in the options folder.
    // Feel free to use them, add your own icons, or leave this blank for the default.
    //$args['menu_icon'] = '';

    // Set a custom title for the options page.
    // Default: Options
    $args['menu_title'] = __('Theme Options', Redux_TEXT_DOMAIN);

    // Set a custom page title for the options page.
    // Default: Options
    $args['page_title'] = __('Theme Options', Redux_TEXT_DOMAIN);
	
	// Set the class for the import/export tab icon.
	$args['import_icon_type'] = 'iconfont';
	$args['import_icon_class'] = 'fa-lg';

	// Set the class for the dev mode tab icon.	
	$args['dev_mode_icon_type'] = 'iconfont';
	
    // Set a custom page slug for options page (wp-admin/themes.php?page=***).
    // Default: redux_options
    $args['page_slug'] = 'ct_theme_options';

    // Set a custom page capability.
    // Default: manage_options
    //$args['page_cap'] = 'manage_options';

    // Set the menu type. Set to "menu" for a top level menu, or "submenu" to add below an existing item.
    // Default: menu
    //$args['page_type'] = 'submenu';

    // Set the parent menu.
    // Default: themes.php
    // A list of available parent menus is available at http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    //$args['page_parent'] = 'options_general.php';

    // Set a custom page location. This allows you to place your menu where you want in the menu order.
    // Must be unique or it will override other items!
    // Default: null
    $args['page_position'] = 58;

    // Set a custom page icon class (used to override the page icon next to heading)
    //$args['page_icon'] = 'icon-themes';

    // Disable the panel sections showing as submenu items.
    // Default: true
    //$args['allow_sub_menu'] = false;
        
    // Set ANY custom page help tabs, displayed using the new help tab API. Tabs are shown in order of definition.
    $args['help_tabs'][] = array(
        'id' => 'redux-opts-1',
        'title' => __('Theme Information 1', Redux_TEXT_DOMAIN),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', Redux_TEXT_DOMAIN)
    );
    $args['help_tabs'][] = array(
        'id' => 'redux-opts-2',
        'title' => __('Theme Information 2', Redux_TEXT_DOMAIN),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', Redux_TEXT_DOMAIN)
    );

    // Set the help sidebar for the options page.                                        
    $args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', Redux_TEXT_DOMAIN);
	
	$args['bg_image_path'] = get_template_directory_uri() . '/images/preset-backgrounds/'; // change this to where you store your bg images
	
	// Portfolio Background Images Reader
	$body_bg_images_path = get_stylesheet_directory() . '/images/preset-backgrounds/'; // change this to where you store your bg images
	$body_bg_images_url = get_template_directory_uri() .'/images/preset-backgrounds/'; // change this to where you store your bg images
	$body_bg_images = array();
	
	if ( is_dir($body_bg_images_path) ) {
	    if ($body_bg_images_dir = opendir($body_bg_images_path) ) { 
	        while ( ($body_bg_images_file = readdir($body_bg_images_dir)) !== false ) {
	            if(stristr($body_bg_images_file, ".png") !== false || stristr($body_bg_images_file, ".jpg") !== false) {
	                $body_bg_images[] = $body_bg_images_url . $body_bg_images_file;
	            }
	        }    
	    }
	}
	
    $sections = array();
    				
    $sections[] = array(
    				'icon' => 'cog',
    				'icon_class' => 'fa-lg',
    				'title' => __('General Options', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">These are the general options for the theme</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(	
    					array(
    						'id' => 'enable_maintenance',
    						'type' => 'button_set',
    						'title' => __('Enable Maintenance', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the themes maintenance mode.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '0'
    						),
    					array(
    						'id' => 'custom_favicon',
    						'type' => 'upload',
    						'title' => __('Custom favicon', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Upload a 16px x 16px Png/Gif image that will represent your website favicon', Redux_TEXT_DOMAIN),
    						'desc' => ''
    						),
    					array(
    						'id' => 'rss_feed_url',
    						'type' => 'text',
    						'title' => __('RSS Feed URL', Redux_TEXT_DOMAIN),
    						'sub_desc' => __('The rss feed URL for your blog.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => '?feed=rss2'
    						),
    					array(
    						'id' => 'google_analytics',
    						'type' => 'textarea',
    						'title' => __('Tracking code', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme. NOTE: Please include the script tag.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => ''
    						),
    					)
    				);
   	$sections[] = array(
   					'icon' => 'code',
   					'icon_class' => 'fa-lg',
   					'title' => __('Custom CSS/JS', Redux_TEXT_DOMAIN),
   					'desc' => __('<p class="description">Add your custom css/js code to the boxes below.</p>', Redux_TEXT_DOMAIN),
   					'fields' => array(
   							array(
   								'id' => 'custom_css',
   								'type' => 'textarea',
   								'title' => __('Custom CSS', Redux_TEXT_DOMAIN), 
   								'sub_desc' => __('Add some CSS to your theme by adding it to this textarea.', Redux_TEXT_DOMAIN),
   								'desc' => '',
   								'std' => ''
   								),
   							array(
   								'id' => 'custom_js',
   								'type' => 'textarea',
   								'title' => __('Custom JS', Redux_TEXT_DOMAIN), 
   								'sub_desc' => __('Add some custom JavaScript to your theme by adding it to this textarea.', Redux_TEXT_DOMAIN),
   								'desc' => '',
   								'std' => ''
   								)
   						)
   					);
    $sections[] = array(
    				'icon' => 'picture-o',
    				'icon_class' => 'fa-lg',
    				'title' => __('Background Options', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">These are the options for the background.</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(
    					array(
    						'id' => 'use_bg_image',
    						'type' => 'button_set',
    						'title' => __('Use Background Image', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Check this to use an image for the body background (boxed layout only).', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						),
    					array(
    						'id' => 'custom_bg_image',
    						'type' => 'upload',
    						'title' => __('Upload Background Image', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Either upload or provide a link to your own background here, or choose from the presets below.', Redux_TEXT_DOMAIN),
    						'desc' => ''
    						),
    					array(
    						'id' => 'bg_size',
    						'type' => 'button_set',
    						'title' => __('Background Size', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('If you are using an image rather than a pattern, select cover to make the image cover the background.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('cover' => 'Cover','auto' => 'Auto'),
    						'std' => 'auto'
    						),
    					array(
    						'id' => 'preset_bg_image',
    						'type' => 'radio_img_bg',
    						'title' => __('Preset body background image', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Select a preset background image for the body background.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array(
    										$args['bg_image_path'] . '45degree_fabric.png' => $args['bg_image_path'] . '45degree_fabric.png',
    										$args['bg_image_path'] . 'argyle.png' => $args['bg_image_path'] . 'argyle.png',
    										$args['bg_image_path'] . 'beige_paper.png' => $args['bg_image_path'] . 'beige_paper.png',
    										$args['bg_image_path'] . 'bgnoise_lg.png' => $args['bg_image_path'] . 'bgnoise_lg.png',
    										$args['bg_image_path'] . 'black_denim.png' => $args['bg_image_path'] . 'black_denim.png',
    										$args['bg_image_path'] . 'black_linen_v2.png' => $args['bg_image_path'] . 'black_linen_v2.png',
    										$args['bg_image_path'] . 'black_paper.png' => $args['bg_image_path'] . 'black_paper.png',
    										$args['bg_image_path'] . 'black-Linen.png' => $args['bg_image_path'] . 'black-Linen.png',
    										$args['bg_image_path'] . 'blackmamba.png' => $args['bg_image_path'] . 'blackmamba.png',
    										$args['bg_image_path'] . 'blu_stripes.png' => $args['bg_image_path'] . 'blu_stripes.png',
    										$args['bg_image_path'] . 'bright_squares.png' => $args['bg_image_path'] . 'bright_squares.png',
    										$args['bg_image_path'] . 'brushed_alu_dark.png' => $args['bg_image_path'] . 'brushed_alu_dark.png',
    										$args['bg_image_path'] . 'brushed_alu.png' => $args['bg_image_path'] . 'brushed_alu.png',
    										$args['bg_image_path'] . 'candyhole.png' => $args['bg_image_path'] . 'candyhole.png',
    										$args['bg_image_path'] . 'checkered_pattern.png' => $args['bg_image_path'] . 'checkered_pattern.png',
    										$args['bg_image_path'] . 'classy_fabric.png' => $args['bg_image_path'] . 'classy_fabric.png',
    										$args['bg_image_path'] . 'concrete_wall_3.png' => $args['bg_image_path'] . 'concrete_wall_3.png',
    										$args['bg_image_path'] . 'connect.png' => $args['bg_image_path'] . 'connect.png',
    										$args['bg_image_path'] . 'cork_1.png' => $args['bg_image_path'] . 'cork_1.png',
    										$args['bg_image_path'] . 'crissXcross.png' => $args['bg_image_path'] . 'crissXcross.png',
    										$args['bg_image_path'] . 'dark_brick_wall.png' => $args['bg_image_path'] . 'dark_brick_wall.png',
    										$args['bg_image_path'] . 'dark_dotted.png' => $args['bg_image_path'] . 'dark_dotted.png',
    										$args['bg_image_path'] . 'dark_geometric.png' => $args['bg_image_path'] . 'dark_geometric.png',
    										$args['bg_image_path'] . 'dark_leather.png' => $args['bg_image_path'] . 'dark_leather.png',
    										$args['bg_image_path'] . 'dark_mosaic.png' => $args['bg_image_path'] . 'dark_mosaic.png',
    										$args['bg_image_path'] . 'dark_wood.png' => $args['bg_image_path'] . 'dark_wood.png',
    										$args['bg_image_path'] . 'detailed.png' => $args['bg_image_path'] . 'detailed.png',
    										$args['bg_image_path'] . 'diagonal-noise.png' => $args['bg_image_path'] . 'diagonal-noise.png',
    										$args['bg_image_path'] . 'fabric_1.png' => $args['bg_image_path'] . 'fabric_1.png',
    										$args['bg_image_path'] . 'fake_luxury.png' => $args['bg_image_path'] . 'fake_luxury.png',
    										$args['bg_image_path'] . 'felt.png' => $args['bg_image_path'] . 'felt.png',
    										$args['bg_image_path'] . 'flowers.png' => $args['bg_image_path'] . 'flowers.png',
    										$args['bg_image_path'] . 'foggy_birds.png' => $args['bg_image_path'] . 'foggy_birds.png',
    										$args['bg_image_path'] . 'graphy.png' => $args['bg_image_path'] . 'graphy.png',
    										$args['bg_image_path'] . 'gray_sand.png' => $args['bg_image_path'] . 'gray_sand.png',
    										$args['bg_image_path'] . 'green_gobbler.png' => $args['bg_image_path'] . 'green_gobbler.png',
    										$args['bg_image_path'] . 'green-fibers.png' => $args['bg_image_path'] . 'green-fibers.png',
    										$args['bg_image_path'] . 'grid_noise.png' => $args['bg_image_path'] . 'grid_noise.png',
    										$args['bg_image_path'] . 'gridme.png' => $args['bg_image_path'] . 'gridme.png',
    										$args['bg_image_path'] . 'grilled.png' => $args['bg_image_path'] . 'grilled.png',
    										$args['bg_image_path'] . 'grunge_wall.png' => $args['bg_image_path'] . 'grunge_wall.png',
    										$args['bg_image_path'] . 'handmadepaper.png' => $args['bg_image_path'] . 'handmadepaper.png',
    										$args['bg_image_path'] . 'inflicted.png' => $args['bg_image_path'] . 'inflicted.png',
    										$args['bg_image_path'] . 'irongrip.png' => $args['bg_image_path'] . 'irongrip.png',
    										$args['bg_image_path'] . 'knitted-netting.png' => $args['bg_image_path'] . 'knitted-netting.png',
    										$args['bg_image_path'] . 'leather_1.png' => $args['bg_image_path'] . 'leather_1.png',
    										$args['bg_image_path'] . 'light_alu.png' => $args['bg_image_path'] . 'light_alu.png',
    										$args['bg_image_path'] . 'light_checkered_tiles.png' => $args['bg_image_path'] . 'light_checkered_tiles.png',
    										$args['bg_image_path'] . 'light_honeycomb.png' => $args['bg_image_path'] . 'light_honeycomb.png',
    										$args['bg_image_path'] . 'lined_paper.png' => $args['bg_image_path'] . 'lined_paper.png',
    										$args['bg_image_path'] . 'little_pluses.png' => $args['bg_image_path'] . 'little_pluses.png',
    										$args['bg_image_path'] . 'mirrored_squares.png' => $args['bg_image_path'] . 'mirrored_squares.png',
    										$args['bg_image_path'] . 'noise_pattern_with_crosslines.png' => $args['bg_image_path'] . 'noise_pattern_with_crosslines.png',
    										$args['bg_image_path'] . 'noisy.png' => $args['bg_image_path'] . 'noisy.png',
    										$args['bg_image_path'] . 'old_mathematics.png' => $args['bg_image_path'] . 'old_mathematics.png',
    										$args['bg_image_path'] . 'padded.png' => $args['bg_image_path'] . 'padded.png',
    										$args['bg_image_path'] . 'paper_1.png' => $args['bg_image_path'] . 'paper_1.png',
    										$args['bg_image_path'] . 'paper_2.png' => $args['bg_image_path'] . 'paper_2.png',
    										$args['bg_image_path'] . 'paper_3.png' => $args['bg_image_path'] . 'paper_3.png',
    										$args['bg_image_path'] . 'pineapplecut.png' => $args['bg_image_path'] . 'pineapplecut.png',
    										$args['bg_image_path'] . 'pinstriped_suit.png' => $args['bg_image_path'] . 'pinstriped_suit.png',
    										$args['bg_image_path'] . 'plaid.png' => $args['bg_image_path'] . 'plaid.png',
    										$args['bg_image_path'] . 'project_papper.png' => $args['bg_image_path'] . 'project_papper.png',
    										$args['bg_image_path'] . 'px_by_Gre3g.png' => $args['bg_image_path'] . 'px_by_Gre3g.png',
    										$args['bg_image_path'] . 'quilt.png' => $args['bg_image_path'] . 'quilt.png',
    										$args['bg_image_path'] . 'random_grey_variations.png' => $args['bg_image_path'] . 'random_grey_variations.png',
    										$args['bg_image_path'] . 'ravenna.png' => $args['bg_image_path'] . 'ravenna.png',
    										$args['bg_image_path'] . 'real_cf.png' => $args['bg_image_path'] . 'real_cf.png',
    										$args['bg_image_path'] . 'robots.png' => $args['bg_image_path'] . 'robots.png',
    										$args['bg_image_path'] . 'rockywall.png' => $args['bg_image_path'] . 'rockywall.png',
    										$args['bg_image_path'] . 'roughcloth.png' => $args['bg_image_path'] . 'roughcloth.png',
    										$args['bg_image_path'] . 'small-crackle-bright.png' => $args['bg_image_path'] . 'small-crackle-bright.png',
    										$args['bg_image_path'] . 'smooth_wall.png' => $args['bg_image_path'] . 'smooth_wall.png',
    										$args['bg_image_path'] . 'snow.png' => $args['bg_image_path'] . 'snow.png',
    										$args['bg_image_path'] . 'soft_kill.png' => $args['bg_image_path'] . 'soft_kill.png',
    										$args['bg_image_path'] . 'square_bg.png' => $args['bg_image_path'] . 'square_bg.png',
    										$args['bg_image_path'] . 'starring.png' => $args['bg_image_path'] . 'starring.png',
    										$args['bg_image_path'] . 'stucco.png' => $args['bg_image_path'] . 'stucco.png',
    										$args['bg_image_path'] . 'subtle_freckles.png' => $args['bg_image_path'] . 'subtle_freckles.png',
    										$args['bg_image_path'] . 'subtle_orange_emboss.png' => $args['bg_image_path'] . 'subtle_orange_emboss.png',
    										$args['bg_image_path'] . 'subtle_zebra_3d.png' => $args['bg_image_path'] . 'subtle_zebra_3d.png',
    										$args['bg_image_path'] . 'tileable_wood_texture.png' => $args['bg_image_path'] . 'tileable_wood_texture.png',
    										$args['bg_image_path'] . 'type.png' => $args['bg_image_path'] . 'type.png',
    										$args['bg_image_path'] . 'vichy.png' => $args['bg_image_path'] . 'vichy.png',
    										$args['bg_image_path'] . 'washi.png' => $args['bg_image_path'] . 'washi.png',
    										$args['bg_image_path'] . 'white_sand.png' => $args['bg_image_path'] . 'white_sand.png',
    										$args['bg_image_path'] . 'white_texture.png' => $args['bg_image_path'] . 'white_texture.png',
    										$args['bg_image_path'] . 'whitediamond.png' => $args['bg_image_path'] . 'whitediamond.png',
    										$args['bg_image_path'] . 'whitey.png' => $args['bg_image_path'] . 'whitey.png',
    										$args['bg_image_path'] . 'woven.png' => $args['bg_image_path'] . 'woven.png',
    										$args['bg_image_path'] . 'xv.png' => $args['bg_image_path'] . 'xv.png'
    										),
    						'std' => ''
    						)
    					)
    				);
    $sections[] = array(
    				'icon_type' => 'image',
    				'icon' => Redux_OPTIONS_URL.'img/header.png',
    				'title' => __('Header Options', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">These are the options for the header.</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(
//    					array(
//    						'id' => 'enable_tb',
//    						'type' => 'button_set',
//    						'title' => __('Enable Top Bar', Redux_TEXT_DOMAIN),
//    						'sub_desc' => __('If enabled, the top bar will show with the menu and social config.', Redux_TEXT_DOMAIN),
//    						'desc' => '',
//    						'options' => array('1' => 'On','0' => 'Off'),
//    						'std' => '1'
//    						),
//    					array(
//    						'id' => 'tb_config',
//    						'type' => 'select',
//    						'title' => __('Top Bar Config', Redux_TEXT_DOMAIN),
//    						'sub_desc' => "Choose the config for the Top Bar. This will define the options below for what you have on the left/right of the Top Bar.",
//    						'options' => array(
//                                'tb-1'  => 'Text / Text',
//                                'tb-2'  => 'Alt Menu / Text',
//                                'tb-3'  => 'Text / Alt Menu',
//                                'tb-4'  => 'Welcome + Super Search / Links',
//                                'tb-5'  => 'Welcome + Super Search / Text',
//                                'tb-6'  => 'Welcome + Super Search / Alt Menu',
//                                'tb-7'  => 'Super Search / Text',
//                                'tb-8'  => 'Super Search / Alt Menu'
//    							),
//    						'desc' => '',
//    						'std' => 'tb-5'
//    						),
    					array(
    						'id' => 'tb_certer_header',
    						'type' => 'text',
    						'title' => __('Site Title', Redux_TEXT_DOMAIN),
    						'sub_desc' => "",
    						'desc' => '',
    						'std' => "horizon vietnam travel"
    						),
                        array(
    						'id' => 'tb_certer_header_desc',
    						'type' => 'text',
    						'title' => __('Tagline', Redux_TEXT_DOMAIN),
    						'sub_desc' => "",
    						'desc' => '',
    						'std' => "Rencontre au bout du monde - les voyages dans les rêves!"
    						),
    					array(
    						'id' => 'tb_right_text',
    						'type' => 'text',
    						'title' => __('Top Bar right text config', Redux_TEXT_DOMAIN),
    						'sub_desc' => "The text that is shown on the right of the Top Bar. You can use shortcodes in here if you like, (i.e. social). NOTE: Make sure you use single quotes (') for parameters with no spaces in between.",
    						'desc' => '',
    						'std' => "[social size='small' style='light' type='twitter,facebook,dribbble']"
    						),
//                        array(
//                            'id' => 'header_left_phone',
//                            'type' => 'text',
//                            'title' => __('Header left phone config', Redux_TEXT_DOMAIN),
//                            'sub_desc' => "The phone that is shown on the left of header on header type all.",
//                            'desc' => '',
//                            'std' => "1-800-567-866"
//                            ),
//                        array(
//                            'id' => 'header_left_email',
//                            'type' => 'text',
//                            'title' => __('Header left email config', Redux_TEXT_DOMAIN),
//                            'sub_desc' => "The email that is shown on the left of header on header type all.",
//                            'desc' => '',
//                            'std' => "support@yourdomain.com "
//                            ),
//    					array(
//    						'id' => 'tb_search_text',
//    						'type' => 'text',
//    						'title' => __('Search text config', Redux_TEXT_DOMAIN),
//    						'sub_desc' => "The text that is shown to the right of the search icon in the top bar / header.",
//    						'desc' => '',
//    						'std' => "Personal Shopper"
//    						),
//    					array(
//    						'id' => 'show_sub',
//    						'type' => 'button_set',
//    						'title' => __('Show subscribe aux option', Redux_TEXT_DOMAIN),
//    						'sub_desc' => __('Check this to show the suscribe dropdown in the links output, allowing users to subscribe via inputting their email address. If you use this, be sure to enter a Mailchimp form action URL in the box below.', Redux_TEXT_DOMAIN),
//    						'desc' => '',
//    						'options' => array('1' => 'On','0' => 'Off'),
//    						'std' => '0'
//    						),
//    					array(
//    						'id' => 'show_translation',
//    						'type' => 'button_set',
//    						'title' => __('Show translation aux option', Redux_TEXT_DOMAIN),
//    						'sub_desc' => __('Check this to show the translation dropdown in the links output.', Redux_TEXT_DOMAIN),
//    						'desc' => '',
//    						'options' => array('1' => 'On','0' => 'Off'),
//    						'std' => '0'
//    						),
//    					array(
//    						'id' => 'show_account',
//    						'type' => 'button_set',
//    						'title' => __('Show account aux option', Redux_TEXT_DOMAIN),
//    						'sub_desc' => __('Check this to show the account sign in / my account in the links output.', Redux_TEXT_DOMAIN),
//    						'desc' => '',
//    						'options' => array('1' => 'On','0' => 'Off'),
//    						'std' => '1'
//    						),
//    					array(
//    						'id' => 'show_cart',
//    						'type' => 'button_set',
//    						'title' => __('Show cart aux option', Redux_TEXT_DOMAIN),
//    						'sub_desc' => __('Check this to show the WooCommerce cart dropdown in the header.', Redux_TEXT_DOMAIN),
//    						'desc' => '',
//    						'options' => array('1' => 'On','0' => 'Off'),
//    						'std' => '0'
//    						),
//    					array(
//    						'id' => 'show_wishlist',
//    						'type' => 'button_set',
//    						'title' => __('Show wishlist aux option', Redux_TEXT_DOMAIN),
//    						'sub_desc' => __('Check this to show the WooCommerce wishlist dropdown in the header. NOTE: You will need the YITH Wishlist plugin to be enabled.', Redux_TEXT_DOMAIN),
//    						'desc' => '',
//    						'options' => array('1' => 'On','0' => 'Off'),
//    						'std' => '0'
////    						),
//    					array(
//    						'id' => 'sub_code',
//    						'type' => 'textarea',
//    						'title' => __('Subscribe form code', Redux_TEXT_DOMAIN),
//    						'sub_desc' => "Enter the form code (e.g. Mailchimp) that will be used for the subscribe dropdown.",
//    						'desc' => '',
//    						'std' => ""
//    						),
//       					array(
//    						'id' => 'header_layout',
//    						'type' => 'radio_img',
//    						'title' => __('Header Layout', Redux_TEXT_DOMAIN),
//    						'sub_desc' => __('Select a header layout option from the examples.', Redux_TEXT_DOMAIN),
//    						'desc' => '',
//    						'options' => array(
//								'header-1' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/header1.png'),
//								'header-2' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/header2.png'),
//								'header-3' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/header3.png'),
//                                'header-4' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/header4.png'),
//								'header-5' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/header5.png'),
//								'header-6' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/header6.png'),
//								'header-7' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/header7.png')
//							),
//    						'std' => 'header-7'
//    						),
//    					array(
//    						'id' => 'header_left_text',
//    						'type' => 'text',
//    						'title' => __('Header left text config', Redux_TEXT_DOMAIN),
//    						'sub_desc' => "The text that is shown on the left of header on header type 1 and type 4.",
//    						'desc' => '',
//    						'std' => "Contact us info@cootheme.com"
//    						),
    					array(
    						'id' => 'logo_upload',
    						'type' => 'upload',
    						'title' => __('Logo', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Upload your logo here (any size).', Redux_TEXT_DOMAIN),
    						'desc' => ''
    						),
    					array(
    						'id' => 'retina_logo_upload',
    						'type' => 'upload',
    						'title' => __('Retina Logo', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Upload the retina version of your logo here.', Redux_TEXT_DOMAIN),
    						'desc' => ''
    						),
    					array(
    						'id' => 'logo_width',
    						'type' => 'text',
    						'title' => __('Logo Width', Redux_TEXT_DOMAIN),
    						'sub_desc' => __('Please enter the width of your logo here (standard size), so that it is restricted for the retina version. Numerical value (no px).', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => '0',
    						'class' => 'mini'
    						),
    					array(
    						'id' => 'logo_height',
    						'type' => 'text',
    						'title' => __('Logo Height', Redux_TEXT_DOMAIN),
    						'sub_desc' => __('Please enter the height of your logo here (standard size). This is optional, and if you do not provide anything here then the logo area height will be restricted to 42px. Numerical value (no px).', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => '',
    						'class' => 'mini'
    						),
    					array(
    						'id' => 'logo_resized_height',
    						'type' => 'text',
    						'title' => __('Logo Resized Height', Redux_TEXT_DOMAIN),
    						'sub_desc' => __('Please enter the height you would like your logo to be when the sticky header resizes. Only works when logo height is set above. Numerical value (no px).', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => '',
    						'class' => 'mini'
    						),
    					array(
    						'id' => 'logo_top_spacing',
    						'type' => 'text',
    						'title' => __('Logo Top spacing', Redux_TEXT_DOMAIN),
    						'sub_desc' => '',
    						'desc' => '',
    						'std' => '0',
    						'class' => 'mini'
    						),
    					array(
    						'id' => 'logo_bottom_spacing',
    						'type' => 'text',
    						'title' => __('Logo Bottom spacing', Redux_TEXT_DOMAIN),
    						'sub_desc' => '',
    						'desc' => '',
    						'std' => '0',
    						'class' => 'mini'
    						),
    					array(
    						'id' => 'header_opacity',
    						'type' => 'slider',
    						'title' => __('Header Opacity', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Select the percentage opacity of the header. NOTE: This is only for Headers 3/4/5.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'from' => '0',
    						'to' => '100',
    						'step' => '5',
    						'unit' => '',
    						'std' => '100'
    						),
    					array(
    						'id' => 'nav_top_spacing',
    						'type' => 'text',
    						'title' => __('Main Nav Top spacing', Redux_TEXT_DOMAIN),
    						'sub_desc' => 'Add spacing here if you need extra spacing above the main navigation (i.e. if you have a large logo). Numerical value (no px).',
    						'desc' => '',
    						'std' => '0',
    						'class' => 'mini'
    						),
    					array(
    						'id' => 'enable_mini_header',
    						'type' => 'button_set',
    						'title' => __('Sticky header', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the sticky header when scrolling down the page.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						),
    					array(
    						'id' => 'sticky_header_mobile',
    						'type' => 'button_set',
    						'title' => __('Sticky header on mobile/tablet', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the sticky header for mobile and tablets.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '0'
    						),
    					array(
    						'id' => 'enable_logo_fade',
    						'type' => 'button_set',
    						'title' => __('Logo hover fade', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the fade effect when you hover the logo.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						),
    					array(
    						'id' => 'enable_header_shadow',
    						'type' => 'button_set',
    						'title' => __('Header Shadow', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the shadow below the header.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						),
    					array(
    						'id' => 'header_search_type',
    						'type' => 'button_set',
    						'title' => __('Header Search', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the search icon in the header menu.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('search-1' => 'Standard (Fancy)','search-2' => 'Overlay with AJAX', 'search-off' => 'Search disabled'),
    						'std' => 'search-1'
    						)
    					)
    				);
    $sections[] = array(
    				'icon_type' => 'image',
    				'icon' => Redux_OPTIONS_URL.'img/footer.png',
    				'title' => __('Footer Options', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">These are the options for the footer.</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(
    					array(
    						'id' => 'enable_footer',
    						'type' => 'button_set',
    						'title' => __('Enable Footer', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the footer widgets section.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						),
    					array(
    						'id' => 'enable_footer_divider',
    						'type' => 'button_set',
    						'title' => __('Footer Divider', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the footer divider above the footer.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						),
    					array(
    						'id' => 'footer_layout',
    						'type' => 'radio_img',
    						'title' => __('Footer Layout', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Select the footer column layout.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array(
    										'footer-1' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-1.png'),
    										'footer-2' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-2.png'),
    										'footer-3' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-3.png'),
    										'footer-4' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-4.png'),
    										'footer-5' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-5.png'),
    										'footer-6' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-6.png'),
    										'footer-7' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-7.png'),
    										'footer-8' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-8.png'),
    										'footer-9' => array('title' => '', 'img' => Redux_OPTIONS_URL.'img/footer-9.png'),
    											),
    						'std' => 'footer-1'
    						),
    					array(
    						'id' => 'enable_copyright',
    						'type' => 'button_set',
    						'title' => __('Enable Copyright', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the footer copyright section.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						),
    					array(
    						'id' => 'enable_copyright_divider',
    						'type' => 'button_set',
    						'title' => __('Copyright Divider', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Enable the copyright divider above the copyright.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						),
    					array(
    						'id' => 'footer_copyright_text',
    						'type' => 'textarea',
    						'title' => __('Footer Copyright Text', Redux_TEXT_DOMAIN),
    						'sub_desc' => 'The copyright text that appears in the footer.',
    						'desc' => '',
    						'std' => "Copyright © 2008 - 2013 Wordpress Themes by <a href='http://www.rivertheme.com/'>RiverTheme.com</a>. All rights reserved."
    						),
    					array(
    						'id' => 'show_backlink',
    						'type' => 'button_set',
    						'title' => __('Show Cootheme Backlink', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('If checked, a backlink to our site will be shown in the footer. This is not compulsory, but always appreciated :)', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('1' => 'On','0' => 'Off'),
    						'std' => '1'
    						)
    					)
    				);
	
	$sections[] = array(
					'icon' => 'star',
					'icon_class' => 'fa-lg',
					'title' => __('Page Builder Options', Redux_TEXT_DOMAIN),
					'desc' => __('<p class="description">These are the general options for the Coo Page Builder</p>', Redux_TEXT_DOMAIN),
					'fields' => array(	
						array(
							'id' => 'advanced_pb',
							'type' => 'button_set',
							'title' => __('Enable Advanced Functionality', Redux_TEXT_DOMAIN), 
							'sub_desc' => __('Enable advanced functionality within the page builder, this includes the ability to add columns. This mode is expermiental as of yet, so you may see issues using columns.', Redux_TEXT_DOMAIN),
							'desc' => '',
							'options' => array('1' => 'On','0' => 'Off'),
							'std' => '0'
							)
						)
					);

    if ( ct_is_current_color_settings_empty() ){
    	   	
    	$sections[] = array(
    				'icon' => 'pencil',
    				'icon_class' => 'fa-lg',
    				'title' => __('Colour Scheme Options', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">Create, import, and export color schemas.</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(
    					array(
    						'id' => 'colour_scheme_select_scheme',
    						'type' => 'select',
    						'title' => __('Select an existing colour scheme to preview', Redux_TEXT_DOMAIN),
    						'sub_desc' => "",
    						'options' => ct_get_color_scheme_list(),
    						'desc' => '',
    						'std' => ct_get_current_color_scheme_id()
    						),
    					array(
    					    'id' => 'colour_scheme_import',
    					    'type' => 'upload_scheme',
    					    'title' => __('Import a Color Scheme', Redux_TEXT_DOMAIN), 
    					    'sub_desc' => __('File must be csv format.', Redux_TEXT_DOMAIN)
    						),
    					array(
    					    'id' => 'colour_scheme_export',
    					    'type' => 'raw_html_narrow',
    					    'title' => __('Export Current Settings As Schema', Redux_TEXT_DOMAIN), 
    					    'sub_desc' => __('Export the CURRENT COLORS IN THE SCHEMA PREVIEW as a csv file.', Redux_TEXT_DOMAIN),
    					    'html' => ct_export_color_scheme_html()
    						),
    					array(
    					    'id' => 'colour_scheme_preview',
    					    'type' => 'raw_html_narrow',
    					    'title' => __('Color Scheme Preview', Redux_TEXT_DOMAIN), 
    					    'sub_desc' => __('<span id="scheme-preview-text">These colors are what currently exist in the WordPress theme customizer.</span>'
    					    				 .'<div class="scheme-buttons" id="scheme-buttons">'
    					    				 .'<input class="save-this-scheme-name" name="save-this-scheme-name" placeholder="Name This Scheme"   style="display:none;" />'					    				 
    					    				 .'<a class="save-this-scheme button-secondary"   style="display:none;">Save This Scheme</a>'
    					    				 .'<a class="delete-this-scheme button-secondary"  style="display:none;">Delete This Scheme</a>'
    					    				 .'<a class="use-this-scheme button-secondary"  style="display:none;">Use This Scheme</a>'
    					    				 .'</div>', Redux_TEXT_DOMAIN),
    					    'html' => ct_get_current_color_scheme_html_preview()
    						)
    					)
    
    				);
    	   	
       	} else {
       	
    	$sections[] = array(
				'icon' => 'pencil',
				'icon_class' => 'fa-lg',
				'title' => __('Colour Scheme Options', Redux_TEXT_DOMAIN),
				'desc' => __('<p class="description">Create, import, and export color schemas.</p>', Redux_TEXT_DOMAIN),
				'fields' => array(
					array(
						'id' => 'colour_scheme_select_scheme',
						'type' => 'select',
						'title' => __('Select an existing colour scheme to preview', Redux_TEXT_DOMAIN),
						'sub_desc' => "",
						'options' => ct_get_color_scheme_list(),
						'desc' => '',
						'std' => ct_get_current_color_scheme_id()
						),
					array(
					    'id' => 'colour_scheme_import',
					    'type' => 'upload_scheme',
					    'title' => __('Import a Color Scheme', Redux_TEXT_DOMAIN), 
					    'sub_desc' => __('File must be csv format.', Redux_TEXT_DOMAIN)
						),
					array(
					    'id' => 'colour_scheme_export',
					    'type' => 'raw_html_narrow',
					    'title' => __('Export Current Settings As Schema', Redux_TEXT_DOMAIN), 
					    'sub_desc' => __('Export the CURRENT COLORS IN THE SCHEMA PREVIEW as a csv file.', Redux_TEXT_DOMAIN),
					    'html' => ct_export_color_scheme_html()
						),
					array(
					    'id' => 'colour_scheme_preview',
					    'type' => 'raw_html_narrow',
					    'title' => __('Color Scheme Preview', Redux_TEXT_DOMAIN), 
					    'sub_desc' => __('<span id="scheme-preview-text">These colors are what currently exist in the WordPress theme customizer.</span>'
					    				 .'<div class="scheme-buttons" id="scheme-buttons">'
					    				 .'<input class="save-this-scheme-name" name="save-this-scheme-name" placeholder="Name This Scheme" />'					    				 
					    				 .'<a class="save-this-scheme button-secondary">Save This Scheme</a>'
					    				 .'<a class="delete-this-scheme button-secondary"  style="display:none;">Delete This Scheme</a>'
					    				 .'<a class="use-this-scheme button-secondary"  style="display:none;">Use This Scheme</a>'
					    				 .'</div>', Redux_TEXT_DOMAIN),
					    'html' => ct_get_current_color_scheme_html_preview()
						)
					)

				);	   	
    	   	
    }
    
    $sections[] = array(
    				'icon' => 'tasks',
    				'icon_class' => 'fa-lg',
    				'title' => __('Default Meta Options', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">These are the options to set the defaults for the meta options.</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(

    					array(
    						'id' => 'default_sidebar_config',
    						'type' => 'select',
    						'title' => __('Default Sidebar Config', Redux_TEXT_DOMAIN),
    						'sub_desc' => "Choose the default sidebar config for pages/posts",
    						'options' => array(
    							'0-m-0'		=> 'No Sidebars',
                                '1-m-0'		=> 'Left Sidebar',
                                '0-m-1'		=> 'Right Sidebar',
    							'1-m-1'		=> 'Both Sidebars'
    						),
    						'desc' => '',
    						'std' => '0-m-0'
    						),
    					array(
    						'id' => 'default_left_sidebar',
    						'type' => 'select',
    						'title' => __('Default Left Sidebar', Redux_TEXT_DOMAIN),
    						'sub_desc' => "Choose the default left sidebar for pages/posts",
    						'options' => ct_sidebars_array(),
    						'desc' => '',
    						'std' => 'Sidebar-1'
    						),
    					array(
    						'id' => 'default_right_sidebar',
    						'type' => 'select',
    						'title' => __('Default Right Sidebar', Redux_TEXT_DOMAIN),
    						'sub_desc' => "Choose the default right sidebar for pages/posts",
    						'options' => ct_sidebars_array(),
    						'desc' => '',
    						'std' => 'Sidebar-1'
    						)
    					)
    				);

   	$sections[] = array(
   					'icon' => 'list',
   					'icon_class' => 'fa-lg',
   					'title' => __('Archive/Category Options', Redux_TEXT_DOMAIN),
   					'desc' => __('<p class="description">These are the options for the archive/category pages.</p>', Redux_TEXT_DOMAIN),
   					'fields' => array(
   						array(
   							'id' => 'archive_sidebar_config',
   							'type' => 'select',
   							'title' => __('Sidebar Config', Redux_TEXT_DOMAIN),
   							'sub_desc' => "Choose the sidebar configuration for the archive/category pages.",
   							'options' => array(
   								'0-m-0'		=> 'No Sidebars',
   								'1-m-0'		=> 'Left Sidebar',
   								'0-m-1'		=> 'Right Sidebar',
   								'1-m-1'		=> 'Both Sidebars'
   								),
   							'desc' => '',
   							'std' => 'right-sidebar'
   							),
   						array(
   							'id' => 'archive_sidebar_left',
   							'type' => 'select',
   							'title' => __('Left Sidebar', Redux_TEXT_DOMAIN),
   							'sub_desc' => "Choose the left sidebar for Left/Both sidebar configs.",
   							'options' => ct_sidebars_array(),
   							'desc' => '',
   							'std' => 'Sidebar-1'
   							),
   						array(
   							'id' => 'archive_sidebar_right',
   							'type' => 'select',
   							'title' => __('Right Sidebar', Redux_TEXT_DOMAIN),
   							'sub_desc' => "Choose the left sidebar for Right/Both sidebar configs.",
   							'options' => ct_sidebars_array(),
   							'desc' => '',
   							'std' => 'Sidebar-1'
   							)
   						)
   					);
    $sections[] = array(
    				'icon' => 'font',
    				'icon_class' => 'fa-lg',
    				'title' => __('Font Options', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">These are the options for fonts used within the theme.</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(
    					array(
    						'id' => 'body_font_option',
    						'type' => 'button_set',
    						'title' => __('Body Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Choose the type of font that you want to use for the body text.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('default' => 'Default', 'standard' => 'Standard','google' => 'Google', 'fontdeck' => 'FontDeck'),
    						'std' => 'default'
    						),
    					array(
    						'id' => 'web_body_font',
    						'type' => 'select',
    						'title' => __('Body Standard Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('The font that is used as the body text and other small text throughout the theme.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array(
    										'Arial' => 'Arial',
    										'Courier New' => 'Courier New',
    										'Georgia' => 'Georgia',
    										'Helvetica' => 'Helvetica',
    										'Lucida Sans' => 'Lucida Sans',
    										'Lucida Sans Unicode' => 'Lucida Sans Unicode',
    										'Myriad Pro' => 'Myriad Pro',
    										'Palatino Linotype' => 'Palatino Linotype',
    										'Tahoma' => 'Tahoma',
    										'Times New Roman' => 'Times New Roman',
    										'Trebuchet MS' => 'Trebuchet MS',
    										'Verdana' => 'Verdana'
    										),
    						'std' => 'Helvetica'
    						),
    					array(
    						'id' => 'google_standard_font',
    						'type' => 'google_webfonts',
    						'title' => __('Standard Google Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('The font that is used as the body text and other small text throughout the theme.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'placeholder' => 'Default Font'
    						),
    					array(
    						'id' => 'fontdeck_standard_font',
    						'type' => 'textarea',
    						'title' => __('Standard FontDeck Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Paste the css here that can be found from step 2 of the FontDeck instructions (<a href="http://cootheme.com/img/fontdeck_step2.png" class="view" target="_blank">view</a>). NOTE: Make sure you provide the JS code in the box at the bottom for this/all FontDeck fonts you want to use.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => ''
    						),
    					array(
    						'id' => 'font_divide_a',
    						'type' => 'divide'
    						),
    					array(
    						'id' => 'headings_font_option',
    						'type' => 'button_set',
    						'title' => __('Headings Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Choose the type of font that you want to use for the body text.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('default' => 'Default', 'standard' => 'Standard','google' => 'Google', 'fontdeck' => 'FontDeck'),
    						'std' => 'default'
    						),
    					array(
    						'id' => 'web_heading_font',
    						'type' => 'select',
    						'title' => __('Heading Standard Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('The font that is used for the headings throughout the theme.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array(
    										'Arial' => 'Arial',
    										'Courier New' => 'Courier New',
    										'Georgia' => 'Georgia',
    										'Helvetica' => 'Helvetica',
    										'Lucida Sans' => 'Lucida Sans',
    										'Lucida Sans Unicode' => 'Lucida Sans Unicode',
    										'Myriad Pro' => 'Myriad Pro',
    										'Palatino Linotype' => 'Palatino Linotype',
    										'Tahoma' => 'Tahoma',
    										'Times New Roman' => 'Times New Roman',
    										'Trebuchet MS' => 'Trebuchet MS',
    										'Verdana' => 'Verdana'
    										),
    						'std' => 'Helvetica'
    						),
    					array(
    						'id' => 'google_heading_font',
    						'type' => 'google_webfonts',//doesnt need to be called for callback fields
    						'title' => __('Headings Google Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('The font that is used for the headings throughout the theme.', Redux_TEXT_DOMAIN),
    						'desc' => ''
    						),
    					array(
    						'id' => 'fontdeck_heading_font',
    						'type' => 'textarea',
    						'title' => __('Heading FontDeck Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Paste the css here that can be found from step 2 of the FontDeck instructions (<a href="http://cootheme.com/img/fontdeck_step2.png" class="view" target="_blank">view</a>). NOTE: Make sure you provide the JS code in the box at the bottom for this/all FontDeck fonts you want to use.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => ''
    						),
    					array(
    						'id' => 'font_divide_b',
    						'type' => 'divide'
    						),
    					array(
    						'id' => 'menu_font_option',
    						'type' => 'button_set',
    						'title' => __('Menu Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Choose the type of font that you want to use for the menu text.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array('default' => 'Default', 'standard' => 'Standard','google' => 'Google', 'fontdeck' => 'FontDeck'),
    						'std' => 'default'
    						),
    					array(
    						'id' => 'web_menu_font',
    						'type' => 'select',
    						'title' => __('Menu Standard Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('The font that is used for the menu.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'options' => array(
    										'Arial' => 'Arial',
    										'Courier New' => 'Courier New',
    										'Georgia' => 'Georgia',
    										'Helvetica' => 'Helvetica',
    										'Lucida Sans' => 'Lucida Sans',
    										'Lucida Sans Unicode' => 'Lucida Sans Unicode',
    										'Myriad Pro' => 'Myriad Pro',
    										'Palatino Linotype' => 'Palatino Linotype',
    										'Tahoma' => 'Tahoma',
    										'Times New Roman' => 'Times New Roman',
    										'Trebuchet MS' => 'Trebuchet MS',
    										'Verdana' => 'Verdana'
    										),
    						'std' => 'Helvetica'
    						),
    					array(
    						'id' => 'google_menu_font',
    						'type' => 'google_webfonts',//doesnt need to be called for callback fields
    						'title' => __('Menu Google Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('The font that is used for the menu.', Redux_TEXT_DOMAIN),
    						'desc' => ''
    						),
    					array(
    						'id' => 'fontdeck_menu_font',
    						'type' => 'textarea',
    						'title' => __('Menu FontDeck Font', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Paste the css here that can be found from step 2 of the FontDeck instructions (<a href="http://cootheme.com/img/fontdeck_step2.png" class="view" target="_blank">view</a>). NOTE: Make sure you provide the JS code in the box at the bottom for this/all FontDeck fonts you want to use.', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => ''
    						),
    					array(
    						'id' => 'font_divide_c',
    						'type' => 'divide'
    						),
    					array(
    						'id' => 'fontdeck_js',
    						'type' => 'textarea',
    						'title' => __('FontDeck JS Code', Redux_TEXT_DOMAIN), 
    						'sub_desc' => __('Paste the js code here that can be found from step 1 of the FontDeck instructions (<a href="http://cootheme.com/img/fontdeck_step1.png" class="view" target="_blank">view</a>).', Redux_TEXT_DOMAIN),
    						'desc' => '',
    						'std' => ''
    						),
    					)
    				);
    				


    $sections[] = array(
    				'icon' => 'shopping-cart',
    				'icon_class' => 'fa-lg',
    				'title' => __('WooCommerce Options', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">These are the options for the WooCommerce pages.</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(
    						array(
    							'id' => 'product_overlay_transition',
    							'type' => 'button_set',
    							'title' => __('Product Overlay Transition', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('Choose whether you would like the product overlay transition to be enabled.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'options' => array('1' => 'On','0' => 'Off'),
    							'std' => '1'
    							),
    						array(
    							'id' => 'enable_pb_product_pages',
    							'type' => 'button_set',
    							'title' => __('Page Builder on Product Pages', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('Choose whether you would like the page builder to be enabled on product pages or not. If it is enabled, then the description accordion will use the "Short Description" content, and the page builder content will appear below the images/details area.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'options' => array('1' => 'On','0' => 'Off'),
    							'std' => '0'
    							),
    						array(
    							'id' => 'enable_catalog_mode',
    							'type' => 'button_set',
    							'title' => __('Catalog Mode', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('Enable this setting to set the products into catalog mode, with no price or checkout process.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'options' => array('1' => 'On','0' => 'Off'),
    							'std' => '0'
    							),
    						array(
    							'id' => 'enable_default_tabs',
    							'type' => 'button_set',
    							'title' => __('Product Description Tabs Mode', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('Enable this setting to revert to the default product description styling tabs - this will allow you to use extensions that add extra tabs to the product tabs.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'options' => array('1' => 'On','0' => 'Off'),
    							'std' => '0'
    							),
    						array(
    							'id' => 'enable_product_zoom',
    							'type' => 'button_set',
    							'title' => __('Enable image zoom on product images', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('Choose whether you would like to enable product image zoom functionality on the product detail page images.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'options' => array('1' => 'On','0' => 'Off'),
    							'std' => '0'
    							),
    						array(
    							'id' => 'woo_sidebar_config',
    							'type' => 'select',
    							'title' => __('WooCommerce Sidebar Config', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the sidebar config for WooCommerce shop/category pages.",
    							'options' => array(
    								'no-sidebars'		=> 'No Sidebars',
    								'left-sidebar'		=> 'Left Sidebar',
    								'right-sidebar'		=> 'Right Sidebar',
    								'both-sidebars'		=> 'Both Sidebars'
    							),
    							'desc' => '',
    							'std' => 'no-sidebars'
    							),
    						array(
    							'id' => 'woo_left_sidebar',
    							'type' => 'select',
    							'title' => __('WooCommerce Left Sidebar', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the left sidebar for WooCommerce shop/category pages.",
    							'options' => ct_sidebars_array(),
    							'desc' => '',
    							'std' => 'woocommerce-sidebar'
    							),
    						array(
    							'id' => 'woo_right_sidebar',
    							'type' => 'select',
    							'title' => __('WooCommerce Right Sidebar', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the right sidebar for WooCommerce shop/category pages.",
    							'options' => ct_sidebars_array(),
    							'desc' => '',
    							'std' => 'woocommerce-sidebar'
    							),
    						array(
    							'id' => 'woo_divide_0',
    							'type' => 'divide'
    							),
    						array(
    							'id' => 'woo_show_page_heading',
    							'type' => 'button_set',
    							'title' => __('Default Show Page Heading', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('Show page title on shop/category WooCommerce page.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'options' => array('1' => 'On','0' => 'Off'),
    							'std' => '1'
    							),
    						array(
    							'id' => 'woo_page_heading_style',
    							'type' => 'select',
    							'title' => __('WooCommerce Page Heading Style', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the page heading style for the shop/category WooCommerce pages.",
    							'options' => array(
    								'standard'		=> 'Standard',
    								'fancy'		=> 'Fancy'
    								),
    							'desc' => '',
    							'std' => 'standard'
    							),
    						array(
    							'id' => 'woo_page_heading_bg_alt',
    							'type' => 'select',
    							'title' => __('WooCommerce Page Heading Background', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the alt background configuration for the shop/category WooCommerce page headings.",
    							'options' => array(
    								'none'		=> 'None',
    								'alt-one'		=> 'Alt 1',
    								'alt-two'		=> 'Alt 2',
    								'alt-three'		=> 'Alt 3',
    								'alt-four'		=> 'Alt 4',
    								'alt-five'		=> 'Alt 5',
    								'alt-six'		=> 'Alt 6',
    								'alt-seven'		=> 'Alt 7',
    								'alt-eight'		=> 'Alt 8',
    								'alt-nine'		=> 'Alt 9',
    								'alt-ten'		=> 'Alt 10'
    								),
    							'desc' => '',
    							'std' => 'none'
    							),
    						array(
    							'id' => 'woo_page_heading_image',
    							'type' => 'upload',
    							'title' => __('WooCommerce Fancy Heading Background Image', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('Upload the fancy heading background image for WooCommerce page heading (Fancy Heading Only).', Redux_TEXT_DOMAIN),
    							'desc' => ''
    							),
    						array(
    							'id' => 'woo_page_heading_text_style',
    							'type' => 'select',
    							'title' => __('WooCommerce Fancy Heading Text Style', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the text style for the WooCommerce page heading (Fancy Heading Only).",
    							'options' => array(
    								'light'		=> 'Light',
    								'dark'		=> 'Dark'
    								),
    							'desc' => '',
    							'std' => 'light'
    							),
    						array(
    							'id' => 'woo_divide_1',
    							'type' => 'divide'
    							),
    						array(
    							'id' => 'default_product_sidebar_config',
    							'type' => 'select',
    							'title' => __('Default Product Sidebar Config', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the sidebar config for WooCommerce shop/category pages.",
    							'options' => array(
    								'no-sidebars'		=> 'No Sidebars',
    								'left-sidebar'		=> 'Left Sidebar',
    								'right-sidebar'		=> 'Right Sidebar',
    								'both-sidebars'		=> 'Both Sidebars'
    							),
    							'desc' => '',
    							'std' => 'no-sidebars'
    							),
    						array(
    							'id' => 'default_product_left_sidebar',
    							'type' => 'select',
    							'title' => __('Default Product Left Sidebar', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the default left sidebar for WooCommerce product pages.",
    							'options' => ct_sidebars_array(),
    							'desc' => '',
    							'std' => 'woocommerce-sidebar'
    							),
    						array(
    							'id' => 'default_product_right_sidebar',
    							'type' => 'select',
    							'title' => __('Default Product Right Sidebar', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Choose the default right sidebar for WooCommerce product pages.",
    							'options' => ct_sidebars_array(),
    							'desc' => '',
    							'std' => 'woocommerce-sidebar'
    							),
    						array(
    							'id' => 'woo_divide_2',
    							'type' => 'divide'
    							),
    						array(
    							'id' => 'checkout_new_account_text',
    							'type' => 'textarea',
    							'title' => __('New account text', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('This text appears in the sign in / sign up area of the checkout process.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'std' => 'Creating an account with Coo is quick and easy, and will allow you to move through our checkout quicker. You can also store multiple shipping addresses, gain access to your order history, and much more.'
    							),
    						array(
    							'id' => 'help_bar_text',
    							'type' => 'text',
    							'title' => __('Help Bar Text', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('This text appears in the help bar on account / checkout pages.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'std' => 'Need help? Call customer services on 0800 123 4567.'
    							),
    						array(
    							'id' => 'email_modal',
    							'type' => 'textarea',
    							'title' => __('Email customer care modal', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('The content that appears in the modal box for the email customer care help link.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'std' => 'Enter your contact details or email form shortcode here. (Text/HTML/Shortcodes accepted).'
    							),
    						array(
    							'id' => 'shipping_modal',
    							'type' => 'textarea',
    							'title' => __('Email customer care modal', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('The content that appears in the modal box for the shipping information help link.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'std' => 'Enter your shipping information here. (Text/HTML/Shortcodes accepted).'
    							),
    						array(
    							'id' => 'returns_modal',
    							'type' => 'textarea',
    							'title' => __('Returns & exchange modal', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('The content that appears in the modal box for the returns & exchange help link.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'std' => 'Enter your returns and exchange information here. (Text/HTML/Shortcodes accepted).'
    							),
    						array(
    							'id' => 'faqs_modal',
    							'type' => 'textarea',
    							'title' => __('FAQs modal', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('The content that appears in the modal box for the faqs help link.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'std' => 'Enter your faqs here. (Text/HTML/Shortcodes accepted).'
    							),
    						array(
    							'id' => 'feedback_modal',
    							'type' => 'textarea',
    							'title' => __('Feedback modal', Redux_TEXT_DOMAIN),
    							'sub_desc' => __('The content that appears in the modal box for the leave feedback link.', Redux_TEXT_DOMAIN),
    							'desc' => '',
    							'std' => 'Enter your feedback modal content here. (Text/HTML/Shortcodes accepted).'
    							),
    					)
    				);
    $sections[] = array(
    				'icon' => 'twitter',
                    'id'   =>'social_network',
    				'icon_class' => 'fa-lg',
    				'title' => __('Social Profiles', Redux_TEXT_DOMAIN),
    				'desc' => __('<p class="description">These are the fields that power the social shortcode. If you include a link/username here, then the icon will be included in the shortcodes output.</p>', Redux_TEXT_DOMAIN),
    				'fields' => array(
    						array(
    							'id' => 'twitter_username',
    							'type' => 'text',
    							'title' => __('Twitter', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Twitter username (no @).",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'facebook_page_url',
    							'type' => 'text',
    							'title' => __('Facebook', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your facebook page/profile url",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'dribbble_username',
    							'type' => 'text',
    							'title' => __('Dribbble', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Dribbble username",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'vimeo_username',
    							'type' => 'text',
    							'title' => __('Vimeo', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Vimeo username",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'tumblr_username',
    							'type' => 'text',
    							'title' => __('Tumblr', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Tumblr username",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'skype_username',
    							'type' => 'text',
    							'title' => __('Skype', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Skype username",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'linkedin_page_url',
    							'type' => 'text',
    							'title' => __('LinkedIn', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your LinkedIn page/profile url",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'googleplus_page_url',
    							'type' => 'text',
    							'title' => __('Google+', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Google+ page/profile URL",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'flickr_page_url',
    							'type' => 'text',
    							'title' => __('Flickr', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Flickr page url",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'youtube_url',
    							'type' => 'text',
    							'title' => __('YouTube', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your YouTube URL",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'pinterest_username',
    							'type' => 'text',
    							'title' => __('Pinterest', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Pinterest username",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'foursquare_url',
    							'type' => 'text',
    							'title' => __('Foursquare', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Foursqaure URL",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'instagram_username',
    							'type' => 'text',
    							'title' => __('Instagram', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Instagram username",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'github_url',
    							'type' => 'text',
    							'title' => __('GitHub', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your GitHub URL",
    							'desc' => '',
    							'std' => ''
    							),
    						array(
    							'id' => 'xing_url',
    							'type' => 'text',
    							'title' => __('Xing', Redux_TEXT_DOMAIN),
    							'sub_desc' => "Your Xing URL",
    							'desc' => '',
    							'std' => ''
    							)
    					)
    				);
    		                
    $tabs = array();

    if (function_exists('wp_get_theme')) {
        $theme_data = wp_get_theme();
        $item_uri = $theme_data->get('ThemeURI');
        $description = $theme_data->get('Description');
        $author = $theme_data->get('Author');
        $author_uri = $theme_data->get('AuthorURI');
        $version = $theme_data->get('Version');
        $tags = $theme_data->get('Tags');
    }

    global $Redux_Options;
    $Redux_Options = new Redux_Options($sections, $args, $tabs);

}
add_action('init', 'setup_framework_options', 5);

/*
 * 
 * Custom function for the callback referenced above
 *
 */
function my_custom_field($field, $value) {
    print_r($field);
    print_r($value);
}

/*
 * 
 * Custom function for the callback validation referenced above
 *
 */
function validate_callback_function($field, $value, $existing_value) {
    $error = false;
    $value =  'just testing';
    /*
    do your validation
    
    if(something) {
        $value = $value;
    } elseif(somthing else) {
        $error = true;
        $value = $existing_value;
        $field['msg'] = 'your custom error message';
    }
    */
    
    $return['value'] = $value;
    if($error == true) {
        $return['error'] = $field;
    }
    return $return;
}