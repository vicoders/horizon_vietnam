<?php

require_once dirname(__FILE__).'/vendor/minify/css.php';
require_once dirname(__FILE__).'/vendor/minify/jsshrink.php';

class CT_Assets {

    public $_scripts = array();
    public $_styles = array();

    public static function checkDateModify($input_file, $output_file) {

        $output_file = str_replace(get_template_directory_uri(), get_template_directory(), $output_file);
        if(!file_exists($output_file))
            return true;

        if(is_array($input_file)) {
            foreach($input_file as $file) {
                $file = str_replace(get_template_directory_uri(), get_template_directory(), $file);
                if(!filter_var($file, FILTER_VALIDATE_URL)){
                    if(filemtime($file) > filemtime($output_file))
                        return true;
                }
            }
        } else {
            if(filemtime($input_file) > filemtime($output_file))
                return true;
        }


        return true;
    }


    public function add_style($files) {
        $styles = $this->_styles;
        if(is_array($files)) {
            foreach($files as $file) {
                array_push($styles, $file);
            }
        }else {
            array_push($styles, $files);
        }
        $this->_styles = $styles;

        //array_push($this->_styles, $file);
    }

    public function add_script($files) {
        $scripts = $this->_scripts;
        if(is_array($files)) {
            foreach($files as $file) {
                array_push($scripts, $file);
            }
        }else {
            array_push($scripts, $files);
        }
        $this->_scripts = $scripts;

        //array_push($this->_scripts, $file);
    }

    public function minify_style($output_file) {
        if( !file_exists($output_file) || $this->checkDateModify($this->_styles, $output_file)) {
            $content = '';
            foreach($this->_styles as $style_path) {
                if($style_path){
                    $style_path = str_replace(get_template_directory_uri(), get_template_directory(), $style_path);
                    $content .= CssMinifier::minify(file_get_contents($style_path));
                }
            }
            return file_put_contents($output_file, $content);
        }
        return false;
    }

    public function minify_script($output_file) {
        if( !file_exists($output_file) || $this->checkDateModify($this->_scripts, $output_file)) {
            $content = '';
            foreach($this->_scripts as $script_path) {
                if($script_path) {
                    $script_path = str_replace(get_template_directory_uri(), get_template_directory(), $script_path);
                    $content .= Minifier::minify(file_get_contents($script_path));
                    //$content .= file_get_contents($script_path);
                }
            }
            return file_put_contents($output_file, $content);
        }
        return false;
    }
}
