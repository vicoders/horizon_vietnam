<?php get_header(); ?>

<?php
	$options = get_option('ct_coo_options');
    //config default sidebar on the post, page
    $default_sidebar_config = $options['default_sidebar_config'];
    $default_left_sidebar = $options['default_left_sidebar'];
    $default_right_sidebar = $options['default_right_sidebar'];

    //config sidebar meta post, page
    $sidebar_config = get_post_meta($post->ID, 'ct_sidebar_config', true);
    $left_sidebar = get_post_meta($post->ID, 'ct_left_sidebar', true);
    $right_sidebar = get_post_meta($post->ID, 'ct_right_sidebar', true);

    if ($sidebar_config == "0-m-0") {
        $sidebar_config = $default_sidebar_config;
    }
    if ($left_sidebar == "") {
        $left_sidebar = $default_left_sidebar;
    }
    if ($right_sidebar == "") {
        $right_sidebar = $default_right_sidebar;
    }
    $config =configLayout(of_get_option('single-layout',$sidebar_config));

	$show_page_title = get_post_meta($post->ID, 'ct_page_title', true);
	$page_title_style = get_post_meta($post->ID, 'ct_page_title_style', true);
	$page_title = get_post_meta($post->ID, 'ct_page_title_one', true);
	$page_subtitle = get_post_meta($post->ID, 'ct_page_subtitle', true);
	$page_title_bg = get_post_meta($post->ID, 'ct_page_title_bg', true);
	$fancy_title_image = rwmb_meta('ct_page_title_image', 'type=image&size=full');
	$page_title_text_style = get_post_meta($post->ID, 'ct_page_title_text_style', true);
	$fancy_title_image_url = "";
	foreach ($fancy_title_image as $detail_image) {
		$fancy_title_image_url = $detail_image['url'];
		break;
	}
    $title_image_url='';
	if (!$fancy_title_image) {
		$fancy_title_image = get_post_thumbnail_id();
		$fancy_title_image_url = wp_get_attachment_url( $fancy_title_image, 'full' );
	}



	$remove_breadcrumbs = get_post_meta($post->ID, 'ct_no_breadcrumbs', true);
	$remove_bottom_spacing = get_post_meta($post->ID, 'ct_no_bottom_spacing', true);
	$remove_top_spacing = get_post_meta($post->ID, 'ct_no_top_spacing', true);

	if ($remove_bottom_spacing) {
	$page_wrap_class .= ' no-bottom-spacing';
	}
	if ($remove_top_spacing) {
	$page_wrap_class .= ' no-top-spacing';
	}
    $image_title='';
    $title_image = rwmb_meta('ct_title_image', 'type=image&size=img-title');
    if (is_array($title_image)) {
        foreach ($title_image as $image) {
            $title_image_url = $image['url'];
            break;
        }
    }

?>

    <!--// OPEN #page-wrap //-->

    <?php

    if((is_home() || is_front_page())){
        global $post;
        $show_posts_slider = get_post_meta($post->ID, 'ct_posts_slider', true);
        $rev_slider_alias = get_post_meta($post->ID, 'ct_rev_slider_alias', true);
        $layerSlider_ID = get_post_meta($post->ID, 'ct_layerslider_id', true);
        if ($show_posts_slider) {
            ct_coo_slider();
        } else if ($rev_slider_alias != "") { ?>
            <div class="home-slider-wrap">
                <?php putRevSlider($rev_slider_alias); ?>
            </div>
        <?php } else if ($layerSlider_ID != "") { ?>
            <div class="home-slider-wrap clearfix">
                <?php echo do_shortcode('[layerslider id="'.$layerSlider_ID.'"]'); ?>
            </div>
        <?php }
    }
    else{
        ?>
        <?php if($title_image_url){
            ?>
            <div class="row">
                <div class="title-image col-md-12">
                    <?php echo $image = '<img itemprop="image" src="'.$title_image_url.'" alt="'.$image_title.'" />';?>
                </div>
            </div>
        <?php
        }?>
        <div class="page-heading col-sm-12 clearfix  <?php echo $page_title_bg; ?>">
            <?php
            // BREADCRUMBS
            if (!$remove_breadcrumbs) {
                echo ct_breadcrumbs();
            }
            ?>
        </div>
     <?php }?>
    
    <div class="inner-page-wrap row clearfix">
        <?php if (have_posts()) : the_post(); ?>
            <!-- Start.Main -->
            <div class="page-content <?php  echo $config['main']['class']; ?> clearfix">
                <div class="page-content-inner">
                    <?php the_content(); ?>
                    <div class="link-pages"><?php wp_link_pages(); ?></div>

                    <?php if ( comments_open() ) { ?>
                    <div id="comment-area">
                        <?php //comments_template('', true); ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End.Main -->
        <?php endif; ?>

            <!--Start.Sidebar-Left-->
                <?php if($config['left-sidebar']['show']){ ?>
                    <div class="left-sidebar <?php echo $config['left-sidebar']['class']; ?>">
                        <div class="sidebar-inner">
                            <?php dynamic_sidebar($left_sidebar); ?>
                        </div>
                    </div>
                <?php } ?>
                    <!--End.Sidebar-Left -->

                    <!--Start.Sidebar-Right-->
                <?php if($config['right-sidebar']['show']){ ?>
                    <div class=" right-sidebar <?php echo $config['right-sidebar']['class']; ?>">
                        <div class="sidebar-inner">
                            <?php dynamic_sidebar($right_sidebar); ?>
                        </div>
                    </div>
                <?php } ?>
             <!--End.Sidebar-right -->

    </div>



<!--// WordPress Hook //-->
<?php get_footer(); ?>