<?php get_header(); ?>

<?php
if (have_posts()) : the_post();
    //MEDIA TYPE
    $media_type = $media_image = $media_video = $media_gallery = '';

    $use_thumb_content = get_post_meta($post->ID, 'ct_thumbnail_content_main_detail', true);


    $post_format = get_post_format($post->ID);
    if ( $post_format == "" ) {
        $post_format = 'standard';
    }

    if ($use_thumb_content) {
        $media_type = get_post_meta($post->ID, 'ct_thumbnail_type', true);
    } else {
        $media_type = get_post_meta($post->ID, 'ct_detail_type', true);
    }


        $media_width = 770;
        $media_height = NULL;
        $video_height = 433;

    $figure_output = '';

    $figure_output .= '<figure class="media-wrap-aboutus " >';
    if ($post_format == "standard") {

        if ($media_type == "video") {

            $figure_output .= ct_video_post($post->ID, $media_width,$media_height, $use_thumb_content)."\n";

        } else if ($media_type == "slider") {

            $figure_output .= ct_gallery_post($post->ID, $use_thumb_content)."\n";

        } else if ($media_type == "layer-slider") {

            $figure_output .= '<div class="layerslider">'."\n";

            $figure_output .= do_shortcode('[rev_slider '.$media_slider.']')."\n";

            $figure_output .= '</div>'."\n";

        } else if ($media_type == "custom") {

            $figure_output .= $custom_media."\n";

        } else {

            $figure_output .= ct_image_post($post->ID, $media_width,$media_height, $use_thumb_content)."\n";

        }

    }
    else {

        $figure_output .= ct_get_post_media($post->ID, $media_width, $media_height, $video_height, $use_thumb_content);

    }

    $figure_output .= '</figure>'."\n";

    ?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>

    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-post coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">
                <div class="post-content clearfix">
                    <!--                    Start.Article -->
                    <section class="article-body-wrap single-wrap-aboutus">
                        <?php echo $figure_output;?>
                        <h3 class="single-title">
                            <?php the_title(); ?>
                        </h3>
                        <div class="body-text clearfix">
                            <?php the_content(); ?>
                        </div>
                    </section>

                    <!--                     Start.Social-->

                    <?php if ( comments_open() ) { ?>
                        <div id="comment-area">
                            <?php comments_template('', true); ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->

            <div class="left-sidebar col-xs-12 col-sm-3 col-sm-pull-9 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_aboutus'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-Left -->
        </div>
    </div>
<?php endif; ?>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>