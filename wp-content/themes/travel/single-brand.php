<?php get_header(); ?>

<?php

$post_author = get_the_author_link();
$post_date = get_the_date();
$post_categories = get_the_category(', ');
$post_categories_first = get_the_category();
$first_categories = $post_categories_first[0]->name;
$link_first_categories = get_category_link( $post_categories_first[0]->term_id );
$post_comments = get_comments_number();

$options = get_option('ct_coo_options');
$default_show_page_heading = $options['default_show_page_heading'];
$default_page_heading_bg_alt = $options['default_page_heading_bg_alt'];

$show_page_title = get_post_meta($post->ID, 'ct_page_title', true);
$show_auxo_list = get_post_meta($post->ID, 'ct_auxo_list', true);
$show_link_demo = get_post_meta($post->ID, 'ct_link_demo', true);
$page_title_style = get_post_meta($post->ID, 'ct_page_title_style', true);
$page_title = get_post_meta($post->ID, 'ct_page_title_one', true);
$page_subtitle = get_post_meta($post->ID, 'ct_page_subtitle', true);
$page_title_bg = get_post_meta($post->ID, 'ct_page_title_bg', true);
$fancy_title_image = rwmb_meta('ct_page_title_image', 'type=image&size=full');
$page_title_text_style = get_post_meta($post->ID, 'ct_page_title_text_style', true);
$fancy_title_image_url = "";

if ($show_page_title == "") {
    $show_page_title = $default_show_page_heading;
}
if ($page_title_bg == "") {
    $page_title_bg = $default_page_heading_bg_alt;
}
if ($page_title == "") {
    $page_title = get_the_title();
}

foreach ($fancy_title_image as $detail_image) {
    $fancy_title_image_url = $detail_image['url'];
    break;
}


if (!$fancy_title_image) {
    $fancy_title_image = get_post_thumbnail_id();
    $fancy_title_image_url = wp_get_attachment_url( $fancy_title_image, 'full' );
}

$full_width_display = get_post_meta($post->ID, 'ct_full_width_display', true);
$show_author_info = get_post_meta($post->ID, 'ct_author_info', true);
$show_social = get_post_meta($post->ID, 'ct_social_sharing', true);
$show_related =  get_post_meta($post->ID, 'ct_related_articles', true);
$remove_breadcrumbs = get_post_meta($post->ID, 'ct_no_breadcrumbs', true);

if ($show_author_info == "") {
    $show_author_info = true;
}
if ($show_social == "") {
    $show_social = true;
}

//config default sidebar on the post, page
$default_sidebar_config = $options['default_sidebar_config'];
$default_left_sidebar = $options['default_left_sidebar'];
$default_right_sidebar = $options['default_right_sidebar'];

//config sidebar meta post, page
$sidebar_config = get_post_meta($post->ID, 'ct_sidebar_config', true);
$left_sidebar = get_post_meta($post->ID, 'ct_left_sidebar', true);
$right_sidebar = get_post_meta($post->ID, 'ct_right_sidebar', true);

if ($sidebar_config == "0-m-0") {
    $sidebar_config = $default_sidebar_config;
}
if ($left_sidebar == "") {
    $left_sidebar = $default_left_sidebar;
}
if ($right_sidebar == "") {
    $right_sidebar = $default_right_sidebar;
}
$config =configLayout(of_get_option('single-layout',$sidebar_config));

$title_image_url="";
$title_image = rwmb_meta('ct_title_image', 'type=image&size=img-title');
if (is_array($title_image)) {
    foreach ($title_image as $image) {
        $title_image_url = $image['url'];
        break;
    }
}
if (have_posts()) : the_post();
//MEDIA TYPE
    $media_type = $media_image = $media_video = $media_gallery = '';

    $use_thumb_content = get_post_meta($post->ID, 'ct_thumbnail_content_main_detail', true);


    $post_format = get_post_format($post->ID);
    if ( $post_format == "" ) {
        $post_format = 'standard';
    }

    if ($use_thumb_content) {
        $media_type = get_post_meta($post->ID, 'ct_thumbnail_type', true);
    } else {
        $media_type = get_post_meta($post->ID, 'ct_detail_type', true);
    }

    if ((($sidebar_config == "1-m-0") || ($sidebar_config == "1-m-1") || ($sidebar_config == "0-m-1")) && !$full_width_display) {
        $media_width = 770;
        $media_height = NULL;
        $video_height = 433;
    } else {
        $media_width = 1170;
        $media_height = NULL;
        $video_height = 658;
    }
    $figure_output = '';

    if ($full_width_display) {
        $figure_output .= '<figure class="media-wrap-brand full-width-detail col-sm-12 " >';
    } else {
        $figure_output .= '<figure class="media-wrap-brand " >';
    }

    if ($post_format == "standard") {

        if ($media_type == "video") {

            $figure_output .= ct_video_post($post->ID, $media_width,$media_height, $use_thumb_content)."\n";

        } else if ($media_type == "slider") {

            $figure_output .= ct_gallery_post($post->ID, $use_thumb_content)."\n";

        } else if ($media_type == "layer-slider") {

            $figure_output .= '<div class="layerslider">'."\n";

            $figure_output .= do_shortcode('[rev_slider '.$media_slider.']')."\n";

            $figure_output .= '</div>'."\n";

        } else if ($media_type == "custom") {

            $figure_output .= $custom_media."\n";

        } else {

            $figure_output .= ct_image_post($post->ID, $media_width,$media_height, $use_thumb_content)."\n";

        }

    }
    else {

        $figure_output .= ct_get_post_media($post->ID, $media_width, $media_height, $video_height, $use_thumb_content);

    }

    $figure_output .= '</figure>'."\n";
    ?>

    <?php if($title_image_url){
        ?>
        <div class="row">
            <div class="title-image col-md-12">
                <?php echo $image = '<img itemprop="image" src="'.$title_image_url.'" alt="'.$image_title.'" />';?>
            </div>
        </div>
    <?php
    }?>
    <?php if ($show_page_title) { ?>
        <div class="row">
        <?php if ($page_title_style == "fancy") { ?>
            <?php if ($fancy_title_image_url != "") { ?>
                <div class="page-heading fancy-heading col-sm-12 clearfix alt-bg <?php echo $page_title_text_style; ?>-style fancy-image" style="background-image: url(<?php echo $fancy_title_image_url; ?>);" data-stellar-background-ratio="0.5">
            <?php } else { ?>
                <div class="page-heading fancy-heading col-sm-12 clearfix alt-bg <?php echo $page_title_bg; ?>">
            <?php } ?>
            <div class="heading-text">
                <h1><?php echo $page_title; ?></h1>
                <?php if ($page_subtitle) { ?>
                    <h3><?php echo $page_subtitle; ?></h3>
                <?php } ?>
            </div>
            </div>
        <?php } else { ?>
            <div class="page-heading col-sm-12">
                <?php
                // BREADCRUMBS
                if (!$remove_breadcrumbs) {
                    echo ct_breadcrumbs();
                }
                ?>
            </div>
        <?php } ?>
        </div>
    <?php } ?>

    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-post coo-main <?php echo $config['main']['class'];?> clearfix">
                <div class="post-content clearfix">
                    <?php echo $figure_output;?>
                    <!--                    Start.Article -->
                    <section class="article-body-wrap">
                        <h3 class="single-title">
                            <?php the_title(); ?>
                        </h3>
                        <div class="body-text clearfix">
                            <?php the_content(); ?>
                        </div>
                    </section>
                    <!--                    End.Article-->
                    <!--                    Start.Related-->
                    <div class="related-wrap">
                        <?php
                        $categories = get_the_category($post->ID);
                        if ($categories) {
                            $category_ids = array();
                            foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

                            $args=array(
                                'category__in' => $category_ids,
                                'post__not_in' => array($post->ID),
                                'showposts'=> 19, // Number of related posts that will be shown.
                                'orderby' => 'rand'
                            );
                        }
                        $related_posts_query = new wp_query($args);
                        $i=0;
                        if( $related_posts_query->have_posts() ) {
                            _e('<h3 class="title-related"><span>'.__("Related Articles", "cootheme").'</span></h3>');
                            echo '<ul class="related-items row clearfix">';
                            while ($related_posts_query->have_posts()) {
                                $related_posts_query->the_post();
                                $item_title = get_the_title();
                                $thumb_image = "";
                                $thumb_image = get_post_meta($post->ID, 'ct_thumbnail_image', true);
                                if (!$thumb_image) {
                                    $thumb_image = get_post_thumbnail_id();
                                }
                                $thumb_img_url = wp_get_attachment_url( $thumb_image, 'full' );
                                $image = aq_resize( $thumb_img_url, 300, 225, true, false);
                                if($i<3){
                                    ?>

                                    <li class="related-item col-md-4 clearfix">
                                        <figure class="animated-overlay overlay-alt">
                                            <?php if ($image) { ?>
                                                <a href="<?php the_permalink(); ?>">
                                                    <img itemprop="image" src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $item_title; ?>" />
                                                </a>
                                            <?php } else { ?>
                                                <div class="img-holder"><i class="ss-pen"></i></div>
                                            <?php } ?>

                                        </figure>
                                        <h5><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo $item_title; ?></a></h5>
                                    </li>
                                <?php }
                                else{
                                    ?>
                                    <li class="related-item-list col-md-6 clearfix">
                                        <h5><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo $item_title; ?></a></h5>
                                    </li>
                                <?php
                                }
                                ?>
                                <?php $i++; }
                            echo '</ul>';
                        }
                        wp_reset_query();
                        ?>
                    </div>
                    <!--                     End.Related-->
                    <!--                     Start.Social-->
                    <div class="signle-bottom clearfix">
                        <div class="pull-left LikeButton clearfix">
                            <!-- Facebook Button -->
                            <div class="FacebookButton">
                                <div id="fb-root"></div>
                                <script type="text/javascript">
                                    (function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) {
                                            return;
                                        }
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/en_US/all.js#appId=177111755694317&xfbml=1";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                </script>
                                <div class="fb-like" data-send="false" data-width="200" data-show-faces="true"
                                     data-layout="button_count" data-href="<?php the_permalink(); ?>"></div>
                            </div>
                            <!-- Twitter Button -->
                            <div class="TwitterButton">
                                <a href="<?php the_permalink(); ?>" class="twitter-share-button"
                                   data-count="horizontal" data-via="" data-size="small">
                                </a>
                            </div>
                            <!-- Google +1 Button -->
                            <div class="GooglePlusOneButton">
                                <!-- Place this tag where you want the +1 button to render -->
                                <div class="g-plusone" data-size="medium"
                                     data-href="<?php the_permalink(); ?>"></div>
                                <!-- Place this render call where appropriate -->
                                <script type="text/javascript">
                                    (function () {
                                        var po = document.createElement('script');
                                        po.type = 'text/javascript';
                                        po.async = true;
                                        po.src = 'https://apis.google.com/js/plusone.js';
                                        var s = document.getElementsByTagName('script')[0];
                                        s.parentNode.insertBefore(po, s);
                                    })();
                                </script>

                            </div>
                            <!-- Pinterest Button -->
                            <div class="PinterestButton">
                                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>&description=<?php the_title(); ?>"
                                   data-pin-do="buttonPin" data-pin-config="beside">
                                    <img class="pinterest"
                                         src="//assets.pinterest.com/images/pidgets/pin_it_button.png"/>
                                </a>
                                <script type="text/javascript">
                                    (function (d) {
                                        var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
                                        p.type = 'text/javascript';
                                        p.async = true;
                                        p.src = '//assets.pinterest.com/js/pinit.js';
                                        f.parentNode.insertBefore(p, f);
                                    }(document));
                                </script>
                            </div>
                            <!-- Linkedin Button -->
                            <div class="LinkedinButton">
                                <script type="IN/Share" data-url="<?php the_permalink(); ?>"
                                        data-counter="right"></script>
                            </div>
                        </div>

                    </div>

                    <?php if ( comments_open() ) { ?>
                        <div id="comment-area">
                            <?php comments_template('', true); ?>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->
            <?php if($config['left-sidebar']['show']){ ?>
                <div class="left-sidebar <?php echo $config['left-sidebar']['class']; ?>">
                    <div class="sidebar-inner">
                        <?php dynamic_sidebar($left_sidebar); ?>
                    </div>
                </div>
            <?php } ?>
            <!--End.Sidebar-Left -->

            <!--Start.Sidebar-Right-->
            <?php if($config['right-sidebar']['show']){ ?>
                <div class=" right-sidebar <?php echo $config['right-sidebar']['class']; ?>">
                    <div class="sidebar-inner">
                        <?php dynamic_sidebar($right_sidebar); ?>
                    </div>
                </div>
            <?php } ?>
            <!--End.Sidebar-right -->
        </div>
    </div>
<?php endif; ?>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>