<?php get_header(); ?>
<style type="text/css">
    .inner-page-wrap .archive-page .page-content .blog-items-wrap .about-special-item .special-img img {
        width: 130px;
        margin-left: 30%;
        margin-right: 30%;
        margin-top: 8%;
    }
</style>
<?php
$options = get_option('ct_coo_options');
global $ct_has_blog;
$ct_has_blog = true;
$image_title = ct_featured_img_title();
$image_category  = category_image_src( array('size' =>'img-title') , false );
$term_slug = get_query_var( 'term' );
$taxonomyName = get_query_var( 'taxonomy' );
$current_term = get_term_by( 'slug', $term_slug, $taxonomyName );
$term_id = $current_term->term_id;
$cat_data = get_option("tag_$term_id");
$seo_title=$cat_data['seo_met_title'];
?>
    <?php if($image_category){
        ?>
        <div class="row">
            <div class="title-image col-md-12">
                <img itemprop="image" src="<?php echo $image_category;?>" alt="img-culture" />
            </div>
        </div>
    <?php
    }?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>

    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-page  coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">

                <div class="page-content clearfix">
                    <div class="page-content-inner">
                        <h1 class="heading-text">
                            <?php  if($seo_title){
                                echo $seo_title;
                            }

                            else{
                                single_cat_title();
                            } ?>
                        </h1>
                        <div class="desception-category"><?php echo category_description(); ?></div>
                        <?php if(have_posts()) : ?>
                            <?php query_posts($query_string . '&orderby=date&order=ASC'); ?>
                            <div class="blog-wrap blog-items-wrap">
                                <!-- OPEN .blog-items -->
                                <ul class=" mini-items clearfix" id="blogGrid">

                                    <?php while (have_posts()) : the_post(); ?>

                                    <?php
                                    $post_format = get_post_format($post->ID);
                                    if ( $post_format == "" ) {
                                        $post_format = 'standard';
                                    }
                                    ?>
                                    <li class="about-special-item clearfix" id="security-<?php echo $post->ID; ?>">
                                        <div class="mini-special-wrap clearfix">
                                            <figure class="animated-overlay overlay-alt  special-img">
                                                <!-- <a class="special-info"> -->
                                                    <?php echo the_post_thumbnail('img-introduce');?>
                                                <!-- </a> -->
                                            </figure>
                                            <div class="special-details-wrap">
                                                <h3 itemprop="name headline" class="title">
                                                    <a><?php echo the_title();?></a>
                                                </h3>
                                                <div itemprop="description" class="excerpt">

                                                    <?php
                                                          echo the_content();
                                                    ?>

                                                </div>

                                            </div>
                                        </div>

                                        <?php endwhile; ?>

                                        <!-- CLOSE .blog-items -->
                                </ul>

                            </div>

                        <?php else: ?>

                        <?php endif; ?>
                        <!--                     Start.Social-->
                        <div class="signle-bottom clearfix">
                            <div class="pull-left LikeButton clearfix">
                                <!-- Facebook Button -->
                                <div class="FacebookButton">
                                    <div id="fb-root"></div>
                                    <script type="text/javascript">
                                        (function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) {
                                                return;
                                            }
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "//connect.facebook.net/en_US/all.js#appId=177111755694317&xfbml=1";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                    <div class="fb-like" data-send="false" data-width="200" data-show-faces="true"
                                         data-layout="button_count" data-href="<?php the_permalink(); ?>"></div>
                                </div>
                                <!-- Twitter Button -->
                                <div class="TwitterButton">
                                    <a href="<?php the_permalink(); ?>" class="twitter-share-button"
                                       data-count="horizontal" data-via="" data-size="small">
                                    </a>
                                </div>
                                <!-- Google +1 Button -->
                                <div class="GooglePlusOneButton">
                                    <!-- Place this tag where you want the +1 button to render -->
                                    <div class="g-plusone" data-size="medium"
                                         data-href="<?php the_permalink(); ?>"></div>
                                    <!-- Place this render call where appropriate -->
                                    <script type="text/javascript">
                                        (function () {
                                            var po = document.createElement('script');
                                            po.type = 'text/javascript';
                                            po.async = true;
                                            po.src = 'https://apis.google.com/js/plusone.js';
                                            var s = document.getElementsByTagName('script')[0];
                                            s.parentNode.insertBefore(po, s);
                                        })();
                                    </script>

                                </div>
                                <!-- Pinterest Button -->
                                <div class="PinterestButton">
                                    <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>&description=<?php the_title(); ?>"
                                       data-pin-do="buttonPin" data-pin-config="beside">
                                        <img class="pinterest"
                                             src="//assets.pinterest.com/images/pidgets/pin_it_button.png"/>
                                    </a>
                                    <script type="text/javascript">
                                        (function (d) {
                                            var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
                                            p.type = 'text/javascript';
                                            p.async = true;
                                            p.src = '//assets.pinterest.com/js/pinit.js';
                                            f.parentNode.insertBefore(p, f);
                                        }(document));
                                    </script>
                                </div>
                                <!-- Linkedin Button -->
                                <div class="LinkedinButton">
                                    <script type="IN/Share" data-url="<?php the_permalink(); ?>"
                                            data-counter="right"></script>
                                </div>
                            </div>

                        </div>

                        <div class="pagination-wrap">
                            <?php echo pagenavi($wp_query); ?>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->

            <div class="left-sidebar col-xs-12 col-sm-3 col-sm-pull-9 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_aboutus'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-Left -->
        </div>
    </div>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>