<?php get_header(); ?>

<?php

$options = get_option('ct_coo_options');
$sidebar_config = $options['archive_sidebar_config'];
$config =configLayout(of_get_option('single-layout',$sidebar_config));
$left_sidebar = $options['archive_sidebar_left'];
$right_sidebar = $options['archive_sidebar_right'];

?>

<div class="row">
	<div class="page-heading col-sm-12 clearfix alt-bg <?php echo $default_page_heading_bg_alt; ?>">
		<!--
		<div class="heading-text">
		<?php $allsearch = new WP_Query("s=$s&showposts=-1"); $key = esc_html($s, 1); $count = $allsearch->post_count; _e('', "cootheme"); wp_reset_query(); ?>
		<?php if ($count == 1) : ?>
			<?php printf(__('<h1>%1$s result for <span>%2$s</span></h1>', 'cootheme'), $count, $key ); ?>
		<?php else : ?>
			<?php printf(__('<h1>%1$s results for <span>%2$s</span></h1>', 'cootheme'), $count, $key ); ?>
		<?php endif; ?>
		</div> -->
		<?php 
			// BREADCRUMBS
			echo ct_breadcrumbs();
		?>
	</div>
</div>

<div class="inner-page-wrap <?php echo $page_wrap_class; ?> clearfix">

	<!-- OPEN page -->
	<div class="archive-page clearfix">
		<div class="page-content clearfix">
            <div class="page-content-inner">
                <?php if(have_posts()) : ?>

                    <div class="blog-wrap">

                        <!-- OPEN .blog-items -->
                        <ul class="blog-items row search-items clearfix">

                        <?php while (have_posts()) : the_post(); ?>

                            <?php
                                $post_format = get_post_format($post->ID);
                                if ( $post_format == "" ) {
                                    $post_format = 'standard';
                                }
                            ?>
                            <li <?php post_class('blog-item col-sm-12 format-'.$post_format); ?>>
                                <?php echo ct_get_search_item($post->ID); ?>
                            </li>

                        <?php endwhile; ?>

                        <!-- CLOSE .blog-items -->
                        </ul>

                    </div>

                <?php else: ?>

                <h3><?php _e("Sorry, there are no posts to display.", "cootheme"); ?></h3>

                <div class="no-results-text">
                    <p><?php _e("Please use the form below to search again.", "cootheme"); ?></p>
                    <form method="get" class="search-form" action="<?php echo home_url(); ?>/">
                        <input type="text" placeholder="<?php _e("Search", "cootheme"); ?>" name="s" />
                    </form>
                    <p><?php _e("Alternatively, you can browse the sitemap below.", "cootheme"); ?></p>
                    <?php echo do_shortcode('[ct_sitemap]'); ?>
                </div>

                <?php endif; ?>

                <div class="pagination-wrap">
                    <?php echo pagenavi($wp_query); ?>
                </div>
			</div>
		</div>
	</div>
    <!--End.page    -->
<!--Start.Sidebar-Left-->
<?php if($config['left-sidebar']['show']){ ?>
    <div class="left-sidebar <?php echo $config['left-sidebar']['class']; ?>">
        <div class="sidebar-inner">
            <?php dynamic_sidebar($left_sidebar); ?>
        </div>
    </div>
<?php } ?>
    <!--End.Sidebar-Left -->

    <!--Start.Sidebar-Right-->
<?php if($config['right-sidebar']['show']){ ?>
    <div class=" right-sidebar <?php echo $config['right-sidebar']['class']; ?>">
        <div class="sidebar-inner">
            <?php dynamic_sidebar($right_sidebar); ?>
        </div>
    </div>
<?php } ?>
    <!--End.Sidebar-right -->
		
</div>

<!--// WordPress Hook //-->
<?php get_footer(); ?>