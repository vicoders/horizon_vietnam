<?php

class CooPageBuilderShortcode_brand extends CooPageBuilderShortcode {

    protected function content($atts, $content = null) {

        $title = $width = $el_class = $output = $filter = $social_icon_type = $items = $el_position = '';

        extract(shortcode_atts(array(
            'item_columns' => '4',
            "item_count"	=> '12',
            "category"		=> '',
            'el_position' => '',
            'width' => '1/1',
            'el_class' => ''
        ), $atts));

        // CATEGORY SLUG MODIFICATION
        if ($category == "All") {$category = "all";}
        if ($category == "all") {$category = '';}
        $category_slug = str_replace('_', '-', $category);

        global $post, $wp_query;

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $brand_args=array(
            'post_type' => 'brand',
            'post_status' => 'publish',
            'paged' => $paged,
            'brand-category' => $category_slug,
            'posts_per_page' => $item_count,
            'ignore_sticky_posts'=> 1
        );
        $brand_members = new WP_Query( $brand_args );
        $count = 0;
        $items .= '<ul class="brand-members row owl-carousel clearfix" data-show="'.$item_columns.'">';

        while ( $brand_members->have_posts() ) : $brand_members->the_post();

            $member_name = get_the_title();

            $member_image = get_post_thumbnail_id();
            $member_link = get_permalink();

            $items .= '<li itemscope data-id="id-'. $count .'" class="clearfix  brand-member-wap '.$item_class.'">';

            $items .= '<div class="brand-member">';

            $img_url = wp_get_attachment_url( $member_image,'full' );

            $image = aq_resize( $img_url, 135, 135, true, false);

            $items .= '<figure class="gallery-style">';

            $items .= '<a href="'.get_permalink().'"><img itemprop="image" src="'.$image[0].'" width="'.$image[1].'" height="'.$image[2].'" /></a>';

            $items .= '<figcaption>';

            $items .= '<span class="brand-member-position">'. $member_position .'</span>';

            $items .= '<div class="short-des">'.$member_bio.'</div>';

            $items .= '</figcaption>';

            $items .= '</figure>';

            $items .= '</div>';
            $items .= '</li>';
            $count++;

        endwhile;

        wp_reset_postdata();

        $items .= '</ul>';


        $el_class = $this->getExtraClass($el_class);
        $width = spb_translateColumnWidthToSpan($width);

        $output .= "\n\t".'<div class="brand_list_widget spb_content_element '.$width.$el_class.'">';
        $output .= "\n\t\t".'<div class="spb_wrapper">';
        $output .= "\n\t\t\t\t".$items;
        $output .= "\n\t\t".'</div> '.$this->endBlockComment('.spb_wrapper');

        $output .= "\n\t".'</div> '.$this->endBlockComment($width);

        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);

        global $ct_include_isotope, $ct_has_brand;
        $ct_include_isotope = true;
        $ct_has_brand = true;

        return $output;

    }
}

SPBMap::map( 'brand', array(
    "name"		=> __("Brand Gallery", "coo-page-builder"),
    "base"		=> "brand",
    "class"		=> "brand",
    "icon"      => "spb-icon-brand",
    "params"	=> array(
        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Number of items", "coo-page-builder"),
            "param_name" => "item_columns",
            "value" => "4",
            "description" => __("The number of brand members to show per page.", "coo-page-builder")
        ),
        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Number of items", "coo-page-builder"),
            "param_name" => "item_count",
            "value" => "12",
            "description" => __("The number of brand members to show per page.", "coo-page-builder")
        ),
        array(
            "type" => "select-multiple",
            "heading" => __("brand category", "coo-page-builder"),
            "param_name" => "category",
            "value" => ct_get_category_list('brand-category'),
            "description" => __("Choose the category for the portfolio items.", "coo-page-builder")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "coo-page-builder"),
            "param_name" => "el_class",
            "value" => "",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "coo-page-builder")
        )
    )
) );

?>