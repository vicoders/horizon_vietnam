<?php

class CooPageBuilderShortcode_testimonial_carousel extends CooPageBuilderShortcode {

    public function content( $atts, $content = null ) {

        global $ct_carousel_id;

        if ($ct_carousel_id == "") {
            $ct_carousel_id = 1;
        } else {
            $ct_carousel_id++;
        }

        $title = $order = $page_link = $items = $item_class = $el_class = $width = $el_position = '';

        extract(shortcode_atts(array(
        	'title' => '',
           	'item_count'	=> '-1',
           	'order'	=> '',
        	'category'		=> 'all',
        	'pagination'	=> 'no',
        	'page_link'	=> '',
            'el_class' => '',
            'el_position' => '',
            'width' => '1/2'
        ), $atts));

        $output = '';

        // CATEGORY SLUG MODIFICATION
        if ($category == "All") {$category = "all";}
        if ($category == "all") {$category = '';}
        $category_slug = str_replace('_', '-', $category);


        // TESTIMONIAL QUERY SETUP

		global $post, $wp_query, $ct_carouselID;

		if ($ct_carouselID == "") {
		$ct_carouselID = 1;
		} else {
		$ct_carouselID++;
		}

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        $testimonials_args = array(
        	'orderby' => $order,
        	'post_type' => 'testimonials',
        	'post_status' => 'publish',
        	'paged' => $paged,
        	'testimonials-category' => $category_slug,
        	'posts_per_page' => $item_count,
        	'no_found_rows' => 1,
        	);

        $testimonials = new WP_Query( $testimonials_args );

        $sidebar_config = get_post_meta(get_the_ID(), 'ct_sidebar_config', true);

        $sidebars = '';
        if (($sidebar_config == "left-sidebar") || ($sidebar_config == "right-sidebar")) {
        $sidebars = 'one-sidebar';
        } else if ($sidebar_config == "both-sidebars") {
        $sidebars = 'both-sidebars';
        } else {
        $sidebars = 'no-sidebars';
        }

        if ($width == "1/1") {
        	if ($sidebars == "both-sidebars") {
        	$item_class = "span6";
        	} else if ($sidebars == "one-sidebar") {
        	$item_class = "span8";
        	} else {
        	$item_class = "span12";
        	}
        } else if ($width == "1/2") {
        	if ($sidebars == "both-sidebars") {
        	$item_class = "span3";
        	} else if ($sidebars == "one-sidebar") {
        	$item_class = "span4";
        	} else {
        	$item_class = "span6";
        	}
        } else if ($width == "3/4") {
        	if ($sidebars == "both-sidebars") {
        	$item_class = "span-bs-threequarter";
        	} else if ($sidebars == "one-sidebar") {
        	$item_class = "span6";
        	} else {
        	$item_class = "span9";
        	}
        } else if ($width == "1/4") {
        	if ($sidebars == "both-sidebars") {
        	$item_class = "span-bs-quarter";
        	} else if ($sidebars == "one-sidebar") {
        	$item_class = "span2";
        	} else {
        	$item_class = "span3";
        	}
        }

        $items .= '<div id="zo2-quote-carousel-'.$ct_carousel_id.'" class="carousel slide" data-ride="carousel">';
        $items .= '<ol class="carousel-indicators">';
        $data_slide_to = 0;
        while ( $testimonials->have_posts() ) : $testimonials->the_post();
            $active = '';
            if($data_slide_to == 0) {
                $active = 'active';
            }
            $items .= '<li class="'.$active.'" data-slide-to="'.$data_slide_to.'" data-target="#zo2-quote-carousel-'.$ct_carousel_id.'"></li>';
            $data_slide_to++;
        endwhile;
        $items .= '</ol>';
        $items .= '<div class="carousel-inner">';

        // TESTIMONIAL LOOP
        $count = 0;
        while ( $testimonials->have_posts() ) : $testimonials->the_post();
        	$count++;
            $class = "";
            if ($count == 1 ) {
                $class = "active";
            }
        	$testimonial_text = get_the_content();
        	$testimonial_cite = get_post_meta($post->ID, 'ct_testimonial_cite', true);
        	$testimonial_cite_subtext = get_post_meta($post->ID, 'ct_testimonial_cite_subtext', true);
        	$testimonial_image = rwmb_meta('ct_testimonial_cite_image', 'type=image', $post->ID);

        	foreach ($testimonial_image as $detail_image) {
        		$testimonial_image_url = $detail_image['url'];
        		break;
        	}

        	if (!$testimonial_image) {
        		$testimonial_image = get_post_thumbnail_id();
        		$testimonial_image_url = wp_get_attachment_url( $testimonial_image, 'full' );
        	}

        	$testimonial_image = aq_resize( $testimonial_image_url, 70, 70, true, false);

        	$items .= '<div class="item '.$class.'">';
            $items .= '<blockquote>';
            $items .= '<div class="row reading">';
        	$items .= '<p>'.do_shortcode($testimonial_text).'</p>';
        	$items .= '</div>';
            $items .= '<div class="row">';
        	if ($testimonial_image) {
            $items .= '<div class="col-sm-4 text-right">';
        	$items .= '<img class="img-circle" src="'.$testimonial_image[0].'" width="'.$testimonial_image[1].'" height="'.$testimonial_image[2].'" alt="'.$testimonial_cite.'" />';
        	$items .= '</div>';
            $items .= '<div class="col-sm-8"><small class="name">'.$testimonial_cite.'</small><small class="position">'.$testimonial_cite_subtext.'</small></div>';
        	} else {
        	$items .= '<div class="col-sm-8"><small class="name">'.$testimonial_cite.'</small><small class="position">'.$testimonial_cite_subtext.'</small></div>';
        	}
        	$items .= '</div>';
        	$items .= '</blockquote>';
            $items .= '</div>';

        endwhile;

        wp_reset_postdata();

        $items .= '</div>';

       	$items .= '</div>';

       	$options = get_option('ct_coo_options');
       	if ($options['enable_swipe_indicators']) {
       	$items .= '<div class="ct-swipe-indicator"></div>';
       	}

       	$items .= '</div>';

   		if ($page_link == "yes") {
	        $options = get_option('ct_coo_options');
	        $testimonials_page = __($options['testimonial_page'], 'cootheme');

			if ($testimonials_page) {
				$items .= '<a href="'.get_permalink($testimonials_page).'" class="read-more">'.__("More", "cootheme").'<i class="ssnavigate-right"></i></a>';
			}
		}

        $width = spb_translateColumnWidthToSpan($width);
        $el_class = $this->getExtraClass($el_class);

        $el_class .= ' testimonial';

		$output .= "\n\t".'<div class="spb_testimonial_carousel_widget spb_content_element '.$width.$el_class.'">';
        $output .= "\n\t\t".'<div class="spb_wrapper carousel-wrap">';
        if ($title != '') {
        $output .= "\n\t\t\t".'<h3 class="spb-heading"><span>'.$title.'</span></h3>';
        }
        $output .= "\n\t\t\t\t".$items;
        $output .= "\n\t\t".'</div> '.$this->endBlockComment('.spb_wrapper');
        $output .= "\n\t".'</div> '.$this->endBlockComment($width);

        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);

        global $ct_include_carousel, $ct_include_isotope;
        $ct_include_carousel = true;
        $ct_include_isotope = true;

        return $output;
    }
}

SPBMap::map( 'testimonial_carousel', array(
    "name"		=> __("Testimonials Carousel", "coo-page-builder"),
    "base"		=> "testimonial_carousel",
    "class"		=> "spb_testimonial_carousel spb_carousel",
    "icon"      => "spb-icon-testimonial_carousel",
    "wrapper_class" => "clearfix",
    "params"	=> array(
    	array(
    	    "type" => "textfield",
    	    "heading" => __("Widget title", "coo-page-builder"),
    	    "param_name" => "title",
    	    "value" => "",
    	    "description" => __("Heading text. Leave it empty if not needed.", "coo-page-builder")
    	),
        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Number of items", "coo-page-builder"),
            "param_name" => "item_count",
            "value" => "6",
            "description" => __("The number of testimonials to show per page. Leave blank to show ALL testimonials.", "coo-page-builder")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Testimonials Order", "coo-page-builder"),
            "param_name" => "order",
            "value" => array(__('Random', "coo-page-builder") => "rand", __('Latest', "coo-page-builder") => "date"),
            "description" => __("Choose the order of the testimonials.", "coo-page-builder")
        ),
        array(
            "type" => "select-multiple",
            "heading" => __("Testimonials category", "coo-page-builder"),
            "param_name" => "category",
            "value" => ct_get_category_list('testimonials-category'),
            "description" => __("Choose the category for the testimonials.", "coo-page-builder")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Testimonials page link", "coo-page-builder"),
            "param_name" => "page_link",
            "value" => array(__('No', "coo-page-builder") => "no", __('Yes', "coo-page-builder") => "yes"),
            "description" => __("Include a link to the testimonials page (which you must choose in the theme options).", "coo-page-builder")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "coo-page-builder"),
            "param_name" => "el_class",
            "value" => "",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "coo-page-builder")
        )
    )
) );

?>