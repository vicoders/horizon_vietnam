<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/7/15
 * Time: 2:25 PM
 */
class CooPageBuilderShortcode_category extends CooPageBuilderShortcode {

    protected function content($atts, $content = null) {

        $options = get_option('ct_coo_options');

        $title = $width = $excerpt_length = $item_class = $offset = $el_class = $output = $items = $el_position = $item_count='';

        extract(shortcode_atts(array(
            "category"		=> '',
            'el_position' => '',
            'width' => '1/1',
            'el_class' => ''
        ), $atts));
        $width = spb_translateColumnWidthToSpan($width);
        $el_class = $this->getExtraClass($el_class);
        $output = '';
        $output .= "\n\t".'<div class="category_widget spb_content_element '.$width.'">';
        $output .= '<ul class="row '.$el_class.' clearfix">';
        // CATEGORY SLUG MODIFICATION
        if ($category == "All" || $category == "all") {
            $categories = "all" ;
            $categories = get_terms('destination-category');
            foreach ($categories as $cats)
            {
                $term_id = $cats->term_id;
                $title = $cats->name;

                $image  = category_image_src( array('term_id'=>$term_id,'size' =>'img-category') , false );
                $output .='<li class="item-category col-md-6 col-sm-6  clearfix">';
                $output .= '<div class="item-category-inner clearfix">';
                $output .='<h3><a class="title-category" href="'.get_category_link( $term_id ).'">'.$title.'</a></h3>';
                $output .='<img src="'.$image.'" alt="'.$cats->name.'" class="img-responsive">';
                $output .='</div>';
                $output .='</li>';

            }
        }
        else{
            $category_slug = str_replace('_', '-', $category);


            global $categories,$image;

            $categories = explode(",", $category_slug);

            foreach ($categories as $category)
            {
                $idObj = get_term_by('slug', $category, 'destination-category');
                $term_id = $idObj->term_id;
                $title = $idObj->name;

                $image   = category_image_src( array('term_id'=>$term_id,'size' =>'img-category') , false );
                $output .= '<li class="item-category col-md-6 col-sm-6  clearfix">';
                $output .= '<div class="item-category-inner clearfix">';
                $output .='<h3><a class="title-category" href="'.get_category_link( $idObj ).'">'.$title.'</a></h3>';
                $output .='<img src="'.$image.'" alt="'.$idObj->name.'" class="img-responsive">';
                $output .='</div>';
                $output .='</li>';

            }
        }
        $output .= '</ul>';
        $output .= "\n\t".'</div> '.$this->endBlockComment($width);
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
SPBMap::map('category', array(
    "name"		=> __("Destination", "coo-page-builder"),
    "base"		=> "category",
    "class"		=> "",
    "icon"      => "spb-icon-posts-carousel",
    "params"	=> array(
        array(
            "type" => "select-multiple",
            "heading" => __("Destination category", "coo-page-builder"),
            "param_name" => "category",
            "value" => ct_get_category_destination('destination-category'),
            "description" => __("Choose the category for the blog items.", "coo-page-builder")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "coo-page-builder"),
            "param_name" => "el_class",
            "value" => "",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "coo-page-builder")
        )

    )
) );

?>