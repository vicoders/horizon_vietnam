<?php

class CooPageBuilderShortcode_spb_head_block extends CooPageBuilderShortcode {

    public function content( $atts, $content = null ) {

        $title = $pb_margin_bottom = $pb_border_bottom = $el_class = $width = $el_position = '';

        extract(shortcode_atts(array(
            'title' => '',
            'icon' => '',
            'pb_margin_bottom' => 'no',
            'pb_border_bottom' => 'no',
            'el_class' => '',
            'el_position' => '',
            'width' => '1/2'
        ), $atts));

        $output = '';

        $el_class = $this->getExtraClass($el_class);
        $width = spb_translateColumnWidthToSpan($width);

        $el_class .= ' spb_head_column';

        if ($pb_margin_bottom == "yes") {
            $el_class .= ' pb-margin-bottom';
        }
        if ($pb_border_bottom == "yes") {
            $el_class .= ' pb-border-bottom';
        }

        $icon_output = "";

        if ($icon) {
            $icon_output = '<i class="'.$icon.'"></i>';
        }

        $output .= "\n\t".'<div class="spb_content_element '.$width.$el_class.'">';
        $output .= "\n\t\t".'<div class="spb_wrapper clearfix">';
        $output .= ($title != '' ) ? "\n\t\t\t".'<h1 class="spb-heading title-site spb-head-heading"><span>'.$title.'</span></h1>' : '';
        $output .= "\n\t\t\t".do_shortcode($content);
        $output .= "\n\t\t".'</div> ' . $this->endBlockComment('.spb_wrapper');
        $output .= "\n\t".'</div> ' . $this->endBlockComment($width);

        //
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}

SPBMap::map( 'spb_head_block', array(
    "name"		=> __("Head Block", "coo-page-builder"),
    "base"		=> "spb_head_block",
    "class"		=> "",
    "icon"      => "spb-icon-text-block",
    "wrapper_class" => "clearfix",
    "controls"	=> "full",
    "params"	=> array(
        array(
            "type" => "textfield",
            "heading" => __("Widget title", "coo-page-builder"),
            "param_name" => "title",
            "value" => "",
            "description" => __("Heading text. Leave it empty if not needed.", "coo-page-builder")
        ),
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Text", "coo-page-builder"),
            "param_name" => "content",
            "value" => __("<p>This is a text block. Click the edit button to change this text.</p>", "coo-page-builder"),
            "description" => __("Enter your content.", "coo-page-builder")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Margin below widget", "coo-page-builder"),
            "param_name" => "pb_margin_bottom",
            "value" => array(__('No', "coo-page-builder") => "no", __('Yes', "coo-page-builder") => "yes"),
            "description" => __("Add a bottom margin to the widget.", "coo-page-builder")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Border below widget", "coo-page-builder"),
            "param_name" => "pb_border_bottom",
            "value" => array(__('No', "coo-page-builder") => "no", __('Yes', "coo-page-builder") => "yes"),
            "description" => __("Add a bottom border to the widget.", "coo-page-builder")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "coo-page-builder"),
            "param_name" => "el_class",
            "value" => "",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "coo-page-builder")
        )
    )
) );
?>