<?php
/*
*
*	Coo Page Builder - Imapact Text Shortcode
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

class CooPageBuilderShortcode_spb_text_introduce extends CooPageBuilderShortcode {

    public  function content( $atts, $content = null ) {
        $img = $image = $color = $type = $target = $href = $border_top = $include_button = $button = $border_bottom = $title = $width = $position = $el_class = '';
        extract(shortcode_atts(array(
            'width' => '1/1',
            'image' =>$image,
            'button'=>$button,
            'href'  =>$href,
            'color' =>$color,
            'title' =>$title,
            'position' => 'cta_align_right',
            'el_position' => '',
            'el_class' => ''
        ), $atts));

        $width = spb_translateColumnWidthToSpan($width);
        $button = '<a class="ct-button '. $color .'" href="'.$href.'"><i class="fa fa-forward"></i><span>' . $title . '</span></a>';
        $img = spb_getImageBySize(array( 'attach_id' => preg_replace('/[^\d]/', '', $image), 'thumb_size' =>"img-introduce"));
        $output = '';

        $output .= "\n\t".'<div class="spb_text_introduce_widget spb_content_element '.$width.$el_class.'">';

        if ($position == "cta_align_left") {
            $output .= '<div class="content align_left">';
            $output .= '<div class="spb_call_text">'. spb_format_content($content) . '</div>'. "\n";
            $output .= $button. "\n";
            $output .='</div>'."\n";
        } else {
            $output .= '<div class="content align_right">';
            $output .= '<div class="spb_call_text">'. spb_format_content($content) . '</div>'. "\n";
            $output .= $button. "\n";
            $output .='</div>'."\n";
        }
        $output .= $img['thumbnail'];
        $output .= "\n\t".'</div> '.$this->endBlockComment($width);
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
$colors_arr = array(__("Accent", "coo-page-builder") => "accent", __("Blue", "coo-page-builder") => "blue", __("Grey", "coo-page-builder") => "grey", __("Light grey", "coo-page-builder") => "lightgrey", __("Purple", "coo-page-builder") => "purple", __("Light Blue", "coo-page-builder") => "lightblue", __("Green", "coo-page-builder") => "green", __("Lime Green", "coo-page-builder") => "limegreen", __("Turquoise", "coo-page-builder") => "turquoise", __("Pink", "coo-page-builder") => "pink", __("Orange", "coo-page-builder") => "orange");

SPBMap::map( 'spb_text_introduce', array(
    "name"		=> __("Text Introduce", "coo-page-builder"),
    "base"		=> "spb_text_introduce",
    "class"		=> "spb_text_introduce_widget",
    "icon"		=> "spb-icon-gallery",
    "params"	=> array(
        array(
            "type" => "attach_image",
            "heading" => __("Image", "coo-page-builder"),
            "param_name" => "image",
            "value" => "",
            "description" => ""
        ),
        array(
            "type" => "textfield",
            "heading" => __("Text on the button", "coo-page-builder"),
            "param_name" => "title",
            "value" => __("Text on the button", "coo-page-builder"),
            "description" => __("Text on the button.", "coo-page-builder")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Add link to button", "coo-page-builder"),
            "param_name" => "href",
            "value" => "#",
            "description" => __("If you would like the button to link to a URL, then enter it here.", "coo-page-builder")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Color", "coo-page-builder"),
            "param_name" => "color",
            "value" => $colors_arr,
            "description" => __("Button color.", "coo-page-builder")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Description position", "coo-page-builder"),
            "param_name" => "position",
            "value" => array(__("Align right", "coo-page-builder") => "cta_align_right", __("Align left", "coo-page-builder") => "cta_align_left"),
            "description" => __("Select description alignment.", "coo-page-builder")
        ),
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "text_introduce",
            "heading" => __("Text", "coo-page-builder"),
            "param_name" => "content",
            "value" => __("click the edit button to change this text.", "coo-page-builder"),
            "description" => __("Enter your content.", "coo-page-builder")
        ),

    )
) );
?>