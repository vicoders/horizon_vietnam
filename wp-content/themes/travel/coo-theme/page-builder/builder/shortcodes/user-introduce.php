<?php
/*
*
*	Coo Page Builder - Imapact Text Shortcode
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

class CooPageBuilderShortcode_spb_user_introduce extends CooPageBuilderShortcode {

    public  function content( $atts, $content = null ) {
        $image = $title = $width = $position = $el_class = $name = '';
        extract(shortcode_atts(array(
            'width' => '1/1',
            'image' =>$image,
            'name' =>$name,
            'el_position' => '',
            'el_class' => ''
        ), $atts));

        $width = spb_translateColumnWidthToSpan($width);
        $img = spb_getImageBySize(array( 'attach_id' => preg_replace('/[^\d]/', '', $image), 'thumb_size' =>"img-user"));
        $output = '';
        $output .= "\n\t".'<div class="spb_user_introduce_widget spb_content_element'.$width.$el_class.'">';
        $output .= '<div class="content">';
        $output .= $img['thumbnail'];
        $output .= '<span class="user-name">'. $name . '</span>'. "\n";
        $output .='</div>'."\n";
        $output .= "\n\t".'</div> '.$this->endBlockComment($width);
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}


SPBMap::map( 'spb_user_introduce', array(
    "name"		=> __("User Introduce", "coo-page-builder"),
    "base"		=> "spb_user_introduce",
    "class"		=> "spb_user_introduce_widget",
    "icon"		=> "spb-icon-clients",
    "params"	=> array(
        array(
            "type" => "attach_image",
            "heading" => __("Image", "coo-page-builder"),
            "param_name" => "image",
            "value" => "",
            "description" => ""
        ),
        array(
            "type" => "textfield",
            "holder" => "h3",
            "heading" => __("Name :", "coo-page-builder"),
            "param_name" => "name",
            "value" => __("Nguyen van A", "coo-page-builder"),
            "description" => __("Name user.", "coo-page-builder")
        )
    )
) );
?>