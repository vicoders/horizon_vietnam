<?php

	/*
	*
	*	Custom Posts Widget
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/
	
	// Register widget
	add_action( 'widgets_init', 'init_ct_booking_tour' );
	function init_ct_booking_tour() { return register_widget('ct_booking_tour'); }

	class ct_booking_tour extends WP_Widget {
		function ct_booking_tour() {
			parent::__construct( 'ct_booking_tour', $name = 'Cootheme Booking Tour' );
		}
	
		function widget( $args, $instance ) {

			global $post;
			extract($args);
            $cat_id="";
            if(is_archive()){
                $category = get_category(get_query_var('cat'));
                $cat_id = $category->cat_ID;
            }

			// Widget Options
			$title 	 = apply_filters('widget_title', $instance['title'] ); // Title		
			$number	 = $instance['number']; // Number of posts to show
			$post_categories	 = $instance['categories'];

			echo $before_widget;
			
		    if ( $post_categories ) echo $before_title .get_cat_name( $post_categories ) . $after_title;

            $args = array(
                'orderby'           => 'id',
                'order'             => 'ASC',
                'hide_empty'        => false,
                'parent'            => $post_categories
            );
            $categories = get_categories($args);
            if(count($categories) > 0)
            {?>
                <ul class="category-posts-list">
                    <?php  foreach($categories as $cate) {
                           $cat_ID=$cate->term_id;
                                $current='';
                            if($cat_ID == $cat_id){
                                $current='active';
                            }
                    ?>
                    <li class="cat-item <?php echo $current; ?>">
                        <a class="category-post-title" href="<?php echo get_category_link($cate); ?>" title="<?php echo ''; ?>"><?php echo $cate->name; ?></a>
                        <ul class="wg-post-items">
                            <?php $args = array(
                                'posts_per_page'   => 5,
                                'category_name'    => $cate->name,
                            );
                            $posts_array = get_posts( $args );
                                foreach ( $posts_array as $post ) : setup_postdata( $post ); ?>
                                    <li>
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </li>
                                <?php endforeach;
                                wp_reset_postdata();

                            ?>
                        </ul>
                    </li>

                    <?php } wp_reset_query();?>
                </ul>

                <?php echo $after_widget;
            }
		    }

        /* Widget control update */
        function update( $new_instance, $old_instance ) {
            $instance    = $old_instance;

            $instance['title']  = strip_tags( $new_instance['title'] );
            $instance['number'] = strip_tags( $new_instance['number'] );
            $instance['categories'] = strip_tags( $new_instance['categories'] );
            return $instance;
            }

		/* Widget settings */
		function form( $instance ) {	
		
			    // Set defaults if instance doesn't already exist
			    if ( $instance ) {
					$title  = $instance['title'];
			        $number = $instance['number'];
                    $categories=$instance['categories'];
			    } else {
				    // Defaults
					$title  = '';
			        $number = '5';
                    $categories='';
			    }
				
				// The widget form
				?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of posts to show:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
				</p>
                <p>
                    <label for="<?php echo $this->get_field_id('categories'); ?>"><?php echo __( 'Choose Category:', 'coo-theme-admin'); ?></label>
                    <select  name="<?php echo $this->get_field_name('categories'); ?>">
                    <?php
                    $args = array(
                        'orderby'           => 'id',
                        'order'             => 'ASC',
                        'parent'            =>0,
                        'hide_empty'        => false,
                    );
                    $categories_option = get_categories($args);
                    foreach ($categories_option as $category) {
                        $option = '<option value="'.$category->term_id.'"'.selected( $categories, $category->term_id, false ).'>';
                        $option .= $category->name;
                        $option .= '</option>';
                        echo $option;
                    }?>
                    </select>
                </p>
		<?php 
		}
	
	}

?>