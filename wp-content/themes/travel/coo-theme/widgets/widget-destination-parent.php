<?php

/*
*
*	Custom Posts Widget
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

// Register widget
add_action( 'widgets_init', 'ct_destination_parent' );
function ct_destination_parent() { return register_widget('ct_destination_parent'); }

class ct_destination_parent extends WP_Widget {
    function ct_destination_parent() {
        parent::__construct( 'ct_destination_parent', $name = 'Cootheme  Destination Parent' );
    }

    function widget( $args, $instance ) {

        global $post;
        extract($args);

        // Widget Options
        $title 	 = apply_filters('widget_title', $instance['title'] ); // Title
        $number	 = $instance['number']; // Number of posts to show

        echo $before_widget;

        if ( $title ) echo $before_title . $title . $after_title;?>

        <ul class="category-posts-list" data-number="<?php echo $number; ?>">
        <?php
        $args = array(
            'orderby'           => 'id',
            'order'             => 'ASC',
            'hide_empty'        => false,
            'parent'            =>0,
        );
        $post_categories=get_terms('destination-category',$args );
        foreach ($post_categories as $category) {
            ?>
            <li class="cat-item">
                <a class="category-post-title" href="<?php echo get_term_link($category->slug, 'destination-category'); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
            </li>
        <?php
        }?>
        </ul>
        <div class='loadMoreCategory button-service'>Voir plus</div>
        <?php

        echo $after_widget;
    }

    /* Widget control update */
    function update( $new_instance, $old_instance ) {
        $instance    = $old_instance;

        $instance['title']  = strip_tags( $new_instance['title'] );
        $instance['number'] = strip_tags( $new_instance['number'] );
        return $instance;
    }

    /* Widget settings */
    function form( $instance ) {

        // Set defaults if instance doesn't already exist
        if ( $instance ) {
            $title  = $instance['title'];
            $number = $instance['number'];
        } else {
            // Defaults
            $title  = '';
            $number = '5';
        }

        // The widget form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
            <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of category to show:', 'coo-theme-admin'); ?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>

    <?php
    }

}

?>