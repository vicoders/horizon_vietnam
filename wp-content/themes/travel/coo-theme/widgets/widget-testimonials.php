<?php

/*
*
*	Custom Testimonials Widget
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

// Register widget
add_action( 'widgets_init', 'init_ct_recent_testimonials' );
function init_ct_recent_testimonials() { return register_widget('ct_recent_testimonials'); }

class ct_recent_testimonials extends WP_Widget {
    function ct_recent_testimonials() {
        parent::__construct( 'ct_recent_custom_testimonials', $name = 'Cootheme Recent Testimonials' );
    }

    function widget( $args, $instance ) {
        global $post;
        extract($args);

        // Widget Options
        $title 	 = apply_filters('widget_title', $instance['title'] ); // Title		
        $number	 = $instance['number']; // Number of posts to show

        echo $before_widget;

        if ( $title ) echo $before_title . $title . $after_title;

        $recent_testimonials = new WP_Query(
            array(
                'post_type' => 'testimonials',
                'posts_per_page' => $number
            )
        );

        if( $recent_testimonials->have_posts() ) :
            ?>
            <ul class="recent-testimonials-list">

                <?php while( $recent_testimonials->have_posts()) : $recent_testimonials->the_post();

                    $custom_post_type = get_post_type(get_the_ID());
                    $categories=get_the_terms( get_the_ID(), 'testimonials-category' );

                    foreach ( $categories as $cat ) {
                        $draught_links[] = $cat->name;
                        $term_link = get_term_link( $cat );
                    }

                    $post_title = get_the_title();
                    $post_permalink = get_permalink();
                    $thumb_image = get_post_thumbnail_id();
                    $thumb_img_url = wp_get_attachment_url( $thumb_image, 'widget-image' );
                    $image = aq_resize( $thumb_img_url, 200,200, true, false);
                    ?>
                    <li class="cleafix">
                        <a href="<?php echo $post_permalink; ?>" class="recent-testimonials-image">
                            <?php if ($image) { ?>
                                <img src="<?php echo $image[0]; ?>" />
                            <?php } ?>
                        </a>
                        <div class="recent-testimonials-details">
                            <a class="recent-testimonials-title" href="<?php echo $post_permalink; ?>" title="<?php echo $post_title; ?>"><?php echo $post_title; ?></a>
                        </div>
                        <div class="content-testimonials-details">
                           <?php echo ct_excerpt('20'); ?>
                        </div>
                        <a href="<?php echo $term_link; ?>" class="button-service"><?php echo __('Voir plus','coo-theme-admin');?></a>
                    </li>

                    <?php wp_reset_query(); endwhile; ?>
            </ul>

        <?php endif; ?>

        <?php

        echo $after_widget;
    }

    /* Widget control update */
    function update( $new_instance, $old_instance ) {
        $instance    = $old_instance;

        $instance['title']  = strip_tags( $new_instance['title'] );
        $instance['number'] = strip_tags( $new_instance['number'] );
        return $instance;
    }

    /* Widget settings */
    function form( $instance ) {

        // Set defaults if instance doesn't already exist
        if ( $instance ) {
            $title  = $instance['title'];
            $number = $instance['number'];
        } else {
            // Defaults
            $title  = '';
            $number = '5';
        }

        // The widget form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
            <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of items to show:', 'coo-theme-admin' ); ?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>
    <?php
    }

}

?>