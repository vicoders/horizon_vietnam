<?php

	/*
	*
	*	Custom Posts Widget
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/

	// Register widget
	add_action( 'widgets_init', 'init_ct_security_footer' );
	function init_ct_security_footer() { return register_widget('ct_security_footer'); }

	class ct_security_footer extends WP_Widget {
		function ct_security_footer() {
			parent::__construct( 'ct_security_footer', $name = 'Security footer' );
		}

		function widget( $args, $instance ) {

			global $post;
			extract($args);

			$title 	 = $instance['title'] ;
			$link_img	 = $instance['link_img'];
            $link_post = $instance['link_post'];

			echo $before_widget;

            ?>
                <a href="<?php echo $link_post; ?>">
                <div class="box text-center">
                    <div class="col-xs-12 security-img">
                        <?php //echo get_the_post_thumbnail($item->ID, 'img-top-footer');?>
                        <img src="<?php echo $link_img ?>" width="110" height="110" class="attachment-img-top-footer size-img-top-footer wp-post-image">
                    </div>
                    <!-- <div class="col-xs-12 security-title"><?php echo $title; ?></div> -->
                    <h5><div class="col-xs-12 security-title"><?php echo $title; ?></div></h5>
                </div>
                </a>
			<?php
			echo $after_widget;
		}

		/* Widget control update */
		function update( $new_instance, $old_instance ) {
			$instance    = $old_instance;

			$instance['title']  = strip_tags($new_instance['title']) ;
			$instance['link_img'] = strip_tags( $new_instance['link_img'] );
			$instance['link_post'] = strip_tags( $new_instance['link_post'] );
			return $instance;
		}

		/* Widget settings */
		function form( $instance ) {

			    // Set defaults if instance doesn't already exist
			    if ( $instance ) {
					$title  = $instance['title'];
			        $link_img = $instance['link_img'];
                    $link_post=$instance['link_post'];
			    } else {
				    // Defaults
					$title  = '';
			        $link_img = '';
                    $link_post='';
			    }

				// The widget form
				?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title post:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('link_img'); ?>"><?php echo __( 'Link for image:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('link_img'); ?>" name="<?php echo $this->get_field_name('link_img'); ?>" type="text" value="<?php echo $link_img; ?>" class="widefat" />
				</p>
                <p>
                    <label for="<?php echo $this->get_field_id('link_post'); ?>"><?php echo __( 'Link for post: ', 'coo-theme-admin'); ?></label>
                    <input id="<?php echo $this->get_field_id('link_post'); ?>" name="<?php echo $this->get_field_name('link_post'); ?>" type="text" value="<?php echo $link_post; ?>" class="widefat" />
                </p>
		<?php
		}

	}

?>