<?php

	/*
	*
	*	Custom Posts Widget
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/
	
	// Register widget
	add_action( 'widgets_init', 'init_ct_categoty_posts' );
	function init_ct_categoty_posts() { return register_widget('ct_categoty_posts'); }

	class ct_categoty_posts extends WP_Widget {
		function ct_categoty_posts() {
			parent::__construct( 'ct_categor_posts', $name = 'Cootheme Categor Posts' );
		}
	
		function widget( $args, $instance ) {

			global $post;
			extract($args);
						
			// Widget Options
			$title 	 = apply_filters('widget_title', $instance['title'] ); // Title		
			$number	 = $instance['number']; // Number of posts to show
			$post_categories	 = $instance['categories'];

			echo $before_widget;
			
		    if ( $post_categories ) echo $before_title .get_cat_name( $post_categories ) . $after_title;
				
			$recent_posts = new WP_Query(
				array(
                    'cat'=>$post_categories,
					'post_type' => 'post',
					'posts_per_page' => $number

					)
			);

			if( $recent_posts->have_posts() ) : 
			
			?>
			
			<ul class="category-posts-list">
				
				<?php while( $recent_posts->have_posts()) : $recent_posts->the_post();
				
				$post_title = get_the_title();
				$post_author = get_the_author_link();
				$post_date = get_the_date();
				$post_categories = get_the_category_list();
				$post_comments = get_comments_number();
				$post_permalink = get_permalink();
				$thumb_image = get_post_thumbnail_id();
				$thumb_img_url = wp_get_attachment_url( $thumb_image, 'widget-image' );
				$image = aq_resize( $thumb_img_url, 94, 75, true, false);
				?>
				<li class="cat-item">
					<a class="category-post-title" href="<?php echo $post_permalink; ?>" title="<?php echo $post_title; ?>"><?php echo $post_title; ?></a>
				</li>
				
				<?php wp_reset_query(); endwhile; ?>
			</ul>
				
			<?php endif; ?>			
			
			<?php
			
			echo $after_widget;
		}
	
		/* Widget control update */
		function update( $new_instance, $old_instance ) {
			$instance    = $old_instance;
				
			$instance['title']  = strip_tags( $new_instance['title'] );
			$instance['number'] = strip_tags( $new_instance['number'] );
			$instance['categories'] = strip_tags( $new_instance['categories'] );
			return $instance;
		}
		
		/* Widget settings */
		function form( $instance ) {	
		
			    // Set defaults if instance doesn't already exist
			    if ( $instance ) {
					$title  = $instance['title'];
			        $number = $instance['number'];
                    $categories=$instance['categories'];
			    } else {
				    // Defaults
					$title  = '';
			        $number = '5';
                    $categories='';
			    }
				
				// The widget form
				?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of posts to show:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
				</p>
                <p>
                    <label for="<?php echo $this->get_field_id('categories'); ?>"><?php echo __( 'Choose Category:', 'coo-theme-admin'); ?></label>
                    <select  name="<?php echo $this->get_field_name('categories'); ?>">
                    <?php
                    $args = array(
                        'orderby' => 'name',
                    );
                    $categories = get_categories($args);
                    foreach ($categories as $category) {
                        $option = '<option value="'.$category->term_id.'">';
                        $option .= $category->name;
                        $option .= '</option>';
                        echo $option;
                    }?>
                    </select>
                </p>
		<?php 
		}
	
	}

?>