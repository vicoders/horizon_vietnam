<?php

	/*
	*
	*	Custom tab Posts Widget
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/
	
	// Register widget
	add_action( 'widgets_init', 'init_ct_tab_posts' );
	function init_ct_tab_posts() { return register_widget('ct_tab_posts'); }
	
	class ct_tab_posts extends WP_Widget {
		function ct_tab_posts() {
			parent::__construct( 'ct_tab_custom_posts', $name = 'Cootheme Tab Posts' );
		}
	
		function widget( $args, $instance ) {
			global $post;
			extract($args);
						
			// Widget Options
			$title 	 = apply_filters('widget_title', $instance['title'] ); // Title		
			$number	 = $instance['number']; // Number of posts to show
			$title1 	 = apply_filters('widget_title', $instance['title1'] ); // Title		
			$number1	 = $instance['number1']; // Number of posts to show
				
			$recent_posts1 = new WP_Query(
				array(
					'post_type' => 'post',
					'posts_per_page' => $number1
					)
			);
			$recent_posts = new WP_Query(
				array(
					'post_type' => 'post',
					'posts_per_page' => $number,
					'orderby'=>'comment_count'
					)
			);
			?>
			<div class="tab-content-sidebar">
				<ul class="nav nav-tabs" id="myTab">
				  <li class="active"><a href="#popular" data-toggle="tab"><?php echo $title ?></a></li>
				  <li><a href="#recent " data-toggle="tab"><?php echo $title1 ?></a></li>
				</ul>

				<div class="tab-content">
				  	<div class="tab-pane active" id="popular">
				  		<?php if( $recent_posts->have_posts() ) : ?>
					  	<ul class="popular-posts-tab">
							<?php while( $recent_posts->have_posts()) : $recent_posts->the_post();
							$post_title = get_the_title();
							$post_author = get_the_author_link();
							$post_date = get_the_date();
							$post_categories = get_the_category_list();
							$post_comments = get_comments_number();
							$post_permalink = get_permalink();
							$thumb_image = get_post_thumbnail_id();
							$thumb_img_url = wp_get_attachment_url( $thumb_image, 'widget-image' );
							$image = aq_resize( $thumb_img_url, 56, 56, true, false);
							?>
							<li>
								<a href="<?php echo $post_permalink; ?>" class="recent-post-image">
									<?php if ($image) { ?>
									<img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
									<?php } ?>
								</a>
								<div class="recent-post-details">
									<a class="recent-post-title" href="<?php echo $post_permalink; ?>" title="<?php echo $post_title; ?>"><?php echo $post_title; ?></a>
									<span><?php printf(__('%2$s', 'cootheme'), $post_author, $post_date); ?></span>
								</div>
							</li>
							
							<?php wp_reset_query(); endwhile; ?>
						</ul>
							
						<?php endif; ?>

				  	</div>
				  	<div class="tab-pane" id="recent">
				  		<?php if( $recent_posts1->have_posts() ) : ?>
					  	<ul class="recent-posts-tab">
							<?php while( $recent_posts1->have_posts()) : $recent_posts1->the_post();
							$post_title = get_the_title();
							$post_author = get_the_author_link();
							$post_date = get_the_date();
							$post_categories = get_the_category_list();
							$post_comments = get_comments_number();
							$post_permalink = get_permalink();
							$thumb_image = get_post_thumbnail_id();
							$thumb_img_url = wp_get_attachment_url( $thumb_image, 'widget-image' );
							$image = aq_resize( $thumb_img_url, 56, 56, true, false);
							?>
							<li>
								<a href="<?php echo $post_permalink; ?>" class="recent-post-image">
									<?php if ($image) { ?>
									<img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" />
									<?php } ?>
								</a>
								<div class="recent-post-details">
									<a class="recent-post-title" href="<?php echo $post_permalink; ?>" title="<?php echo $post_title; ?>"><?php echo $post_title; ?></a>
									<span><?php printf(__('%2$s', 'cootheme'), $post_author, $post_date); ?></span>
								</div>
							</li>
							
							<?php wp_reset_query(); endwhile; ?>
						</ul>
							
						<?php endif; ?>

				  	</div>
				</div>

				<script>
				  $(function () {
				    $('#myTab a:last').tab('show')
				  })
				</script>


			</div>
			
						
			
			<?php
			
			echo $after_widget;
		}
	
		/* Widget control update */
		function update( $new_instance, $old_instance ) {
			$instance    = $old_instance;
				
			$instance['title']  = strip_tags( $new_instance['title'] );
			$instance['number'] = strip_tags( $new_instance['number'] );
			$instance['title1']  = strip_tags( $new_instance['title1'] );
			$instance['number1'] = strip_tags( $new_instance['number1'] );
			return $instance;
		}
		
		/* Widget settings */
		function form( $instance ) {	
		
			    // Set defaults if instance doesn't already exist
			    if ( $instance ) {
					$title  = $instance['title'];
			        $number = $instance['number'];
			        $title1  = $instance['title1'];
			        $number1 = $instance['number1'];
			    } else {
				    // Defaults
					$title  = 'Popular';
			        $number = '5';
			        $title1  = 'Recent post';
			        $number1 = '5';
			    }
				
				// The widget form
				?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of posts to show:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
				</p>

				<p>
					<label for="<?php echo $this->get_field_id('title1'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title1'); ?>" name="<?php echo $this->get_field_name('title1'); ?>" type="text" value="<?php echo $title1; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number1'); ?>"><?php echo __( 'Number of posts to show:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('number1'); ?>" name="<?php echo $this->get_field_name('number1'); ?>" type="text" value="<?php echo $number1; ?>" size="3" />
				</p>
		<?php 
		}
	
	}

?>