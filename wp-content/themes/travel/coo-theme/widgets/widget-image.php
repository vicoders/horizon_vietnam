 <?php
 add_action( 'widgets_init', 'image_widget' );
 function image_widget() {
        register_widget( 'image_widget' );
 }
 class image_widget extends WP_Widget
 {
         public function __construct()
         {
            // Basic widget details
             $image_detail=array(
                 'classname'    =>'image_widget',
                 'description'  =>'Creates a featured item consisting of a title, image and link.'
             );
             parent::__construct('image_widget','Image widget',$image_detail);
         }
         public function widget( $args, $instance )
         {
         // Widget output in the front end
             echo $args['before_widget'];
             if ( ! empty( $instance['title'] ) ) {
                 echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
             }
            // Rest of the widget content
             echo $args['after_widget'];

             add_action('admin_enqueue_scripts', array( $this, 'mfc_assets' ) );?>
             <p>
                <img src='<?php echo $instance['image_widget'] ?>'>
            </p>

         <?php }
         public function update( $new_instance, $old_instance ) {
        // Form saving logic - if needed
             $instance['title'] =strip_tags($new_instance['title']);
             $instance['image_widget'] =strip_tags($new_instance['image_widget']);
            return $instance;
         }
         public function form( $instance ) {
        // Backend Form
               $title='';
                if(!empty($instance['title'])){
                    $title=$instance['title'];
                }
                $image_widget='';
                if(!empty($instance['image_widget'])){
                    $image_widget=$instance['image_widget'];
                }?>
                <p>
                    <label for="<?php echo $this->get_field_name('title');?>"> <?php echo $title; ?> </label>
                    <input type="text"
                           class="<?php echo $this->get_field_name('title');?>"
                           id="<?php echo $this->get_field_id('title');?>"
                           name="<?php echo $this->get_field_name('title');?>"
                           value="<?php echo esc_attr('Title');?>"
                        />
                </p>

             <p>
                 <label for="<?php echo $this->get_field_name( 'image_widget' ); ?>"><?php _e( 'Image:' ); ?></label>
                 <input name="<?php echo $this->get_field_name( 'image_widget' ); ?>" id="<?php echo $this->get_field_id( 'image_widget' ); ?>" class="widefat" type="text" size="36" value="<?php echo esc_url( $image ); ?>" />
                 <input class="upload_image_button" type="button" value="Upload Image" />
             </p>


         <?php }




 }
 ?>