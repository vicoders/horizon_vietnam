<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 6/4/15
 * Time: 9:07 AM
 */
add_action( 'widgets_init', 'widget_service' );
function widget_service() {
    register_widget( 'Widget_Service' );
}
class Widget_Service extends WP_Widget
{
     function __construct()
    {
    // Basic widget details
        $widget_service = array(
            'classname' => 'widget_service',
            'description' =>'Description and link.'
        );
        parent::__construct( 'widget_service', 'Coo-Service  Widget', $widget_service );
    }
     function widget( $args, $instance )
    {
    // Widget output in the front end
        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );
        $description1 = $instance['description1'];
        $description2 = $instance['description2'];
        $description3 = $instance['description3'];
        $description4 = $instance['description4'];
        $description5 = $instance['description5'];
        $description6 = $instance['description6'];
        $link_url     =  $instance['link_url'];
        $output = '';
        echo $before_widget;
        if ( $title ) echo $before_title . $title . $after_title;
        ?>
        <style type="text/css">
            .widget_service .sidebar-service ul li a {
                color: #d80101;
                font-size: 13px;
                font-weight: 500;
            }
            @media screen and (max-width: 767px) {
                .widget_service .sidebar-service ul li a {
                    font-size: 17px;
                }
            }
        </style>
        <?php
        $output .= '<div class="sidebar-service"><ul class="clearfix">';
        if ($description1 != "") {
            $output .= '<li>';
                $output .= '<a href="'.$link_url.'"><span>';
                $output .= $description1;
                $output .= '</span></a>';
            $output .= '</li>';
        }
        if ($description2 != "") {
            $output .= '<li>';
            $output .= '<a href="'.$link_url.'"><span>';
            $output .= $description2;
            $output .= '</span></a>';
            $output .= '</li>';
        }
        if ($description3 != "") {
            $output .= '<li>';
            $output .= '<a href="'.$link_url.'"><span>';
            $output .= $description3;
            $output .= '</span></a>';
            $output .= '</li>';
        }
        if ($description4 != "") {
            $output .= '<li>';
            $output .= '<a href="'.$link_url.'"><span>';
            $output .= $description4;
            $output .= '</span></a>';
            $output .= '</li>';
        }
        if ($description5 != "") {
            $output .= '<li>';
            $output .= '<a href="'.$link_url.'"><span>';
            $output .= $description5;
            $output .= '</span></a>';
            $output .= '</li>';
        }
        if ($description6 != "") {
            $output .= '<li>';
            $output .= '<a href="'.$link_url.'"><span>';
            $output .= $description6;
            $output .= '</span></a>';
            $output .= '</li>';
        }
        $output .= '</ul>';
        $output .= '<a class="button-service" href="'.$link_url.'">';
        $output .=  __('Voir plus','coo-theme-admin');
        $output .='</a>';
        $output .= '</div>';

        echo $output;

        echo $after_widget;
    }
     function update( $new_instance, $old_instance ) {
    // Form saving logic - if needed
         $instance = $old_instance;

         $instance['title'] = strip_tags( $new_instance['title'] );
         $instance['description1'] = strip_tags( $new_instance['description1'] );
         $instance['description2'] = strip_tags( $new_instance['description2'] );
         $instance['description3'] = strip_tags( $new_instance['description3'] );
         $instance['description4'] = strip_tags( $new_instance['description4'] );
         $instance['description5'] = strip_tags( $new_instance['description5'] );
         $instance['description6'] = strip_tags( $new_instance['description6'] );
         $instance['link_url'] = strip_tags( $new_instance['link_url'] );

         return $instance;

     }
     function form( $instance ) {
    // Backend Form
         $title = '';
         if( !empty( $instance['title'] ) ) {
             $title = $instance['title'];
         }
         $defaults = array( 'title' => '', 'description1' => '','description2' => '','description3' => '','description4' => '','description5' => '','description6' => '',);
         $instance = wp_parse_args( (array) $instance, $defaults );

         $description1 = '';
         if( !empty( $instance['description1'] ) ) {
             $description1 = $instance['description1'];
         }
         $description2 = '';
         if( !empty( $instance['description2'] ) ) {
             $description2 = $instance['description2'];
         }
         $description3 = '';
         if( !empty( $instance['description3'] ) ) {
             $description3 = $instance['description3'];
         }
         $description4 = '';
         if( !empty( $instance['description4'] ) ) {
             $description4 = $instance['description4'];
         }
         $description5 = '';
         if( !empty( $instance['description5'] ) ) {
             $description5 = $instance['description5'];
         }
         $description6 = '';
         if( !empty( $instance['description6'] ) ) {
             $description6 = $instance['description6'];
         }

         $link_url = '';
         if( !empty( $instance['link_url'] ) ) {
             $link_url = $instance['link_url'];
         }
         ?>
         <p>
             <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
             <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
         </p>

         <p>
             <label for="<?php echo $this->get_field_name( 'description1' ); ?>"><?php _e( 'Description 1:' ); ?></label>
             <textarea class="widefat" id="<?php echo $this->get_field_id( 'description1' ); ?>" name="<?php echo $this->get_field_name( 'description1' ); ?>" type="text" ><?php echo esc_attr( $description1 ); ?></textarea>
         </p>
         <p>
             <label for="<?php echo $this->get_field_name( 'description2' ); ?>"><?php _e( 'Description 2:' ); ?></label>
             <textarea class="widefat" id="<?php echo $this->get_field_id( 'description2' ); ?>" name="<?php echo $this->get_field_name( 'description2' ); ?>" type="text" ><?php echo esc_attr( $description2 ); ?></textarea>
         </p>
         <p>
             <label for="<?php echo $this->get_field_name( 'description3' ); ?>"><?php _e( 'Description 3:' ); ?></label>
             <textarea class="widefat" id="<?php echo $this->get_field_id( 'description3' ); ?>" name="<?php echo $this->get_field_name( 'description3' ); ?>" type="text" ><?php echo esc_attr( $description3 ); ?></textarea>
         </p>
         <p>
             <label for="<?php echo $this->get_field_name( 'description4' ); ?>"><?php _e( 'Description 4:' ); ?></label>
             <textarea class="widefat" id="<?php echo $this->get_field_id( 'description4' ); ?>" name="<?php echo $this->get_field_name( 'description4' ); ?>" type="text" ><?php echo esc_attr( $description4 ); ?></textarea>
         </p>
         <p>
             <label for="<?php echo $this->get_field_name( 'description5' ); ?>"><?php _e( 'Description 5:' ); ?></label>
             <textarea class="widefat" id="<?php echo $this->get_field_id( 'description5' ); ?>" name="<?php echo $this->get_field_name( 'description5' ); ?>" type="text" ><?php echo esc_attr( $description5 ); ?></textarea>
         </p>
         <p>
             <label for="<?php echo $this->get_field_name( 'description6' ); ?>"><?php _e( 'Description 6:' ); ?></label>
             <textarea class="widefat" id="<?php echo $this->get_field_id( 'description6' ); ?>" name="<?php echo $this->get_field_name( 'description6' ); ?>" type="text" ><?php echo esc_attr( $description6 ); ?></textarea>
         </p>

         <p>
             <label for="<?php echo $this->get_field_name( 'link_url' ); ?>"><?php _e( 'Link URL:' ); ?></label>
             <input class="widefat" id="<?php echo $this->get_field_id( 'link_url' ); ?>" name="<?php echo $this->get_field_name( 'link_url' ); ?>" type="text" value="<?php echo esc_attr( $link_url ); ?>" />
         </p>

     <?php
    }
}