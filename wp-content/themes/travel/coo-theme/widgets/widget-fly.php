<?php

/*
*
*	Custom Advert Grid Widget
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

class ct_fly_widget extends WP_Widget {

    function ct_fly_widget() {
        $widget_ops = array( 'classname' => 'widget-fly', 'description' => 'Styled Assistance of up to eight 125x125 adverts' );
        $control_ops = array( 'width' => 250, 'height' => 200, 'id_base' => 'fly-widget' ); //default width = 250
        parent::__construct( 'fly-widget', 'Cootheme Fly Widget', $widget_ops, $control_ops );
    }

    function form($instance) {
        $defaults = array('title' => '','link_url1' => '','link_url2' => '');
        $instance = wp_parse_args( (array) $instance, $defaults );

        ?>
        <p>
            <label><?php _e('Title', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
        </p>
        <p>
            <label><?php _e('Link URL Devis gratuit réponse sous 24h', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'link_url2' ); ?>" name="<?php echo $this->get_field_name( 'link_url2' ); ?>" value="<?php echo $instance['link_url2']; ?>" class="widefat" type="text"/>
        </p>
        <p>
            <label><?php _e('Link URL Rappel telephonique gratuit', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'link_url1' ); ?>" name="<?php echo $this->get_field_name( 'link_url1' ); ?>" value="<?php echo $instance['link_url1']; ?>" class="widefat" type="text"/>
        </p>

    <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['link_url1'] = strip_tags( $new_instance['link_url1'] );
        $instance['link_url2'] = strip_tags( $new_instance['link_url2'] );

        return $instance;
    }

    function widget($args, $instance) {

        extract( $args );
        $title = $instance['title'];
        $link_url1 = $instance['link_url1'];
        $link_url2 = $instance['link_url2'];
        $output = '';
        echo $before_widget;
        if ( $title ) echo $before_title . $title . $after_title;

        $output .= '<div class="sidebar-fly"><ul class="clearfix">';
        if ($link_url2 != "") {
            $output .= '<li>';
            $output .= '<a class="button-fly"  href="'.$link_url2.'">';
            $output .= '<span class="advisory-free">devis gratuit</span>';
            $output .= '<span class="advisory-small">réponse sous 24h</span>';
            $output .= '</a>';
            $output .= '</li>';
        }
        if ($link_url1 != "") {
            $output .= '<li>';
            $output .= '<a class="button-fly"  href="'.$link_url1.'">';
            $output .= '<span class="call-free"> rappel  </span>';
            $output .= '<span class="call-two">telephonique</span>';
            $output .= '<span class="call-small"> gratuit  </span>';
            $output .= '</a>';
            $output .= '</li>';
        }
        $output .= '</ul>';
        $output .= '</div>';
        echo $output;

        echo $after_widget;

    }

}

add_action( 'widgets_init', 'ct_load_fly_widget' );

function ct_load_fly_widget() {
    register_widget('ct_fly_widget');
}

?>
