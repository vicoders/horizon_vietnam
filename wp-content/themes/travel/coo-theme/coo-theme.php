<?php
	
	/*
	*
	*	Cootheme Main Class
	*	------------------------------------------------
	*	Cootheme v2.0
	* 	http://www.cootheme.com
	*
	*/
	
	
	/* COO FUNCTIONS
	================================================== */  
	include_once(CT_FRAMEWORK_PATH . '/ct-functions.php');
	

	/* CUSTOM POST TYPES
	================================================== */  
//	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/portfolio-type.php');
//	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/gallery-type.php');
    require_once(CT_FRAMEWORK_PATH . '/custom-post-types/sea-type.php');//các Tour du lịch biển

    require_once(CT_FRAMEWORK_PATH . '/custom-post-types/destination-type.php');//điểm đến du lịch

	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/levietnam-type.php');//văn hóa

	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/guide-type.php');//hướng dẫn du lịch

	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/aboutus-type.php');// thông tin về cá nhân

	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/special-type.php');// đặc điểm đặc biệt của công ty

    require_once(CT_FRAMEWORK_PATH . '/custom-post-types/books-type.php');//sách nói về  VN

    require_once(CT_FRAMEWORK_PATH . '/custom-post-types/embassies-type.php');//đại sứ quán

    require_once(CT_FRAMEWORK_PATH . '/custom-post-types/responsibility-type.php');//trách nhiệm du lịch

    require_once(CT_FRAMEWORK_PATH . '/custom-post-types/security-type.php');//Bai viet ve bao mat

    require_once(CT_FRAMEWORK_PATH . '/custom-post-types/nos-bureaux-type.php');//Bai viet ve bao mat





//	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/team-type.php');
	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/brand-type.php');
//	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/clients-type.php');
	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/testimonials-type.php');

	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/faqs-type.php');
//	require_once(CT_FRAMEWORK_PATH . '/custom-post-types/ct-post-type-permalinks.php');

	
	/* COO PAGE BUILDER
	================================================== */ 
	include_once(CT_FRAMEWORK_PATH . '/page-builder/ct-page-builder.php');
	
	
	/* META BOX FRAMEWORK
	================================================== */  
	include_once(CT_FRAMEWORK_PATH . '/meta-box/meta-box.php');
	include_once(CT_FRAMEWORK_PATH . '/meta-boxes.php');
		
	
	/* WOOCOMMERCE FILTERS/HOOKS
	================================================== */  
	include_once(CT_FRAMEWORK_PATH . '/ct-woocommerce.php');
	
	
	/* SHORTCODES
	================================================== */  
	include_once(CT_FRAMEWORK_PATH . '/shortcodes.php');
	
	
	/* MEGA MENU
	================================================== */  
	include_once(CT_FRAMEWORK_PATH . '/ct-megamenu/ct-megamenu.php');

	
	/* SUPER SEARCH
	================================================== */  
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	include_once(CT_FRAMEWORK_PATH . '/ct-supersearch.php');
	}
	
	
	/* WIDGETS
	================================================== */  
//	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-twitter.php');
//	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-flickr.php');
//	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-video.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-aboutus.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-levietnam-posts.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-guide-posts.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-category-posts.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-page-links.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-destination-parent.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-destination-children.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-booking-tour.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-sea-tour.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-fly.php');
//	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-tab.php');
//	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-portfolio.php');
//	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-portfolio-grid.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-advertgrid.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-assistance.php');
//	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-infocus.php');
//	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-comments.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-testimonials.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-service.php');
	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-security-footer.php');

	include_once(CT_FRAMEWORK_PATH . '/widgets/widget-phone-header.php');

	
	/* TEXT DOMAIN
	================================================== */
	load_theme_textdomain( 'coo-theme-admin', get_template_directory() . '/coo-theme/language' );
	
?>