<?php

	$args = array(
	    "label" 						=> _x('Category', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Sécurité et sérénité', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'security-serenity',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'security-category', 'security', $args );


	add_action('init', 'security_register');

	function security_register() {

	    $labels = array(
	        'name' => _x('Sécurité et sérénité', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Sécurité et sérénité', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'job', "coo-theme-admin"),
	        'add_new_item' => __('Add New', "coo-theme-admin"),
	        'edit_item' => __('Edit', "coo-theme-admin"),
	        'new_item' => __('New', "coo-theme-admin"),
	        'view_item' => __('View', "coo-theme-admin"),
	        'search_items' => __('Search', "coo-theme-admin"),
	        'not_found' =>  __('No Sécurité et sérénité have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
            'rewrite' =>  array('slug' => 'security-serenity','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('security-category', 'post_tag',)
	       );

	    register_post_type( 'security' , $args );
	}
	add_filter("manage_edit-security_columns", "security_edit_columns");
	function security_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Sécurité et sérénité", "coo-theme-admin"),
	            "description" => __("Description", "coo-theme-admin"),
	            "security-category" => __("Categories", "coo-theme-admin")
	        );

	        return $columns;
	}

?>