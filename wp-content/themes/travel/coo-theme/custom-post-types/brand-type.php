<?php

/* ==================================================

Brand Post Type Functions

================================================== */


$args = array(
    "label" 						=> _x('Brand Categories', 'category label', "coo-theme-admin"),
    "singular_label" 				=> _x('Brand Category', 'category singular label', "coo-theme-admin"),
    'public'                        => true,
    'hierarchical'                  => true,
    'show_ui'                       => true,
    'show_in_nav_menus'             => true,
    'args'                          => array( 'orderby' => 'term_order' ),
    'rewrite'                       => array(
                                        'slug' => 'brand',
                                        'with_front' => false ),
    'query_var'                     => true
);

register_taxonomy( 'brand-category', 'brand', $args );


add_action('init', 'brand_register');

function brand_register() {

    $labels = array(
        'name' => _x('Brand', 'post type general name', "coo-theme-admin"),
        'singular_name' => _x('Brand Member', 'post type singular name', "coo-theme-admin"),
        'add_new' => _x('Add New', 'Brand member', "coo-theme-admin"),
        'add_new_item' => __('Add New Brand Member', "coo-theme-admin"),
        'edit_item' => __('Edit Brand Member', "coo-theme-admin"),
        'new_item' => __('New Brand Member', "coo-theme-admin"),
        'view_item' => __('View Brand Member', "coo-theme-admin"),
        'search_items' => __('Search Brand Members', "coo-theme-admin"),
        'not_found' =>  __('No Brand members have been added yet', "coo-theme-admin"),
        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'rewrite' => false,
        'supports' => array('title', 'editor', 'thumbnail'),
        'has_archive' => true,
        'taxonomies' => array('brand-category','post_tag')
    );

    register_post_type( 'brand' , $args );
}

add_filter("manage_edit-brand_columns", "brand_edit_columns");

function brand_edit_columns($columns){
    $columns = array(
        "cb" => "<input type=\"checkbox\" />",
        "thumbnail" => "",
        "title" => __("Brand Member", "coo-theme-admin"),
        "description" => __("Description", "coo-theme-admin"),
        "brand-category" => __("Categories", "coo-theme-admin")
    );

    return $columns;
}

?>