<?php

	/* ==================================================

	Books Post Type Functions

	================================================== */
	$args = array(
	    "label" 						=> _x('Notre équipe Category', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Notre équipe', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'agence-voyage-vietnam',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'aboutus-category', 'aboutus', $args );


	add_action('init', 'aboutus_register');

	function aboutus_register() {

	    $labels = array(
	        'name' => _x('Notre équipe', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Notre équipe', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'job', "coo-theme-admin"),
	        'add_new_item' => __('Add New Notre équipe', "coo-theme-admin"),
	        'edit_item' => __('Edit Notre équipe', "coo-theme-admin"),
	        'new_item' => __('New Notre équipe', "coo-theme-admin"),
	        'view_item' => __('View Notre équipe', "coo-theme-admin"),
	        'search_items' => __('Search Notre équipe', "coo-theme-admin"),
	        'not_found' =>  __('No Notre équipe have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
	        'rewrite' =>  array('slug' => 'agence-locale-vietnamienne','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('aboutus-category', 'post_tag',)
	       );

	    register_post_type( 'aboutus' , $args );
	}
	add_filter("manage_edit-aboutus_columns", "aboutus_edit_columns");
	function aboutus_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Notre équipe", "coo-theme-admin"),
	            "description" => __("Description", "coo-theme-admin"),
	            "aboutus-category" => __("Categories", "coo-theme-admin")
	        );

	        return $columns;
	}

?>