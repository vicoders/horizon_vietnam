<?php

/* ==================================================

Books Post Type Functions

================================================== */
	$args = array(
	    "label" 						=> _x('Destination Category', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Destination', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'visiter-le-vietnam',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'destination-category', 'destination', $args );


	add_action('init', 'destination_register');

	function destination_register() {

	    $labels = array(
	        'name' => _x('Destination', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Destination', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'job', "coo-theme-admin"),
	        'add_new_item' => __('Add New Destination', "coo-theme-admin"),
	        'edit_item' => __('Edit Destination', "coo-theme-admin"),
	        'new_item' => __('New Destination', "coo-theme-admin"),
	        'view_item' => __('View Destination', "coo-theme-admin"),
	        'search_items' => __('Search Destination', "coo-theme-admin"),
	        'not_found' =>  __('No Destination have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
	        'rewrite' =>  array('slug' => 'villes-du-vietnam','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('destination-category', 'post_tag',)
	       );

	    register_post_type( 'destination' , $args );
	}
	add_filter("manage_edit-destination_columns", "destination_edit_columns");
	function destination_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Destination", "coo-theme-admin"),
	            "description" => __("Description", "coo-theme-admin"),
	            "destination-category" => __("Categories", "coo-theme-admin")
	        );

	        return $columns;
	}

?>