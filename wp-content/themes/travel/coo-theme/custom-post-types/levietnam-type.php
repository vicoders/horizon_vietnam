<?php

	/* ==================================================

	Books Post Type Functions

	================================================== */
	$args = array(
	    "label" 						=> _x('Culture Category', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Culture', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'guide-culturel',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'levietnam-category', 'levietnam', $args );


	add_action('init', 'levietnam_register');

	function levietnam_register() {

	    $labels = array(
	        'name' => _x('Culture', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Culture', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'job', "coo-theme-admin"),
	        'add_new_item' => __('Add New Culture', "coo-theme-admin"),
	        'edit_item' => __('Edit Culture', "coo-theme-admin"),
	        'new_item' => __('New Culture', "coo-theme-admin"),
	        'view_item' => __('View Culture', "coo-theme-admin"),
	        'search_items' => __('Search Culture', "coo-theme-admin"),
	        'not_found' =>  __('No Culture have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
            'rewrite' => array('slug' => 'culture-vietnam','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('levietnam-category', 'post_tag',)
	       );

	    register_post_type( 'levietnam' , $args );
	}

	add_filter("manage_edit-levietnam_columns", "levietnam_edit_columns");

	function levietnam_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Culture", "coo-theme-admin"),
	            "description" => __("Description", "coo-theme-admin"),
	            "levietnam-category" => __("Categories", "coo-theme-admin")
	        );

	        return $columns;
	}

?>