<?php

	/* ==================================================

	Books Post Type Functions

	================================================== */
	$args = array(
	    "label" 						=> _x('Bonnes raisons Category', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Bonnes raisons', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'notre-mission',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'special-category', 'special', $args );


	add_action('init', 'special_register');

	function special_register() {

	    $labels = array(
	        'name' => _x('Bonnes raisons', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Bonnes raisons', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'job', "coo-theme-admin"),
	        'add_new_item' => __('Add New Bonnes raisons', "coo-theme-admin"),
	        'edit_item' => __('Edit Bonnes raisons', "coo-theme-admin"),
	        'new_item' => __('New Bonnes raisons', "coo-theme-admin"),
	        'view_item' => __('View Bonnes raisons', "coo-theme-admin"),
	        'search_items' => __('Search Bonnes raisons', "coo-theme-admin"),
	        'not_found' =>  __('No Bonnes raisons have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
            'rewrite' =>  array('slug' => 'nos-projets','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('special-category', 'post_tag',)
	       );

	    register_post_type( 'special' , $args );
	}
	add_filter("manage_edit-special_columns", "special_edit_columns");
	function special_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Bonnes raisons", "coo-theme-admin"),
	            "description" => __("Description", "coo-theme-admin"),
	            "special-category" => __("Categories", "coo-theme-admin")
	        );

	        return $columns;
	}

?>