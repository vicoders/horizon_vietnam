<?php

	/* ==================================================
	
	Ambassadeurss Post Type Functions
	
	================================================== */
	    
	    
	$args = array(
	    "label" 						=> _x('Ambassadeurs Categories', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Ambassadeurs Category', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                        'slug' => 'horizon-vietnam-en-france',
                                        'with_front' => false ),
	    'query_var'                     => true
	);
	
	register_taxonomy( 'embassies-category', 'embassies', $args );
	
	
	add_action('init', 'embassies_register');  
	  
	function embassies_register() {  
	
	    $labels = array(
	        'name' => _x('Ambassadeurss', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Ambassadeurs', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'Ambassadeurs', "coo-theme-admin"),
	        'add_new_item' => __('Add New Ambassadeurs', "coo-theme-admin"),
	        'edit_item' => __('Edit Ambassadeurs', "coo-theme-admin"),
	        'new_item' => __('New Ambassadeurs', "coo-theme-admin"),
	        'view_item' => __('View Ambassadeurs', "coo-theme-admin"),
	        'search_items' => __('Search Ambassadeurss', "coo-theme-admin"),
	        'not_found' =>  __('No embassies have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );
	
	    $args = array(  
	        'labels' => $labels,  
	        'public' => true,  
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => false,
            'rewrite' =>  array('slug' => 'nos-ambassadeurs','with_front' => false ),
            'supports' => array('title', 'editor','thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('embassies-category', 'post_tag')
	       );  
	  
	    register_post_type( 'embassies' , $args );  
	}  
	
	add_filter("manage_edit-embassies_columns", "embassies_edit_columns");   
	
	function embassies_edit_columns($columns){  
	        $columns = array(  
	            "cb" => "<input type=\"checkbox\" />",
                "thumbnail" => "",
	            "title" => __("Ambassadeurs", "coo-theme-admin"),
	            "embassies-category" => __("Categories", "coo-theme-admin")
	        );  
	  
	        return $columns;  
	}

?>