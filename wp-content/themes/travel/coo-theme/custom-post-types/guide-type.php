<?php

	/* ==================================================

	Books Post Type Functions

	================================================== */
	$args = array(
	    "label" 						=> _x('Guide Category', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Guide', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'guide-de-voyage-vietnam',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'guide-category', 'guide', $args );


	add_action('init', 'guide_register');

	function guide_register() {

	    $labels = array(
	        'name' => _x('Guide', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Guide', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'job', "coo-theme-admin"),
	        'add_new_item' => __('Add New Guide', "coo-theme-admin"),
	        'edit_item' => __('Edit Guide', "coo-theme-admin"),
	        'new_item' => __('New Guide', "coo-theme-admin"),
	        'view_item' => __('View Guide', "coo-theme-admin"),
	        'search_items' => __('Search Guide', "coo-theme-admin"),
	        'not_found' =>  __('No Guide have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
            'rewrite' => array('slug' => 'vietnam-guide','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('guide-category', 'post_tag',)
	       );

	    register_post_type( 'guide' , $args );
	}

	add_filter("manage_edit-guide_columns", "guide_edit_columns");

	function guide_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Guide", "coo-theme-admin"),
	            "description" => __("Description", "coo-theme-admin"),
	            "guide-category" => __("Categories", "coo-theme-admin")
	        );

	        return $columns;
	}

?>