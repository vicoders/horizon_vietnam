<?php

/* ==================================================

Books Post Type Functions

================================================== */
	$args = array(
	    "label" 						=> _x('Séjour balneaire Category', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Séjour balneaire', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'sejour-vietnam',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'sea-category', 'sea', $args );


	add_action('init', 'sea_register');

	function sea_register() {

	    $labels = array(
	        'name' => _x('Séjour balneaire', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Séjour balneaire', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'job', "coo-theme-admin"),
	        'add_new_item' => __('Add New Séjour balneaire', "coo-theme-admin"),
	        'edit_item' => __('Edit Séjour balneaire', "coo-theme-admin"),
	        'new_item' => __('New Séjour balneaire', "coo-theme-admin"),
	        'view_item' => __('View Séjour balneaire', "coo-theme-admin"),
	        'search_items' => __('Search Séjour balneaire', "coo-theme-admin"),
	        'not_found' =>  __('No Séjour balneaire have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
	        'rewrite' =>  array('slug' => 'sejour-au-vietnam','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('sea-category', 'post_tag',)
	       );

	    register_post_type( 'sea' , $args );
	}
	add_filter("manage_edit-sea_columns", "sea_edit_columns");
	function sea_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Séjour balneaire", "coo-theme-admin"),
	            "description" => __("Description", "coo-theme-admin"),
	            "sea-category" => __("Categories", "coo-theme-admin")
	        );

	        return $columns;
	}

?>